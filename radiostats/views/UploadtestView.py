from django.shortcuts import render
from radiostats.models import Radiostat
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import checksession,getfromSession, savesessiondata
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.static_values import AppMsgs
from users.views.UserView import UserView
from datetime import datetime
import json
from airebeam_customer_portal.AppServices.LogResultService import LogResultService
from airebeam_customer_portal import static_values

class UploadtestView(UserView):
    template_name = 'radiostats/single_speedtest.html'
    
    def __init__(self):
        self.error = ''
        self.context = {}
    
    def get(self, request):
        if checksession(AppMsgs.MBRKEY, request):
            login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
            is_session_exceded = check_if_session_time_exceeds(login_time)
    #         Logout and redirect to login page
            if is_session_exceded:
                return HttpResponseRedirect(reverse('users:logout'))
            try:
                self.error = AppMsgs.NOT_FOUND_CONNECTION
                self.context = {AppMsgs.ERROR: self.error}
                self.__fetch_radiostats()
                
            except Exception as e:
                self.set_last_speedtest_time(None)
                pass
            
            return render(request, self.template_name, self.context)
        else:
            return HttpResponseRedirect(reverse('users:login'))
        
    def insert_ul_speed_in_summary(self, radio_stats):
        summary_str = radio_stats[AppMsgs.SUMMARY_KEY]
        sub_str = 'DL_FLAG'
        if summary_str.find(sub_str) != -1:
            summary_arr = summary_str.split(sub_str)
            ul_summary_str = '\r\nRadio TX: ' + str(radio_stats[AppMsgs.UPLOAD_KEY][AppMsgs.AVERAGE_KEY]) + ' Mbps\n'
            final_summary_str = summary_arr[0] + ul_summary_str + summary_arr[1]
            radio_stats[AppMsgs.SUMMARY_KEY] = final_summary_str    
    
#    Fetch radio stats in session    
    def __fetch_radiostats(self):
        mbr = getfromSession(AppMsgs.MBRKEY, self.request)
        r = Radiostat()
        upload_result = {}
        radio_stats = self.get_temp_speedtest_result()
        if radio_stats is not None:
            payload = json.dumps(radio_stats)
            upload_result = r.getUploadStats(mbr, self.request, payload)
            radio_stats[AppMsgs.UPLOAD_KEY] = upload_result
            log_result_obj = LogResultService()
            log_payload = json.dumps(radio_stats)
            log_response = log_result_obj.call_service(log_payload)
            log_id = log_response[AppMsgs.RESP]
            radio_stats[AppMsgs.ID_KEY] = log_id
            self.insert_ul_speed_in_summary(radio_stats)
            self.set_temp_speedtest_result(radio_stats)
            self.__process_radiostats_result(radio_stats)
       
#    Find upload key and set upload speed in context 
    def __process_radiostats_result(self, radiostats):

        if AppMsgs.UPLOAD_KEY in radiostats:
            upload_test_result = radiostats[AppMsgs.UPLOAD_KEY]
            if AppMsgs.PASSED in upload_test_result:
                self.error = ''
                self.context[AppMsgs.ERROR] = self.error
                self.context[AppMsgs.SPEEDTEST_RESULT] =  str(upload_test_result[AppMsgs.AVERAGE_KEY]) + AppMsgs.MBPS
                self.context[AppMsgs.SPEEDTEST_ID] = radiostats[AppMsgs.ID_KEY]
                summary_str = radiostats[AppMsgs.SUMMARY_KEY]
                summary_url = static_values.BaseUrl.LOG_RESULT_URL + ('&logid={0}'.format(radiostats[AppMsgs.ID_KEY]))
                radiostats[AppMsgs.SUMMARY_KEY] = summary_str + summary_url
                savesessiondata(AppMsgs.STATLIST_KEY, radiostats, self.request)
                self.set_temp_speedtest_result(None)
                time = str(datetime.now())
                self.set_last_speedtest_time(time)