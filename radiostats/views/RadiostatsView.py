from django.shortcuts import render
from radiostats.models import Radiostat
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import checksession,getfromSession, savesessiondata
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.utils import  get_selected_radio_stats
from airebeam_customer_portal.static_values import AppMsgs
from users.views.UserView import UserView
from airebeam_customer_portal.enum import AjaxCallStatus    

class RadiostatsView(UserView):
    template_name = 'radiostats/view_radiostats.html'
    
    def __init__(self):
        self.error = AppMsgs.NOT_FOUND_CONNECTION
        self.context = {}
        
    def get(self, request):
        if checksession(AppMsgs.MBRKEY, request):
            login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
            is_session_exceded = check_if_session_time_exceeds(login_time)
    #         Logout and redirect to login page
            if is_session_exceded:
                return HttpResponseRedirect(reverse('users:logout'))
            
            rs_download = rs_upload = rs_in_bytes = rs_out_bytes = rs_timestamp = \
                rs_lanstatus = rs_ping = rs_antenna = rs_uptime = rs_logid = \
                rs_download_settings = rs_upload_settings= [] 
                
            try:
                stats_requested_by_user = request.GET.get(AppMsgs.STATS_BY_CUSTOMER, None)
                stats_dict = self.__fetch_radiostats(stats_requested_by_user)
                if stats_dict is not None and AppMsgs.TIMESTAMP_KEY in stats_dict[0]:
                    self.error = ''
                    rs_download, rs_upload, rs_in_bytes, rs_out_bytes, rs_timestamp, rs_lanstatus, rs_ping, rs_antenna, rs_uptime, rs_logid,\
                    rs_download_settings, rs_upload_settings =  get_selected_radio_stats(stats_dict)

            except Exception as e:
                pass

            self.context = {AppMsgs.ERROR: self.error, AppMsgs.RADIOSTATS_DOWNLOAD:rs_download, AppMsgs.RADIOSTATS_UPLOAD:rs_upload, 
                       AppMsgs.RADIOSTATS_INBYTES:rs_in_bytes, AppMsgs.RADIOSTATS_OUTBYTES:rs_out_bytes,
                       AppMsgs.RADIOSTATS_TIMESTAMP:rs_timestamp, AppMsgs.RADIOSTATS_LANSTATUS:rs_lanstatus, 
                       AppMsgs.RADIOSTATS_PING:rs_ping, AppMsgs.RADIOSTATS_ANTENNA:rs_antenna, AppMsgs.RADIOSTATS_UPTIME:rs_uptime,
                       AppMsgs.RADIOSTATS_LOGID:rs_logid, AppMsgs.RADIOSTATS_DOWNLOAD_SETTINGS:rs_download_settings,
                       AppMsgs.RADIOSTATS_UPLOAD_SETTINGS:rs_upload_settings}
            return render(request, self.template_name, self.context)
        else:
            return HttpResponseRedirect(reverse('users:login'))
        
        
    def __fetch_radiostats(self, stats_requested_by_user):
        mbr = getfromSession(AppMsgs.MBRKEY, self.request)
        stats_dict = getfromSession(AppMsgs.STATLIST_KEY, self.request)
        if stats_dict is None or \
            (stats_requested_by_user is not None and stats_requested_by_user == '1'):
            r = Radiostat()
            stats_result = r.getStats(mbr, self.request)
            call_status = AjaxCallStatus.COMPLETED
            self.set_radiostats_call_status(call_status)
            
            if stats_dict is None:
                savesessiondata(AppMsgs.STATLIST_KEY, stats_result, self.request)
                stats_dict = getfromSession(AppMsgs.STATLIST_KEY, self.request)
            
            elif (stats_requested_by_user is not None and stats_requested_by_user == '1'):
                stats_dict = list(getfromSession(AppMsgs.STATLIST_KEY, self.request))
                stats_dict.insert(0, stats_result)
                self.set_temp_speedtest_result(stats_result)
        
        return stats_dict
