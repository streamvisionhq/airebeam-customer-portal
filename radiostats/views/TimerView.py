from django.http import HttpResponse
from airebeam_customer_portal.utils import checksession
from airebeam_customer_portal.utils import get_timer
from users.views.UserView import UserView
from airebeam_customer_portal.User import User
import json
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast

# Returns remaining time after last speedtest call. This time used as timer on frontend
class TimerView(UserView):
    def get(self, request):
        if checksession(AppMsgs.MBRKEY, request):
    #         Logout and redirect to login page
            last_speedtest_time = User.get_last_speedtest_time(request)
            if last_speedtest_time is not None:
                timer = get_timer(last_speedtest_time)
            else:
                timer = 0
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.TIMER_KEY:timer}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
             
        else:
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)