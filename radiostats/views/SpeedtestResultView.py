from django.shortcuts import render
from airebeam_customer_portal.utils import checksession,getfromSession,\
    savesessiondata
from airebeam_customer_portal.utils import convert_into_MB, get_selected_radio_stats
from airebeam_customer_portal.AppServices.SpeedTestService import SpeedTestService
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.enum import AjaxCallStatus
from airebeam_customer_portal.ServicePayload import ServicePayload
from users.views.UserView import UserView

class SpeedtestResultView(UserView):
    template_name = 'radiostats/view_radiostats.html'
    def get(self, request):
        if checksession(AppMsgs.MBRKEY, request):
#         Logout and redirect to login page
            error = AppMsgs.NOT_FOUND_CONNECTION
            stats_dict = {}
            try:
                stats_dict = getfromSession(AppMsgs.STATLIST_KEY, request)
                
                mbr = getfromSession(AppMsgs.MBRKEY, request)
    #             if stats_dict is None:
                st_service_object = SpeedTestService()
                payload = ServicePayload.get_speedtest_payload(mbr, AppMsgs.PERFORM_TEST)
                call_status = AjaxCallStatus.INPROGRESS
                self.set_speedtest_call_status(call_status)
                st_response = st_service_object.call_service(payload)
                stats = st_response[AppMsgs.RESP]
                savesessiondata(AppMsgs.STATLIST_KEY, stats, request)
                call_status = AjaxCallStatus.COMPLETED
                self.set_speedtest_call_status(call_status)
                stats_dict = getfromSession(AppMsgs.STATLIST_KEY, request)
                
                rs_download = rs_upload = rs_in_bytes = rs_out_bytes = rs_timestamp = \
                rs_lanstatus = rs_ping = rs_antenna = rs_uptime = rs_logid = \
                rs_download_settings = rs_upload_settings= []  
                 
                if AppMsgs.TIMESTAMP_KEY in stats_dict[0]:
                    error = ''
                    rs_download, rs_upload, rs_in_bytes, rs_out_bytes, rs_timestamp, rs_lanstatus, rs_ping, rs_antenna, rs_uptime, rs_logid,\
                    rs_download_settings, rs_upload_settings =  get_selected_radio_stats(stats_dict)
                
                    
            except Exception as e:
                pass
            
            context = {AppMsgs.ERROR: error, AppMsgs.RADIOSTATS_DOWNLOAD:rs_download, AppMsgs.RADIOSTATS_UPLOAD:rs_upload, 
                       AppMsgs.RADIOSTATS_INBYTES:rs_in_bytes, AppMsgs.RADIOSTATS_OUTBYTES:rs_out_bytes,
                       AppMsgs.RADIOSTATS_TIMESTAMP:rs_timestamp, AppMsgs.RADIOSTATS_LANSTATUS:rs_lanstatus, 
                       AppMsgs.RADIOSTATS_PING:rs_ping, AppMsgs.RADIOSTATS_ANTENNA:rs_antenna, AppMsgs.RADIOSTATS_UPTIME:rs_uptime,
                       AppMsgs.RADIOSTATS_LOGID:rs_logid, AppMsgs.RADIOSTATS_DOWNLOAD_SETTINGS:rs_download_settings,
                       AppMsgs.RADIOSTATS_UPLOAD_SETTINGS:rs_upload_settings}
            return render(request, self.template_name, context)
             
        else:
            error = AppMsgs.NOT_FOUND_CONNECTION
            stats_dict = {}
            context = {AppMsgs.RECENT_STATS_DICT: stats_dict, AppMsgs.ERROR: error}
            return render(request, self.template_name, context)
