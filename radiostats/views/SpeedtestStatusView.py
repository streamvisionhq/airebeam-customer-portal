from django.http import HttpResponse
import json
from airebeam_customer_portal.utils import checksession
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast

class SpeedtestStatusView(UserView):    
    def get(self, request):
        if checksession(AppMsgs.MBRKEY, request):
    #         Logout and redirect to login page
            call_status = self.get_speedtest_call_status()
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.CALL_STATUS:call_status}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
             
        else:
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.CALL_STATUS:0}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)