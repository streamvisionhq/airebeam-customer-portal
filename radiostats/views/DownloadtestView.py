from django.shortcuts import render
from radiostats.models import Radiostat
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.static_values import AppMsgs
from users.views.UserView import UserView
from airebeam_customer_portal.utils import checksession, getfromSession, \
    savesessiondata
import json


class DownloadtestView(UserView):
    template_name = 'radiostats/single_speedtest.html'
    
    def __init__(self):
        self.error = ''
        self.context = {}
    
    def get(self, request):
        if checksession(AppMsgs.MBRKEY, request):
            login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
            is_session_exceded = check_if_session_time_exceeds(login_time)
    #         Logout and redirect to login page
            if is_session_exceded:
                return HttpResponseRedirect(reverse('users:logout'))
            try:
                self.error = AppMsgs.NOT_FOUND_CONNECTION
                self.context = {AppMsgs.ERROR: self.error}
                self.__fetch_radiostats()
                
            except Exception as e:
                pass
            
            return render(request, self.template_name, self.context)
        else:
            return HttpResponseRedirect(reverse('users:login'))



    def insert_dl_speed_in_summary(self, radio_stats):
        summary_str = radio_stats[AppMsgs.SUMMARY_KEY]
        sub_str = AppMsgs.LOSS
        if summary_str.find(sub_str) != -1:
            summary_arr = summary_str.split(sub_str)
            dl_summary_str = '\r\n' + AppMsgs.RADIO_RX + str(radio_stats[AppMsgs.DOWNLOAD_KEY][AppMsgs.AVERAGE_KEY]) + AppMsgs.MBPS_DL_FLAG
            final_summary_str = summary_arr[0] + sub_str + dl_summary_str + summary_arr[1]
            radio_stats[AppMsgs.SUMMARY_KEY] = final_summary_str

#    Fetch and save radio stats in session
    def __fetch_radiostats(self):
        mbr = getfromSession(AppMsgs.MBRKEY, self.request)
        r = Radiostat()
        download_result = {}
        radio_stats = self.get_temp_speedtest_result()
        if radio_stats is not None:
            payload = json.dumps(radio_stats)
            download_result = r.getDownloadStats(mbr, self.request, payload)
            radio_stats[AppMsgs.DOWNLOAD_KEY] = download_result
            self.insert_dl_speed_in_summary(radio_stats) 
            self.set_temp_speedtest_result(radio_stats)
            self.__process_radiostats_result(radio_stats)

#    Find download key and set download speed and log id in context
    def __process_radiostats_result(self, radiostats):
        if AppMsgs.DOWNLOAD_KEY in radiostats:
            download_test_result = radiostats[AppMsgs.DOWNLOAD_KEY]
            if AppMsgs.PASSED in download_test_result:
                self.error = ''
                self.context[AppMsgs.ERROR] = self.error
                self.context[AppMsgs.SPEEDTEST_RESULT] = str(download_test_result[AppMsgs.AVERAGE_KEY]) + AppMsgs.MBPS
                self.context[AppMsgs.SPEEDTEST_ID] = ''
