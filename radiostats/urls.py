from django.conf.urls import url
from radiostats.views.SpeedtestResultView import SpeedtestResultView
from radiostats.views.OnRefreshSpeedtestView import OnRefreshSpeedtestView
from radiostats.views.SpeedtestStatusView import SpeedtestStatusView
from radiostats.views.RadiostatsView import RadiostatsView
from radiostats.views.TimerView import TimerView
from radiostats.views.RadiostatsStatusView import RadiostatsStatusView
from radiostats.views.DownloadtestView import DownloadtestView
from radiostats.views.UploadtestView import UploadtestView

app_name='radiostats'

urlpatterns = [
    url(r'^get_radiostats/$', RadiostatsView.as_view(), name='get_radiostats'),
    url(r'^download_stats/$', DownloadtestView.as_view(), name='download_stats'),
    url(r'^upload_stats/$', UploadtestView.as_view(), name='upload_stats'),
    url(r'^get_speedtest_timer/$', TimerView.as_view(), name='get_speedtest_timer'),
    url(r'^radstats/$', OnRefreshSpeedtestView.as_view(), name='radiostats'),
    url(r'^speedtest_result/$', SpeedtestResultView.as_view(), name='speedtest_result'),
    url(r'^is_speedtest_completed/$', SpeedtestStatusView.as_view(), name='is_speedtest_completed'),
    url(r'^is_radio_stats_completed/$', RadiostatsStatusView.as_view(), name='is_speedtest_completed'),


 ]