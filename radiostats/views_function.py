from django.shortcuts import render
from models import Radiostat
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
import json
from airebeam_customer_portal.utils import checksession,getfromSession,\
    savesessiondata
from airebeam_customer_portal.utils import check_if_session_time_exceeds, get_timer
from mbr_online_check.models import onlineCheck
from airebeam_customer_portal.utils import convert_into_MB, get_selected_radio_stats
from airebeam_customer_portal.AppServices.SpeedTestService import SpeedTestService
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.enum import AjaxCallStatus
from airebeam_customer_portal.User import User
from airebeam_customer_portal.ServicePayload import ServicePayload
from datetime import datetime

#  Delete this file if it is not used
def radiostats(request):
    if checksession(AppMsgs.MBRKEY, request):
        login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
        is_session_exceded = check_if_session_time_exceeds(login_time)
#         Logout and redirect to login page
        if is_session_exceded:
            return HttpResponseRedirect(reverse('users:logout'))
        error = AppMsgs.NOT_FOUND_CONNECTION
        try:
            mbr = getfromSession(AppMsgs.MBRKEY, request)
            stats_dict = getfromSession(AppMsgs.STATLIST_KEY, request)
            if stats_dict is None:
                r = Radiostat()
                r.getStats(mbr, request)
                stats_dict = getfromSession(AppMsgs.STATLIST_KEY, request)
            
            rs_download = rs_upload = rs_in_bytes = rs_out_bytes = rs_timestamp = \
            rs_lanstatus = rs_ping = rs_antenna = rs_uptime = rs_logid = \
            rs_download_settings = rs_upload_settings= [] 
            if stats_dict is not None and AppMsgs.TIMESTAMP_KEY in stats_dict[0]:
                    error = ''
                    rs_download, rs_upload, rs_in_bytes, rs_out_bytes, rs_timestamp, rs_lanstatus, rs_ping, rs_antenna, rs_uptime, rs_logid,\
                    rs_download_settings, rs_upload_settings =  get_selected_radio_stats(stats_dict)
            
        except Exception as e:
            pass
        context = {AppMsgs.ERROR: error, AppMsgs.RADIOSTATS_DOWNLOAD:rs_download, AppMsgs.RADIOSTATS_UPLOAD:rs_upload, 
                   AppMsgs.RADIOSTATS_INBYTES:rs_in_bytes, AppMsgs.RADIOSTATS_OUTBYTES:rs_out_bytes,
                   AppMsgs.RADIOSTATS_TIMESTAMP:rs_timestamp, AppMsgs.RADIOSTATS_LANSTATUS:rs_lanstatus, 
                   AppMsgs.RADIOSTATS_PING:rs_ping, AppMsgs.RADIOSTATS_ANTENNA:rs_antenna, AppMsgs.RADIOSTATS_UPTIME:rs_uptime,
                   AppMsgs.RADIOSTATS_LOGID:rs_logid, AppMsgs.RADIOSTATS_DOWNLOAD_SETTINGS:rs_download_settings,
                   AppMsgs.RADIOSTATS_UPLOAD_SETTINGS:rs_upload_settings}
        return render(request, 'radiostats/view_radiostats.html', context)
    else:
        return HttpResponseRedirect(reverse('users:login'))


# ajax callback function for radiostats
def radiostats_with_speedtest(request):
    if checksession(AppMsgs.MBRKEY, request):
        login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
        is_session_exceded = check_if_session_time_exceeds(login_time)
#         Logout and redirect to login page
        if is_session_exceded:
            return HttpResponseRedirect(reverse('users:logout'))
        error = AppMsgs.NOT_FOUND_CONNECTION
        try:
            mbr = getfromSession(AppMsgs.MBRKEY, request)
            r = Radiostat()
            payload = ServicePayload.get_speedtest_payload(mbr, AppMsgs.PERFORM_TEST)
            r.getStatsWithSpeedTest(payload, request)
            stats_dict = getfromSession(AppMsgs.STATLIST_KEY, request)
            time = str(datetime.now())
            User.set_last_speedtest_time(request, time)
            rs_download = rs_upload = rs_in_bytes = rs_out_bytes = rs_timestamp = \
            rs_lanstatus = rs_ping = rs_antenna = rs_uptime = rs_logid = \
            rs_download_settings = rs_upload_settings= []  
            if AppMsgs.TIMESTAMP_KEY in stats_dict[0]:
                    error = ''
                    rs_download, rs_upload, rs_in_bytes, rs_out_bytes, rs_timestamp, rs_lanstatus, rs_ping, rs_antenna, rs_uptime, rs_logid,\
                    rs_download_settings, rs_upload_settings =  get_selected_radio_stats(stats_dict)
            
        except Exception as e:
            User.set_last_speedtest_time(request, None)
            pass
        context = {AppMsgs.ERROR: error, AppMsgs.RADIOSTATS_DOWNLOAD:rs_download, AppMsgs.RADIOSTATS_UPLOAD:rs_upload, 
                   AppMsgs.RADIOSTATS_INBYTES:rs_in_bytes, AppMsgs.RADIOSTATS_OUTBYTES:rs_out_bytes,
                   AppMsgs.RADIOSTATS_TIMESTAMP:rs_timestamp, AppMsgs.RADIOSTATS_LANSTATUS:rs_lanstatus, 
                   AppMsgs.RADIOSTATS_PING:rs_ping, AppMsgs.RADIOSTATS_ANTENNA:rs_antenna, AppMsgs.RADIOSTATS_UPTIME:rs_uptime,
                   AppMsgs.RADIOSTATS_LOGID:rs_logid, AppMsgs.RADIOSTATS_DOWNLOAD_SETTINGS:rs_download_settings,
                   AppMsgs.RADIOSTATS_UPLOAD_SETTINGS:rs_upload_settings}
        return render(request, 'radiostats/view_radiostats.html', context)
    else:
        return HttpResponseRedirect(reverse('users:login'))
    
    
def get_speedtest_remaining_time(request):
    if checksession(AppMsgs.MBRKEY, request):
#         Logout and redirect to login page
        last_speedtest_time = User.get_last_speedtest_time(request)
        if last_speedtest_time is not None:
            timer = get_timer(last_speedtest_time)
        else:
            timer = 0
        response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.TIMER_KEY:timer}
        response_dict = json.dumps(response_dict)
        return HttpResponse(response_dict, content_type=AppMsgs.CONTENT_TYPE_VALUE)
         
    else:
        response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE}
        response_dict = json.dumps(response_dict)
        return HttpResponse(response_dict, content_type=AppMsgs.CONTENT_TYPE_VALUE)

# This function is no longer used
# ajax callback function for recent radiostats
def getrecent(request):
    if checksession(AppMsgs.MBRKEY, request):
        login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
        is_session_exceded = check_if_session_time_exceeds(login_time)
#         Logout and redirect to login page
        if is_session_exceded:
            return HttpResponseRedirect(reverse('users:logout'))
        error = 'no recent stats.'
        recent_stats_dict = []
        try:
            recent_stats_dict = getfromSession(AppMsgs.STATLIST_KEY, request)
        except:
            pass
        context = {AppMsgs.RECENT_STATS_DICT: recent_stats_dict, AppMsgs.ERROR: error}
        return render(request, 'radiostats/recent_radiostats.html', context)
    else:
        return HttpResponseRedirect(reverse('users:login'))
    
def get_speedtest_results(request):
    if checksession(AppMsgs.MBRKEY, request):
#         Logout and redirect to login page
        error = AppMsgs.NOT_FOUND_CONNECTION
        stats_dict = {}
        try:
            stats_dict = getfromSession(AppMsgs.STATLIST_KEY, request)
            
            mbr = getfromSession(AppMsgs.MBRKEY, request)
            onlinecheck = getfromSession(AppMsgs.ONLINECHECK_KEY, request)
#             if stats_dict is None:
            st_service_object = SpeedTestService()
            payload = ServicePayload.get_speedtest_payload(mbr, AppMsgs.PERFORM_TEST)
            call_status = AjaxCallStatus.INPROGRESS
            User.set_speedtest_call_status(request, call_status)
            st_response = st_service_object.call_service(payload)
            stats = st_response[AppMsgs.RESP]
            savesessiondata(AppMsgs.STATLIST_KEY, stats, request)
            call_status = AjaxCallStatus.COMPLETED
            User.set_speedtest_call_status(request, call_status)
            
            onlinecheck = getfromSession(AppMsgs.ONLINECHECK_KEY, request)
            if onlinecheck is not None and 'INBYTES' in onlinecheck and type(onlinecheck['INBYTES']) != 'str':
                in_bytes = onlinecheck['INBYTES']
                out_bytes = onlinecheck['OUTBYTES']
                onlinecheck['in_bytes'] = convert_into_MB(in_bytes)
                onlinecheck['out_bytes'] = convert_into_MB(out_bytes)
                
            stats_dict = getfromSession(AppMsgs.STATLIST_KEY, request)
            rs_download = rs_upload = rs_in_bytes = rs_out_bytes = rs_timestamp = rs_lanstatus = rs_ping = rs_antenna = rs_uptime = rs_logid = [] 
            if AppMsgs.TIMESTAMP_KEY in stats_dict[0]:
                error = ''
                rs_download, rs_upload, rs_in_bytes, rs_out_bytes, rs_timestamp, rs_lanstatus, rs_ping, rs_antenna, rs_uptime, rs_logid =  get_selected_radio_stats(stats_dict, onlinecheck)
            
                
        except Exception as e:
            pass
        
        context = {AppMsgs.ERROR: error, AppMsgs.RADIOSTATS_DOWNLOAD:rs_download, AppMsgs.RADIOSTATS_UPLOAD:rs_upload, 
                   AppMsgs.RADIOSTATS_INBYTES:rs_in_bytes, AppMsgs.RADIOSTATS_OUTBYTES:rs_out_bytes,
                   AppMsgs.RADIOSTATS_TIMESTAMP:rs_timestamp, AppMsgs.RADIOSTATS_LANSTATUS:rs_lanstatus, 
                   AppMsgs.RADIOSTATS_PING:rs_ping, AppMsgs.RADIOSTATS_ANTENNA:rs_antenna, AppMsgs.RADIOSTATS_UPTIME:rs_uptime,
                   AppMsgs.RADIOSTATS_LOGID:rs_logid}
        return render(request, 'radiostats/view_radiostats.html', context)
         
    else:
        error = AppMsgs.NOT_FOUND_CONNECTION
        stats_dict = {}
        context = {AppMsgs.RECENT_STATS_DICT: stats_dict, AppMsgs.ERROR: error}
        return render(request, 'radiostats/view_radiostats.html', context)
    
def is_speedtest_completed(request):
    if checksession(AppMsgs.MBRKEY, request):
#         Logout and redirect to login page
        call_status = User.get_speedtest_call_status(request)
        response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.CALL_STATUS:call_status}
        response_dict = json.dumps(response_dict)
        return HttpResponse(response_dict, content_type=AppMsgs.CONTENT_TYPE_VALUE)
         
    else:
        response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.CALL_STATUS:0}
        response_dict = json.dumps(response_dict)
        return HttpResponse(response_dict, content_type=AppMsgs.CONTENT_TYPE_VALUE)
    
    
# This can be used in future     
def is_stats_exist(request):
    if checksession(AppMsgs.MBRKEY, request):
#         Logout and redirect to login page
        stats_dict = getfromSession(AppMsgs.STATLIST_KEY, request)
        if stats_dict is not None and len(stats_dict):
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.STATUS_KEY:1, 'rstats_exist':1}
        else:
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.STATUS_KEY:0, 'rstats_exist':0}
        
        response_dict = json.dumps(response_dict)
        return HttpResponse(response_dict, content_type=AppMsgs.CONTENT_TYPE_VALUE)
         
    else:
        response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.STATUS_KEY:0, 'rstats_exist':0}
        response_dict = json.dumps(response_dict)
        return HttpResponse(response_dict, content_type=AppMsgs.CONTENT_TYPE_VALUE)