from __future__ import unicode_literals
import requests
import json
from django.db import models
from airebeam_customer_portal.utils import savesessiondata, getfromSession
import os
from airebeam_customer_portal import settings, config
from datetime import datetime
from airebeam_customer_portal.AppServices.SpeedTestService import SpeedTestService
from airebeam_customer_portal.AppUrl import AppUrl
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.enum import AppStatus
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod

class Radiostat():
    radio_stats_dir_path = 'user_radio_stats'
    stats={}
    key = config.BackofficeApiKey

# Make service and move this method
    def getStats(self,mbr,request):
        url       = 'https://b.airebeam.com/airecheck?radio=' + str(mbr)
        headers   = {AppMsgs.AUTHORIZATION_KEY: AppMsgs.BEARER + self.key}
        response  = requests.get(url, headers=headers)
        if response.status_code == 200:
            stats_result_json = json.loads(response.content)
            params = ""
            Logger.log_api(RequestMethod.GET, response.status_code, 
                           response.url, params, response.text)
            return stats_result_json
        else:
#             Handle error
            pass
        
#         self.stats=self.saveresponse(response,request)
# Make service and move this method        
    def getDownloadStats(self,mbr,request, payload):
        url       = 'https://b.airebeam.com/airecheck&xhr=true&download=true&customertest=true&portal=true' 
        headers   = {AppMsgs.AUTHORIZATION_KEY: AppMsgs.BEARER + self.key, AppMsgs.CONTENT_TYPE_KEY:AppMsgs.CONTENT_TYPE_VALUE}
        response  = requests.post(url, headers=headers, data=payload)        
        if response.status_code == 200:
            download_result_json = json.loads(response.content)
            params = payload
            Logger.log_api(RequestMethod.POST, response.status_code, 
                           response.url, params, response.text)
            return download_result_json
        else:
#             Handle error
            pass
        
    def getUploadStats(self,mbr,request, payload):
        url       = 'https://b.airebeam.com/airecheck&xhr=true&upload=true&customertest=true&portal=true'
        headers   = {AppMsgs.AUTHORIZATION_KEY: AppMsgs.BEARER + self.key, AppMsgs.CONTENT_TYPE_KEY:AppMsgs.CONTENT_TYPE_VALUE}
        response  = requests.post(url, headers=headers, data=payload)
        if response.status_code == 200:
            upload_result_json = json.loads(response.content)
            params = payload
            Logger.log_api(RequestMethod.POST, response.status_code, 
                           response.url, params, response.text)
            return upload_result_json
        else:
#             Handle error
            pass
        
    def getStatsWithSpeedTest(self, payload, request):
        st_service_object = SpeedTestService()
        st_response = st_service_object.call_service(payload)
        stats = st_response[AppMsgs.RESP]
        if st_response[AppMsgs.MSG] == AppStatus.SUCCESS:
            savesessiondata(AppMsgs.STATLIST_KEY, stats, request)

    def getapiKey(self):
        return self.key
    
    def saveresponse(self,response,request):
        if response.status_code == 200:
            stats = json.loads(response.content)
            savesessiondata(AppMsgs.STATLIST_KEY, stats, request)
            return stats
        else:
            return 'unable to get data'
        
    def get_stats_str(self, request):
        recent_stats_dict = getfromSession(AppMsgs.STATLIST_KEY, request)
        stats_summary = ''
        for rs in recent_stats_dict:
            if AppMsgs.TIMESTAMP_KEY in rs:
                
                timestamp_title = 'Radio stats taken at {0}'.format(rs[AppMsgs.TIMESTAMP_KEY])
                lt = len(timestamp_title)
                separator_str = AppMsgs.BOLD_CONTAINER.format('*' * len(timestamp_title))
                timestamp_str = AppMsgs.BOLD_CONTAINER.format(timestamp_title)
                summary = rs[AppMsgs.SUMMARY_KEY]
                summary = summary.split('\r\n')
                summary_lst = []
                for sum in summary:
                    if sum != '':
                        summary_lst.append('<div>{0}</div>'.format(sum))
                    else:
                        summary_lst.append('<br/>')
                         
                summary_str = ''.join(summary_lst)
                stats_summary = stats_summary + separator_str + timestamp_str + summary_str
            else:
                stats_summary = '<div class="stats-failed-msg">Radio Stats/Speed Test Failed</div>'
                break
        return stats_summary

#   This method is no longer used      
    def make_stats_file(self, request):
        recent_stats_dict = getfromSession(AppMsgs.STATLIST_KEY, request)
        radio_stats_path = os.path.join(settings.BASE_DIR, self.radio_stats_dir_path)
        radio_stats_path = os.path.join(radio_stats_path, self.__get_radiostats_formatted_filename(request))
        with open(radio_stats_path, 'w') as f:
            for rs in recent_stats_dict:
                if AppMsgs.TIMESTAMP_KEY in rs:
                    f.write('Radio stats taken at %s\n' % rs[AppMsgs.TIMESTAMP_KEY])
                    f.write('\r\n')
                    f.write(rs[AppMsgs.SUMMARY_KEY])
                f.write('\r\n')

#   This method is no longer used                
    def get_file_path(self, request):
        radio_stats_path = os.path.join(settings.BASE_DIR, self.radio_stats_dir_path)
        radio_stats_path = os.path.join(radio_stats_path, self.__get_radiostats_formatted_filename(request))
        if os.path.isfile(radio_stats_path):
            return  radio_stats_path
        else:
            return ''

#   This method is no longer used        
    def __get_radiostats_formatted_filename(self, request):
        mbr = getfromSession(AppMsgs.MBRKEY, request)
        mbr = str(mbr)
        now_time = datetime.now().time()
        time_str = now_time.strftime('%H%M%S')
        radio_stats_filename = 'radio_stats_' + mbr + '_' + time_str + '.txt'
        return radio_stats_filename
