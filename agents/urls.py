from django.conf.urls import url

from agents.views.AgentLoginView import AgentLoginView

app_name = 'agents'
urlpatterns = [
    url(r'login/$', AgentLoginView.as_view(), name = 'index'),
]