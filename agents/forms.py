from django import forms
 
class AgentLoginForm(forms.Form):
    username = forms.CharField(max_length=100, label='Username', 
                          widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Password'}))
    mbr = forms.CharField(max_length=100, label='Account Number', 
                          widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Account Number'}))