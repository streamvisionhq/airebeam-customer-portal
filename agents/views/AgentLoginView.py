from django.shortcuts import render, redirect
from agents.forms import AgentLoginForm
from airebeam_customer_portal.enum import user_access_level, ForgotPasswordStatus, AppStatus,\
    AjaxCallStatus
from airebeam_customer_portal.utils import savesessiondata, checksession
from mbr_online_check.models import mbrDetail
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
import datetime
from airebeam_customer_portal.AppServices.ForgotPasswordService import ForgotPasswordService
import base64
from airebeam_customer_portal.static_values import AppMsgs, MbrDetailKeys,\
    ExceptionMsgs
from airebeam_customer_portal.ServicePayload import ServicePayload
from users.views.UserView import UserView
from tickets.enum import TicketStatus, TicketPriority
import json
from airebeam_customer_portal.AppServices.TicketService import TicketService
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from airebeam_customer_portal.AppServices.GCaptchaservice import GCaptchaservice
from airebeam_customer_portal import config
from airebeam_customer_portal.AppServices.AgentLoginService import AgentLoginService
from airebeam_customer_portal.AppServices.LoginService import LoginService

class AgentLoginView(UserView):
    '''
    Login view for operators.
    '''
    form_class = AgentLoginForm
    form_template_name = 'agents/agent_login_form.html'
    exception_template = 'abcp_exception.html'
    
    def __init__(self):
        self.error = ''
        self.username = ''
        self.password = ''
        self.mbr = ''
        
    def get(self, request):
        '''
        Shows operator login form.
        @param request:
        '''
        try:
            if checksession(AppMsgs.MBRKEY,request) or \
            self.get_agent_username() is not None:
                return redirect('dashboard:index')
            self.error = ''
            form = self.form_class()
            context = {AppMsgs.FORMKEY: form, AppMsgs.ERROR: self.error}
            return render(request, self.form_template_name, context)
        
        except Exception as e:
            message = ExceptionMsgs.GENERAL_EXCEPTION
            except_dict = { AppMsgs.EXCEPT_MSG:message}   
            return render(request, self.exception_template, except_dict)
        
    def post(self, request):
        '''
        Process form parameters and redirect to dashboard .
        @param request:
        '''
        try:
            self.error = ''
            form = self.form_class(request.POST)
            if form.is_valid():
                self.__set_form_fields(form)
                if(self.__login_agent_into_portal()):
                    return redirect('dashboard:index')
                            
            return render(request, self.form_template_name, {
                    AppMsgs.FORMKEY: form, AppMsgs.ERROR: self.error
                })
        except Exception as e:
            message = ExceptionMsgs.GENERAL_EXCEPTION
            except_dict = { AppMsgs.EXCEPT_MSG:message}   
            return render(request, self.exception_template, except_dict)
    
    def __set_form_fields(self, form):
        self.username = form.data[AppMsgs.USERNAME_KEY]
        self.password = form.data[AppMsgs.PASSWORD_KEY]
        self.mbr = form.data[AppMsgs.MBRKEY]

    def __login_agent_into_portal(self):
        '''
        Method calls login service and set data for operator in session
        '''
        agent_login_obj = AgentLoginService()
        payload = ServicePayload.get_agent_login_service_payload(self.username, self.password)
        agent_login_response = agent_login_obj.call_service(payload)
        if agent_login_response[AppMsgs.RESP][AppMsgs.AUTHORIZED_KEY]:
            self.__set_agent_data()
            mbr = self.mbr
            mbr_detail_obj = LoginService.fetch_mbr_details(self, mbr)
            if mbr_detail_obj.detail == AppMsgs.ERROR:
                return True
            else:
                savesessiondata(AppMsgs.MBRKEY, mbr, self.request)
                self.request.session[AppMsgs.USERNAME_KEY] = mbr_detail_obj.detail[MbrDetailKeys.FIRSTNAME_KEY]
                LoginService.search_customer_on_fd_by_phone(self, self.mbr)
                return True
        else:
            self.error = AppMsgs.ACCOUNT_NOT_FOUND

        return False

    def __set_agent_data(self):
        '''
        Store operator username, access level and login time in session.
        '''
        self.set_agent_username(self.username)
        user_acl = user_access_level[AppMsgs.SHALLOW_KEY]
        self.set_access_level(user_acl)
        login_time = str(datetime.datetime.now())
        savesessiondata(AppMsgs.LOGIN_TIME_KEY, login_time, self.request)



