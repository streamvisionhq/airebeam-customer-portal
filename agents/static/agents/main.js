$( document ).ready(function() {
	$( "#id_username" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     submit_form();
		}
	});
	
	$( "#id_password" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     submit_form();
		}
	});
	
	$( "#id_mbr" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     submit_form();
		}
	});
	
});

//Write this because we want to disable submit button
//Submit button makes problem if we hit more than one
function submit_form(){
	var $fp_form = $('#agent_login_form');
	var $fp_submit_btn = $('#agent_submit_btn');
	$fp_submit_btn.prop('disabled', true);
	$fp_form.submit();
}

function return_to_login(){
	window.location = '/'
}
