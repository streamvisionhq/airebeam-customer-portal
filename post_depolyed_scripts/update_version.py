#!/usr/bin/python

import sys

FILE_PATH = "../project_static/version.txt"

def update_third_num():
    ver_third_elem = get_updated_third_version()
    write_version_to_file(ver_third_elem)


def update_last_num():
    ver_without_last_elem = get_updated_last_version()
    write_version_to_file(ver_without_last_elem)

def write_version_to_file(ver_without_last_elem):
    with open(FILE_PATH, "w") as f:
        f.write(ver_without_last_elem)

def get_updated_last_version():
    with open(FILE_PATH) as f:
        data = f.read()
        ver_arr = data.split('.')
        new_ver = int(ver_arr[3]) + 1
        ver_arr[3] = str(new_ver)
    updated_version = '.'.join(ver_arr)
    return updated_version 

def get_updated_third_version():
    with open(FILE_PATH) as f:
        data = f.read()
        ver_arr = data.split('.')
        new_ver = int(ver_arr[2]) + 1
        ver_arr[2] = str(new_ver)
        ver_arr[3] = str(0)
    updated_version = '.'.join(ver_arr)
    return updated_version

def process_args(argv):
    print '\n'
    try:
        if argv[0] == '-h':
            print 'python update_version.py [-ulast] [-uthird]'
        elif argv[0] in ['-ulast', '--update-last']:
            update_last_num()
            print 'Increased last version number'
        elif argv[0] in ['-uthird', '--update-third']:
            update_third_num()
            print 'Increased third version number'
        else:
            print 'python update_version.py [-ulast] [-uthird]'
            
    except:
        print 'python update_version.py [-ulast] [-uthird]'
        sys.exit(2)

if __name__ == "__main__":
    process_args(sys.argv[1:])
