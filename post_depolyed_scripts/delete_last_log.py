#!/usr/bin/python

import sys
import os
from datetime import date
from datetime import datetime, timedelta

FILE_PATH = "../ActivityLogs/"

previous_seven_day_date = datetime.today() - timedelta(days=7)
previous_seven_day_date = str(previous_seven_day_date.year) + '-' + str(previous_seven_day_date.month) + '-' + str(previous_seven_day_date.day)
previous_seven_day_file_path = FILE_PATH + previous_seven_day_date + '-logs.csv'
if(os.path.exists(previous_seven_day_file_path)):
    os.remove(previous_seven_day_file_path)
