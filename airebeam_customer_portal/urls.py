"""airebeam_customer_portal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url,include
from django.contrib import admin
from users.views.Error404View import Error404View
handler404 = Error404View.as_view()

urlpatterns = [
    url(r'^change_password/', include('change_password.urls')),
    url(r'^reset_password/', include('reset_password.urls')),
    url(r'^terms/', include('terms_and_conditions.urls')),
    url(r'^operator/', include('agents.urls')),
    url(r'^contact_us/', include('contact_us.urls')),
    url(r'^forgot_password/', include('forgot_password.urls')),
    url(r'^users/', include('users.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^radiostats/', include('radiostats.urls')),
    url(r'^mbr_online_check/', include('mbr_online_check.urls')),
    url(r'^tickets/', include('tickets.urls')),
    url(r'^', include('dashboard.urls')),

] 

