from enum import AppStatus
from static_values import ForgotPasswordPayload
from static_values import SpeedtestPayload
from static_values import CreateReplyPayload
from airebeam_customer_portal.static_values import UpdatedTicketPayload,\
    CustomerInfoPayload
from airebeam_customer_portal.enum import FdMapLevel
from airebeam_customer_portal.static_values import AppMsgs

class ServicePayload:
    @staticmethod
    def get_forgot_password_service_payload(addressText, phoneNumberText, lastFourCcDigitText):
        return {ForgotPasswordPayload.ADDRESS_KEY: addressText, 
                ForgotPasswordPayload.PHONE_KEY:phoneNumberText, 
                ForgotPasswordPayload.CREDIT_CARD:lastFourCcDigitText}

    @staticmethod
    def get_speedtest_payload(mbr, performTest='false'):
        return {SpeedtestPayload.ACCOUNT_KEY:mbr, SpeedtestPayload.SPEEDTEST_KEY:performTest}
    
    @staticmethod
    def get_create_reply_payload(replyBody, fdCustomerId):
        return {CreateReplyPayload.BODY_KEY: replyBody, CreateReplyPayload.USER_ID_KEY:fdCustomerId}
    
    @staticmethod
    def get_updated_ticket_payload(isRead="0"):
        return {UpdatedTicketPayload.CUSTOM_FIELDS_KEY : { UpdatedTicketPayload.IS_READ_KEY: isRead}}
    
    @staticmethod
    def get_create_ticket_payload(fdGroup, fdType, fdPriority, ticketBody, subject, status, fdCustomerId, customFields):
        payload = {AppMsgs.GROUP_ID: fdGroup, AppMsgs.TYPE_KEY:fdType,
                    AppMsgs.PRIORITY: fdPriority, AppMsgs.DESCRIPTION:ticketBody, 
                    AppMsgs.SUBJECT_KEY: subject,
                     AppMsgs.STATUS_KEY: status, 
                    AppMsgs.REQUESTER_ID:fdCustomerId,
                    UpdatedTicketPayload.CUSTOM_FIELDS_KEY: customFields
                    }
        return payload

    @staticmethod
    def get_general_ticket_payload(fdPriority, ticketBody, subject, status, email, fdGroup, fdType):
        payload = {AppMsgs.PRIORITY: fdPriority, AppMsgs.DESCRIPTION:ticketBody, 
                    AppMsgs.SUBJECT_KEY: subject,
                     AppMsgs.STATUS_KEY: status, 
                    AppMsgs.EMAIL:email,
                    AppMsgs.GROUP_ID: fdGroup, AppMsgs.TYPE_KEY:fdType,
                    }
        return payload
    
    @staticmethod
    def get_create_customer_payload(name, mbr, email, address, phoneHome, mobile):
        payload = { AppMsgs.NAME: name, ForgotPasswordPayload.PHONE_KEY:mbr}
        custom_fields = {AppMsgs.MAPPING_LEVEL_KEY: str(FdMapLevel.PORTAL_RELIABLE_CONTACT)}
        if email is not None:
            payload[AppMsgs.EMAIL] = email
        if address is not None:
            payload[ForgotPasswordPayload.ADDRESS_KEY] = address
        if mobile is not None:
            payload[AppMsgs.MOBILE] = mobile
        if phoneHome is not None:
            custom_fields[AppMsgs.PHONE_META] = phoneHome
        
        if len(custom_fields):    
            payload[UpdatedTicketPayload.CUSTOM_FIELDS_KEY] = custom_fields
        return payload
    
    @staticmethod
    def get_captcha_payload(secret,  gCaptchaResponse):
        payload = {AppMsgs.SECRET: secret, AppMsgs.RESP:gCaptchaResponse}
        return payload
    
    @staticmethod
    def get_agent_login_service_payload(username, password):
        payload = {AppMsgs.EMERALD_USER:username, AppMsgs.EMERALD_PASSWORD:password}
        return payload
    
    @staticmethod
    def get_customer_info_service_payload(mbr):
        return {CustomerInfoPayload.MBR: mbr}
    
    @staticmethod
    def get_update_password_payload(mbr,password,key,medium):
        payload = {AppMsgs.MBRKEY: mbr, AppMsgs.PASSWORD_KEY:password, 
                    AppMsgs.RESET_PASS_KEY: key,
                     AppMsgs.RESET_PASS_MEDIUM: medium
                    }
        return payload
    
    @staticmethod
    def get_customer_contact_info_payload(fdPriority, ticketBody, subject, status, fdCustomerId, fdGroup, fdType):
        payload = {AppMsgs.PRIORITY: fdPriority, AppMsgs.DESCRIPTION:ticketBody, 
                    AppMsgs.SUBJECT_KEY: subject,
                     AppMsgs.STATUS_KEY: status, 
                    AppMsgs.REQUESTER_ID:fdCustomerId,
                    AppMsgs.GROUP_ID: fdGroup, AppMsgs.TYPE_KEY:fdType,
                    }
        return payload
    
    @staticmethod
    def get_not_my_tickets_payload(fdPriority, ticketBody, subject, status, fdCustomerId, fdGroup, fdType):
        payload = {AppMsgs.PRIORITY: fdPriority, AppMsgs.DESCRIPTION:ticketBody, 
                    AppMsgs.SUBJECT_KEY: subject,
                     AppMsgs.STATUS_KEY: status, 
                    AppMsgs.REQUESTER_ID:fdCustomerId,
                    AppMsgs.GROUP_ID: fdGroup, AppMsgs.TYPE_KEY:fdType,
                    }
        return payload
    
    @staticmethod
    def get_change_password_payload(mbr, oldPassword, newPassword):
        payload = {AppMsgs.MBRKEY : mbr, AppMsgs.OLD_PASSWORD_KEY : oldPassword, AppMsgs.NEW_PASSWORD_KEY : newPassword}
        return payload