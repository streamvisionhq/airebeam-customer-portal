class switch(object):
    value = None
    def __new__(cls, value):
        cls.value = value
        return True

def case(*args):
    return any((arg == switch.value for arg in args))