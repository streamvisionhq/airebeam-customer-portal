from requests.auth import HTTPBasicAuth
import os
import config_local

StaticFileDirs = config_local.StaticfilesDirs
StaticRoot = config_local.StaticRoot
Auth = HTTPBasicAuth(config_local.FreshdeskEmail, config_local.FreshdeskPassword)

EmeraldCredentials = config_local.EmeraldCredentials

Database = config_local.DatabaseConfig 

CaptchaSecret = config_local.CaptchaSecret
CaptchaKey = config_local.CaptchaKey
BackofficeApiKey = config_local.BackofficeApiKey
PdfUrl = config_local.PdfUrl
TestStr = config_local.TestStr
HidePublicNotes = config_local.HidePublicNotes