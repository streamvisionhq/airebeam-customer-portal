from airebeam_customer_portal.enum import AjaxCallStatus
from airebeam_customer_portal.static_values import AppMsgs

class User(object):
    @staticmethod
    def set_access_level(accessLevel, request):
        request.session[AppMsgs.USER_ACCESS_LEVEL_KEY] = accessLevel

    @staticmethod    
    def get_access_level(request):
        return request.session[AppMsgs.USER_ACCESS_LEVEL_KEY]
    
    @staticmethod
    def set_speedtest_call_status(request, callStatus):
        request.session[AppMsgs.SPEEDTEST_CALL_STATUS_KEY] = callStatus
    
    @staticmethod
    def get_speedtest_call_status(request):
        return request.session[AppMsgs.SPEEDTEST_CALL_STATUS_KEY]
    
    @staticmethod
    def set_last_speedtest_time(request, time):
        request.session[AppMsgs.LAST_SPEEDTEST_TIME_KEY] = time
    
    @staticmethod
    def get_last_speedtest_time(request):
        if AppMsgs.LAST_SPEEDTEST_TIME_KEY in request.session:
            return request.session[AppMsgs.LAST_SPEEDTEST_TIME_KEY]
        else:
            return None