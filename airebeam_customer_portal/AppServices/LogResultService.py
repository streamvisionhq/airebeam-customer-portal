import requests
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from airebeam_customer_portal import config
from airebeam_customer_portal.static_values import AppMsgs
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod

class LogResultService(object):
    key = config.BackofficeApiKey
    
    def __init__(self):
        pass
        
    def call_service(self, payload):            
        try:
            url = AppUrl.get_log_result_url()
            headers   = {AppMsgs.AUTHORIZATION_KEY: AppMsgs.BEARER + self.key, AppMsgs.CONTENT_TYPE_KEY:AppMsgs.CONTENT_TYPE_VALUE}
            log_result_request = requests.post(url, data=payload, headers=headers)
            status = log_result_request.status_code
            response_obj = log_result_request.json()
            generated_response = ""
            
            if status == requests.codes.ok:
                generated_response = AppResponse.get_success_response(response_obj)
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = payload
            Logger.log_api(RequestMethod.POST, status, 
                           log_result_request.url, params, log_result_request.text)
            return generated_response
        except:
            return AppResponse.get_exception_reponse()
    