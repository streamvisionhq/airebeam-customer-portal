import requests
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod

class CustomerInfoService(object):
    
    def __init__(self):
        pass
        
    def call_service(self, payload):            
        try:
            url = AppUrl.get_customer_info_url()
            ci_request = requests.post(url, data=payload)
            status = ci_request.status_code
            response_obj = ci_request.json()
            generated_response = ""
            
            if status == requests.codes.ok:
                generated_response = AppResponse.get_success_response(response_obj)
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = payload
            Logger.log_api(RequestMethod.POST, status, ci_request.url, params, ci_request.text)
            return generated_response
        except:
            return AppResponse.get_exception_reponse()
    