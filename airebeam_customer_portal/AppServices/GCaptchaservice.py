import requests
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod

class GCaptchaservice(object):
    
    def __init__(self):
        pass
        
    def verify_captcha(self, payload):            
        try:
            url = AppUrl.get_verify_captcha_url()
            gcaptcha_request = requests.post(url, data=payload)
            status = gcaptcha_request.status_code
            generated_response = ""
            
            if status == requests.codes.ok:
                response_obj = gcaptcha_request.json()
                generated_response = AppResponse.get_success_response(response_obj)
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = payload
            Logger.log_api(RequestMethod.POST, status, 
                           gcaptcha_request.url, params, gcaptcha_request.text)
            return generated_response
            
        except:
            return AppResponse.get_exception_reponse()
    