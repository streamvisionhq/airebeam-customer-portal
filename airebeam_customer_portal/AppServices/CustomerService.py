import requests
import xmltodict
import json
from airebeam_customer_portal import config
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from airebeam_customer_portal.enum import FdMapLevel
from airebeam_customer_portal.static_values import AppMsgs, UpdatedTicketPayload
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod

class CustomerService(object):
    url = ''
    auth = config.Auth
    headers = {AppMsgs.CONTENT_TYPE_KEY: AppMsgs.CONTENT_TYPE_VALUE}
    def __init__(self):
        pass
        
    def get_fd_customer_by_phone(self, mbr):            
        try:
            url = AppUrl.get_search_customer_url(mbr)
            contact_request = requests.get(url, auth=self.auth, headers=self.headers)
            generated_response = ""
            
            if contact_request.status_code == requests.codes.ok and contact_request.json():
                contact_info_lst = contact_request.json()
                contact_info = []
                for c_info in contact_info_lst:
                    mapping_level = int(c_info[UpdatedTicketPayload.CUSTOM_FIELDS_KEY][AppMsgs.MAPPING_LEVEL_KEY])
                    if  mapping_level == FdMapLevel.RELIABLE_CONTACT or \
                        mapping_level == FdMapLevel.PORTAL_RELIABLE_CONTACT:
                        contact_info = [c_info]
                        break
                    
                if len(contact_info) == 1:
                    generated_response = AppResponse.get_success_response(contact_info)
                else:
                    generated_response = AppResponse.get_failure_reponse()
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = ""
            Logger.log_api(RequestMethod.GET, contact_request.status_code, 
                           contact_request.url, params, contact_request.text)
            return generated_response
        except:
            return AppResponse.get_exception_reponse()
        
    def create_customer(self, payload):
        try:
            url = AppUrl.get_customer_url()
            customer_create_request = requests.post(url, auth=self.auth, data=payload, 
                                                   headers=self.headers)
            generated_response = ""
            
            if customer_create_request.status_code == 201:
                customer = customer_create_request.json()
                generated_response = AppResponse.get_success_response(customer)
                
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = payload
            Logger.log_api(RequestMethod.POST, customer_create_request.status_code, 
                           customer_create_request.url, params, customer_create_request.text)
            return generated_response
        except:
            return AppResponse.get_exception_reponse()

    def one_time_credit_card_charge(self, payload):
        url = "https://billing.airebeam.com/api.ews"
        response_obj = requests.post(url, data=payload, headers=self.headers)

        response = ""

        if response_obj.status_code == 200:
            xmldata     = response_obj.content
            data        = xmltodict.parse(xmldata)
            response    = data
        else:
            response    = AppResponse.get_failure_reponse()

        return response