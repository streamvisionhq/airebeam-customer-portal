import requests
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.enum import AppStatus
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod

class UserEmeraldServicesApi(object):
    url = ''

    def __init__(self, url):
        self.url = url
        
    def call_service(self):            
        try:
            user_services_request = requests.get(self.url)
            status = user_services_request.status_code
            response_obj = user_services_request.json()
            generated_response = ""
            if status == requests.codes.ok:
                generated_response = AppResponse.get_success_response(response_obj)
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = ""
            Logger.log_api(RequestMethod.GET, status, 
                           user_services_request.url, params, user_services_request.text)
            return generated_response
        except:
            return AppResponse.get_exception_reponse()
        