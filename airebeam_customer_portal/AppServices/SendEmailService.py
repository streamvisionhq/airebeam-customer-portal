import requests
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod
from airebeam_customer_portal.enum import Protocol

class SendEmailService(object):
    
    def __init__(self):
        pass
        
    def call_service(self, mbr, resetPageUrl):            
        try:
            url = AppUrl.get_send_email_url(mbr, resetPageUrl)
            send_email_request = requests.get(url)
            status = send_email_request.status_code
            response_obj = send_email_request.json()
            generated_response = ""
            
            if status == requests.codes.ok:
                generated_response = AppResponse.get_success_response(response_obj)
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = ""
            Logger.log_api(RequestMethod.GET, status, 
                           send_email_request.url, params, send_email_request.text)
            return generated_response
        except:
            return AppResponse.get_exception_reponse()
    
    @staticmethod
    def get_protocol(request):
        if request.is_secure():
            protocol = Protocol.HTTPS
        else:
            protocol = Protocol.HTTP
        return protocol