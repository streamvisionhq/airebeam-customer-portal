import requests
from airebeam_customer_portal.utils import convert_date_time, getfromSession,\
    strip_radio_stats
from airebeam_customer_portal import config
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from airebeam_customer_portal.static_values import FormatStrings,\
    ConversationSource, CreateReplyPayload
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.enum import AppStatus
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod
from airebeam_customer_portal.ServicePayload import ServicePayload
from radiostats.models import Radiostat
import json

class ConversationService(object):
    url = ''
    auth = config.Auth
    headers = {AppMsgs.CONTENT_TYPE_KEY: AppMsgs.CONTENT_TYPE_VALUE}
    def __init__(self, request):
        self.request = request
        
    def create_reply(self, ticketId, payload):
        try:
            url = AppUrl.get_create_conversation_url(ticketId)
            r = requests.post(url, auth=self.auth, data=payload, headers=self.headers)
            generated_response = ""
            
            if r.status_code == 201:
                generated_response = AppResponse.get_success_response()
                
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = payload
            Logger.log_api(RequestMethod.POST, r.status_code, r.url, params, r.text)
            return generated_response
        
        except:
            return AppResponse.get_exception_reponse()
        


    def __modify_conv_appearance_data(self, convLst, dtFormatStr, fdCustomerId, conv):
        '''
        Modify some conversation attributes to display on front end.
        @param convLst:
        @param dtFormatStr:
        @param fdCustomerId:
        @param conv:
        '''
        conv[AppMsgs.CREATED_AT_KEY] = convert_date_time(conv[AppMsgs.CREATED_AT_KEY], dtFormatStr)
        conversation_body = conv[AppMsgs.BODY_KEY]
        conv[AppMsgs.BODY_KEY] = strip_radio_stats(conversation_body)
        user_id = long(conv[CreateReplyPayload.USER_ID_KEY])
#         Set this key to indicate whether reply from customer or from customer support user.
        if user_id == fdCustomerId:
            conv[AppMsgs.IS_SHOW_IMAGE_KEY] = False
        else:
            conv[AppMsgs.IS_SHOW_IMAGE_KEY] = True
        
#         Set this to display NOTE/REPLY label on conversation.
        if conv[AppMsgs.SOURCE_KEY] == ConversationSource.NOTE:
            conv[AppMsgs.SOURCE_TEXT_KEY] = AppMsgs.PUBLIC_NOTE_LABEL
        elif conv[AppMsgs.SOURCE_KEY] == ConversationSource.REPLY:
            conv[AppMsgs.SOURCE_TEXT_KEY] = AppMsgs.REPLIED_LABEL
        convLst.append(conv)

    def __process_conversation_list(self, convLst, allConversations, dtFormatStr, fdCustomerId):
        for conv in allConversations:
############# Block: Do not modify attributes if it is public note.############
            if conv[AppMsgs.SOURCE_KEY] == ConversationSource.NOTE:
                if config.HidePublicNotes != True and conv[AppMsgs.PRIVATE_KEY] == False:
                    self.__modify_conv_appearance_data(convLst, dtFormatStr, fdCustomerId, conv)
####################### End Block ###############################                    
            else:
                self.__modify_conv_appearance_data(convLst, dtFormatStr, fdCustomerId, conv)
                
    def get_conversations_by_ticket_id(self, ticketId):
        try:
            page = 1
            per_page = 100
            conv_lst = []
            
            while True:
                url = AppUrl.get_conversations_url(ticketId, page, per_page)
                list_conversations_request = requests.get(url, auth=self.auth, headers=self.headers)
                if list_conversations_request.status_code == requests.codes.ok:
                    all_conversations = list_conversations_request.json()
                    dt_format_str = FormatStrings.date_str
                    fd_customer_id = getfromSession(AppMsgs.FD_CUSTOMER_ID_KEY, self.request)
                    fd_customer_id = long(fd_customer_id)
                    self.__process_conversation_list(conv_lst, all_conversations, dt_format_str, fd_customer_id)
                    params = ""
                    Logger.log_api(RequestMethod.GET, list_conversations_request.status_code, 
                                   list_conversations_request.url, params, list_conversations_request.text)
                    
#                     Do not fetch more conversations. There are no more pages.
                    if len(all_conversations) < per_page:
                        break
                    page += 1
                else:
                    Logger.log_api(RequestMethod.GET, list_conversations_request.status_code, 
                                   list_conversations_request.url, params, list_conversations_request.text)
                    break
                    return AppResponse.get_failure_reponse()

            return AppResponse.get_success_response(conv_lst)
            
        except:
            return AppResponse.get_exception_reponse()
    
    @staticmethod    
    def prepare_ticket_reply_msg(view_obj, fdCustomerId, isSupportTicket, replyBody):
        '''
        Prepare reply msg which is sent on ticket.
        @param fdCustomerId:
        @param isSupportTicket:
        @param replyBody:
        '''
        payload = ServicePayload.get_create_reply_payload(replyBody, fdCustomerId)
#         Should replace constant with ENUM
############# Block: Attach radio stats if it is support ticket ############
        if isSupportTicket != '' and int(isSupportTicket) == 1:
            radio_stat_obj = Radiostat()
            stats_summary = radio_stat_obj.get_stats_str(view_obj.request)
            replyBody = replyBody + stats_summary
            payload = ServicePayload.get_create_reply_payload(replyBody, fdCustomerId)
########################### End Block #####################################            
        payload = json.dumps(payload)
        return payload
    
#    This method is not used anywhere, this should be removed
    def create_reply_with_attachment(self, payload, filePath, ticketId):
        try:
            files = {'attachments[]': open(filePath, 'rb')}
            self.url += ticketId + '/reply'
            create_reply_request = requests.post(self.url, auth=self.auth, data=payload, files=files)
            if create_reply_request.status_code == 201:
                return {AppMsgs.MSG: AppStatus.SUCCESS, AppMsgs.RESP:'', AppMsgs.STATUS_KEY:1}
                
            else:
                return {AppMsgs.MSG:'failure', AppMsgs.RESP:''}
        except:
            return {AppMsgs.MSG:'exception', AppMsgs.RESP:''}
        pass