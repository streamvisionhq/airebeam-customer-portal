import requests
from tickets.enum import ticket_string_map, TicketStatus
from airebeam_customer_portal.utils import convert_date_time, getfromSession
from airebeam_customer_portal.AppServices.ConversationService import ConversationService
import json
from airebeam_customer_portal import config
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from airebeam_customer_portal.static_values import FormatStrings,\
    UpdatedTicketPayload, RequestMethod, MbrDetailKeys
from airebeam_customer_portal.static_values import AppMsgs
from logger.models import Logger
from airebeam_customer_portal.User import User
from airebeam_customer_portal.enum import user_access_level

class TicketService(object):
    url = ''
    auth = config.Auth
    headers = {AppMsgs.CONTENT_TYPE_KEY: AppMsgs.CONTENT_TYPE_VALUE}
    def __init__(self, request):
        self.request = request
        

    def __process_all_tickets_list(self, ticketLst, allTickets, dtFormatStr):
        for ticket in allTickets:
            ticket_last_updated = convert_date_time(ticket[AppMsgs.UPDATED_AT_KEY], dtFormatStr)
            ticket_status_str = ticket_string_map[ticket[AppMsgs.STATUS_KEY]]
            ticket_status_normalize_str = ticket_status_str.replace('_', ' ').capitalize()
            ticket_type = ticket[AppMsgs.TYPE_KEY]
            ticket[AppMsgs.UPDATED_AT_KEY] = ticket_last_updated
            ticket[AppMsgs.STATUS_TEXT_KEY] = ticket_status_normalize_str
            is_read = ticket[UpdatedTicketPayload.CUSTOM_FIELDS_KEY][UpdatedTicketPayload.IS_READ_KEY]
            highlight = self.__ticket_highlight(is_read)
            ticket[AppMsgs.HIGHLIGHT_KEY] = highlight
            ticketLst.append(ticket)

    def get_all_tickets_list(self, fdCustomerId):
        try:
            page = 1
            per_page = 100
            ticket_lst = []
            
            while True:
                url = AppUrl.get_requester_tickets_url(fdCustomerId, page, per_page)
                tickets_request = requests.get(url, auth=self.auth, headers=self.headers)
                if tickets_request.status_code == requests.codes.ok:
                    all_tickets = tickets_request.json()
                    dt_format_str = FormatStrings.date_str
                    self.__process_all_tickets_list(ticket_lst, all_tickets, dt_format_str)
                    params = ""
                    Logger.log_api(RequestMethod.GET, tickets_request.status_code, 
                                   tickets_request.url, params, tickets_request.text)
                    if len(all_tickets) < per_page:
                        break
                    page += 1
                else:
                    Logger.log_api(RequestMethod.GET, tickets_request.status_code, 
                                   tickets_request.url, params, tickets_request.text)
                    break
                    return AppResponse.get_failure_reponse()

            return AppResponse.get_success_response(ticket_lst)    
            
        except:
            return AppResponse.get_exception_reponse()
        

    def __process_specific_tickets_list(self, ticketLst, allTickets, dtFormatStr):
        for ticket in allTickets:
            if ticket[AppMsgs.STATUS_KEY] in [TicketStatus.OPEN, 
                TicketStatus.PENDING, 
                TicketStatus.Dispatched, 
                TicketStatus.Waiting_on_Customer, 
                TicketStatus.Waiting_on_Third_Party]:
                ticket_status_str = ticket_string_map[ticket[AppMsgs.STATUS_KEY]]
                ticket_status_normalize_str = ticket_status_str.replace('_', ' ').capitalize()
                ticket_last_updated = convert_date_time(ticket[AppMsgs.UPDATED_AT_KEY], dtFormatStr)
                is_read = ticket[UpdatedTicketPayload.CUSTOM_FIELDS_KEY][UpdatedTicketPayload.IS_READ_KEY]
                highlight = self.__ticket_highlight(is_read)
                ticket_fields = {AppMsgs.ID_KEY:ticket[AppMsgs.ID_KEY], AppMsgs.SUBJECT_KEY:ticket[AppMsgs.SUBJECT_KEY], 
                    AppMsgs.STATUS_TEXT_KEY:ticket_status_normalize_str, 
                    AppMsgs.UPDATED_AT_KEY:ticket_last_updated, 
                    AppMsgs.HIGHLIGHT_KEY:highlight}
                ticketLst.append(ticket_fields)

    def customer_specific_tickets(self, fdCustomerId):
        try:
            page = 1
            per_page = 100
            ticket_lst = []
            
            while True:
                url = AppUrl.get_requester_tickets_url(fdCustomerId, page, per_page)
                tickets_request = requests.get(url, auth=self.auth, headers=self.headers)
                if tickets_request.status_code == requests.codes.ok:
                    all_tickets = tickets_request.json()
                    dt_format_str = FormatStrings.date_str
                    self.__process_specific_tickets_list(ticket_lst, all_tickets, dt_format_str)
                    params = ""
                    Logger.log_api(RequestMethod.GET, tickets_request.status_code, 
                                   tickets_request.url, params, tickets_request.text)
                    if len(all_tickets) < per_page:
                        break
                    page += 1
                else:
                    Logger.log_api(RequestMethod.GET, tickets_request.status_code, 
                                   tickets_request.url, params, tickets_request.text)
                    break
                    return AppResponse.get_failure_reponse()
            
            return AppResponse.get_success_response(ticket_lst)
            
        except:
            return AppResponse.get_exception_reponse()
        pass
           
    def get_ticket(self, ticketId):
        try:
            url = AppUrl.get_ticket_detail_url(ticketId)
            ticket_detail_request = requests.get(url, auth=self.auth, headers=self.headers)
            generated_response = ""
            if ticket_detail_request.status_code == requests.codes.ok:
                ticket_detail = ticket_detail_request.json()
                dt_format_str = FormatStrings.date_str
                ticket_detail[AppMsgs.CREATED_AT_KEY] = convert_date_time(ticket_detail[AppMsgs.CREATED_AT_KEY], dt_format_str)
                generated_response = AppResponse.get_success_response(ticket_detail)
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = ""
            Logger.log_api(RequestMethod.GET, ticket_detail_request.status_code, 
                           ticket_detail_request.url, params, ticket_detail_request.text)
            return generated_response
        except:
            return AppResponse.get_exception_reponse()
        
    def create_ticket(self, payload):
        try:
            url = AppUrl.get_ticket_url()
            create_ticket_request = requests.post(url, auth=self.auth, data=payload, headers=self.headers)
            generated_response = ""
            params = payload
            
            if create_ticket_request.status_code == 201:
                generated_response = AppResponse.get_success_response()
                
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = payload
            Logger.log_api(RequestMethod.POST, create_ticket_request.status_code, 
                           create_ticket_request.url, params, create_ticket_request.text)
            return generated_response
        except:
            return AppResponse.get_exception_reponse()
        
    def __ticket_highlight(self, isRead):
        highlight = True
        if isRead is not None:
            if int(isRead) == 1:
                highlight = False
        else:
            highlight = False
            
        return highlight
    
    def update_ticket_by_id(self, payload, ticketId):
        try:
            url = AppUrl.get_ticket_url_with_id(ticketId)
            update_ticket_request = requests.put(url, auth=self.auth, data=payload, headers=self.headers)
            if update_ticket_request.status_code == 200:
                return AppResponse.get_success_response()
                
            else:
                return AppResponse.get_failure_reponse()
        except:
            return AppResponse.get_exception_reponse()
    
    @staticmethod
    def prepare_update_payment_ticket_body(request, valueList, paymentByEcheck, mbrDetail):
        account_number = mbrDetail[MbrDetailKeys.MBR_KEY]
        account_holder_name = mbrDetail[MbrDetailKeys.LASTNAME_KEY] + ' ' + mbrDetail[MbrDetailKeys.FIRSTNAME_KEY]
        contact_number = mbrDetail[MbrDetailKeys.PHONEHOME_KEY]
        contact_email = mbrDetail[MbrDetailKeys.EMAIL_KEY]
        eft_account_last_four = mbrDetail[MbrDetailKeys.EFTACCOUNTLASTFOUR]
        eft_routing_number = mbrDetail[MbrDetailKeys.EFTROUTINGNUMBER]
        eft_auth_name = mbrDetail[MbrDetailKeys.EFTAUTHNAME]
        new_payment_method = AppMsgs.CREDIT_CARD_LABEL
        last_four_digits = mbrDetail[MbrDetailKeys.LAST_FOUR]
        expiration_date = mbrDetail[MbrDetailKeys.EXPIRATION_DATE]
        
        ticket_body = ''
        
        user_acl = user_access_level[AppMsgs.SHALLOW_KEY]
        desc_body_heading = TicketService.ticket_desc_heading(request, user_acl)
        ticket_body += desc_body_heading
        
        label_list = ['Account Number(MBR)', 'Account Hold Name', 'Contact Phone Number', 
                      'Contact email address', 'New Payment Method']
        
        if last_four_digits is not None:
            label_list.append('Last four digits of credit card on file')
        if expiration_date is not None:
            label_list.append('Expiration date')
        
        if paymentByEcheck is not None and paymentByEcheck == 1:
            label_list = ['Account Number(MBR)', 'Account Hold Name', 'Contact Phone Number', 
                          'Contact email address', 'New Payment Method']
            if eft_auth_name is not None:
                label_list.append('Account Holder Name')
            if eft_account_last_four is not None: 
                label_list.append('Account Number')
            if eft_routing_number is not None:
                label_list.append('Routing Code')
                
        for count, label in enumerate(label_list):
            ticket_body += '<div> {0} : {1} </div>'.format(label, valueList[count])
            
        return ticket_body
    
    
    @staticmethod
    def ticket_desc_heading(request, userAcl):
        desc_label = ''
        user_access = User.get_access_level(request)
        if user_access == userAcl:
                if AppMsgs.AGENT_USERNAME in request.session:
                    element_text = ' <b>Submitted by {0}</b>'.format(request.session[AppMsgs.AGENT_USERNAME]) + ' '
                else:
                    element_text = ' <b>Shallow Access</b>' + ' '
                desc_label += '<div>'
                desc_label += element_text 
                desc_label += '</div>'
        return desc_label