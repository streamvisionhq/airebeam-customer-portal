import requests
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from airebeam_customer_portal import config
from airebeam_customer_portal.static_values import AppMsgs

#  No longer used
class SpeedTestService(object):
    key = config.BackofficeApiKey
    headers   = {AppMsgs.AUTHORIZATION_KEY: AppMsgs.BEARER + key}
    
    def __init__(self):
        pass
        
    def call_service(self, payload):            
        try:
            url = AppUrl.get_speedtest_url()
            speedtest_request = requests.post(url, data=payload, headers=self.headers)
            status = speedtest_request.status_code
            response_obj = speedtest_request.json()
            if status == requests.codes.ok:
                return AppResponse.get_success_response(response_obj)
            else:
                return AppResponse.get_failure_reponse()
        except:
            return AppResponse.get_exception_reponse()
    