import requests
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod, ExceptionMsgs
from airebeam_customer_portal.static_values import AppMsgs, LogActivity
from airebeam_customer_portal.enum import AjaxCallStatus, AppStatus
import datetime
from airebeam_customer_portal.utils import savesessiondata
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
from mbr_online_check.models import mbrDetail
from requests.exceptions import RequestException, ConnectionError, HTTPError
from airebeam_customer_portal.AcpException import AcpException

class LoginService(object):
    def __init__(self):
        pass
        
    def call_service(self, mbr, password):
        '''
        Calls login api and authenticate customer.
        @param mbr:
        @param password:
        '''
        try:
            url = AppUrl.get_login_url(mbr, password)
            login_request = requests.get(url)
            status = login_request.status_code
            generated_response = ""
            
            if status == requests.codes.ok:
                response_obj = login_request.json()
                generated_response = AppResponse.get_success_response(response_obj)
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = ""
            Logger.log_api(RequestMethod.GET, status, login_request.url, params, login_request.text, LogActivity.CUSTOMER_LOGIN)
            Logger.made_log_as_csv()
            login_request.raise_for_status()
            return generated_response
        
        except ConnectionError as e:
            raise AcpException('', AppMsgs.CONNECTION_ERROR)
        
        except HTTPError as e:
            raise AcpException('', AppMsgs.REQUEST_UNSUCCESSFUL)
           
    
    @staticmethod
    def set_customer_data(viewObject, responseObject, userAcl, mbr):
        '''
        Set terms url, force reset password flag, reset pass token, 
        user access level and login time in session.
        @param responseObject:
        '''
        if AppMsgs.TERMS_CONDITIONS in responseObject:
            terms_url = responseObject[AppMsgs.TERMS_CONDITIONS]
            terms_url = terms_url.replace('http:', '')
            viewObject.set_terms_url(terms_url)
        if AppMsgs.FORCE_RESET in responseObject:
            force_reset = responseObject[AppMsgs.FORCE_RESET]
            viewObject.set_force_reset_pass(force_reset)
        if AppMsgs.TOKEN in responseObject:
            token = responseObject[AppMsgs.TOKEN]
            viewObject.set_reset_pass_token(token)
        
        viewObject.set_access_level(userAcl)
        call_status = AjaxCallStatus.NOT_STARTED
        viewObject.set_radiostats_call_status(call_status)
        login_time = str(datetime.datetime.now())
        savesessiondata(AppMsgs.LOGIN_TIME_KEY,login_time,viewObject.request)
        savesessiondata(AppMsgs.MBRKEY,mbr,viewObject.request)
    
    @staticmethod
    def search_customer_on_fd_by_phone(viewObject, mbr):
        '''
        Method searches customer on fresh desk. If it is exist on fresh desk, store its FD 
        id in session. 
        '''
        customer_service_object = CustomerService()
        cs_response = customer_service_object.get_fd_customer_by_phone(mbr)
        if cs_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
            contact_info = cs_response[AppMsgs.RESP]
            savedata = contact_info[0][AppMsgs.ID_KEY]
            savesessiondata(AppMsgs.FD_CUSTOMER_ID_KEY,savedata, viewObject.request)
    
    @staticmethod
    def fetch_mbr_details(viewObject, mbr):
        '''
        Fetches customer MBR details
        '''
        mbr_detail_obj = mbrDetail()
        mbr_detail_obj.getmbrdetails(mbr, viewObject.request)
        return mbr_detail_obj