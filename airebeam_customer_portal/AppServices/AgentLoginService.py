import requests
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from airebeam_customer_portal import config
from airebeam_customer_portal.static_values import AppMsgs, LogActivity
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod

class AgentLoginService(object):
    key = config.BackofficeApiKey
    
    def __init__(self):
        pass
        
    def call_service(self, payload):            
        try:
            url = AppUrl.get_agent_login_url()
            headers   = {AppMsgs.AUTHORIZATION_KEY: AppMsgs.BEARER + self.key}
            login_request = requests.post(url, data=payload, headers=headers)
            status = login_request.status_code
            response_obj = login_request.json()
            generated_response = ""
            
            if status == requests.codes.ok:
                generated_response = AppResponse.get_success_response(response_obj)
            elif status == 401:
                generated_response = AppResponse.get_success_response(response_obj)
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params= payload
            Logger.log_api(RequestMethod.POST, status, login_request.url, params, login_request.text, LogActivity.AGENT_LOGIN)
            Logger.made_log_as_csv()
            return generated_response
        except:
            return AppResponse.get_exception_reponse()
    