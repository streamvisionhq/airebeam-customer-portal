import requests
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod

class SendSmsCodeService(object):
    
    def __init__(self):
        pass
        
    def call_service(self, mbr):            
        try:
            url = AppUrl.get_send_sms_url(mbr)
            send_sms_request = requests.get(url)
            status = send_sms_request.status_code
            response_obj = send_sms_request.json()
            generated_response = ""
            
            if status == requests.codes.ok:
                generated_response = AppResponse.get_success_response(response_obj)
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = ""
            Logger.log_api(RequestMethod.GET, status, 
                           send_sms_request.url, params, send_sms_request.text)
            return generated_response
        except:
            return AppResponse.get_exception_reponse()
    