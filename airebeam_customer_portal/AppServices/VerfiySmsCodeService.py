import requests
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod

class VerfiySmsCodeService(object):
    
    def __init__(self):
        pass
        
    def call_service(self, mbr, code):            
        try:
            url = AppUrl.get_verify_sms_code_url(mbr, code)
            verify_code_request = requests.get(url)
            status = verify_code_request.status_code
            response_obj = verify_code_request.json()
            generated_response = ""
            
            if status == requests.codes.ok:
                generated_response = AppResponse.get_success_response(response_obj)
            else:
                generated_response = AppResponse.get_failure_reponse()
                
            params = ""
            Logger.log_api(RequestMethod.GET, status, 
                           verify_code_request.url, params, verify_code_request.text)
            return generated_response
        except:
            return AppResponse.get_exception_reponse()
    