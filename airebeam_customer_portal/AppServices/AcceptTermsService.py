import requests
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppUrl import AppUrl
from logger.models import Logger
from airebeam_customer_portal.static_values import RequestMethod

class AcceptTermsService(object):
    
    def __init__(self):
        pass
        
    def call_service(self, mbr):            
        try:
            url = AppUrl.get_accept_terms_url(mbr)
            accept_terms_request = requests.get(url)
            status = accept_terms_request.status_code
            response_obj = accept_terms_request.json()
            generated_response = ""
            
            if status == requests.codes.ok:
                generated_response = AppResponse.get_success_response(response_obj)
            else:
                generated_response = AppResponse.get_failure_reponse()
            
            params = ""
            Logger.log_api(RequestMethod.GET, status, accept_terms_request.url, params, accept_terms_request.text)
            return generated_response
        
        except:
            return AppResponse.get_exception_reponse()
    