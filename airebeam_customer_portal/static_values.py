import config_local

class AppMsgs:
    MSG = 'msg'
    RESP = 'response'
    ERROR = 'error'
    RADIOSTATS_DOWNLOAD_SETTINGS = 'rs_download_settings'
    RADIOSTATS_UPLOAD_SETTINGS = 'rs_upload_settings'
    RADIOSTATS_DOWNLOAD = 'rs_download'
    RADIOSTATS_UPLOAD = 'rs_upload'
    RADIOSTATS_INBYTES = 'rs_in_bytes'
    RADIOSTATS_OUTBYTES = 'rs_out_bytes'
    RADIOSTATS_TIMESTAMP = 'rs_timestamp'
    RADIOSTATS_LANSTATUS = 'rs_lanstatus'
    RADIOSTATS_PING = 'rs_ping'
    RADIOSTATS_ANTENNA = 'rs_antenna'
    RADIOSTATS_UPTIME = 'rs_uptime'
    RADIOSTATS_LOGID = 'rs_logid'
    FORMKEY = 'form'
    MBRKEY = 'mbr'
    ACCOUNT_NOT_FOUND = 'Account not found'
    PHONE_AND_ADDRESS_MISSING = 'Missing HomePhone & Address1, at least one of the below fields is required.'
    LOGIN_CREDENTIALS_INVALID = 'Invalid credentials. Please enter correct information!'
    CUSTOMER_ACCESS_NOT_EXIST = 'Unable to login. Customer support will contact you soon'
    MBR_DETAIL_KEY = 'mbr_detail'
    ONLINECHECK_KEY = 'onlinecheck'
    USERNAME_KEY = 'username'
    LOGIN_TIME_KEY = 'login_time'
    NOT_FETCH_MBR_DETAILS = 'Could not fetch mbr details. Please try later.'
    NOT_FETCH_ONLINECHECK = 'Could not fetch online service check. Please try later.'
    STATLIST_KEY = 'stat_list'
    NOT_FOUND_CONNECTION = 'Connection not found.'
    NOT_FETCH_TICKETS = 'Could not fetch tickets. Please try later.'
    DO_NOT_HAVE_OPEN_TICKETS = 'You do not have any open or pending tickets.' + \
                                'To see your previous tickets, please click the "View All" button.'
    FD_CUSTOMER_ID_KEY = 'fd_customer_id'
    TICKETS_DICT_KEY = 'tickets_dict'
    TICKET_LIST_KEY = 'tickets_list'
    NOT_FETCH_TICKET_DETAIL = 'Could not fetch ticket detail. Please try later.'
    TICKET_DETAIL_KEY = 'ticket_detail'
    IS_SUPPORT_KEY = 'is_support'
    PARENT_CATEGORIES_KEY = 'parent_categories'
    SUBCATEGORIES_KEY = 'sub_categories'
    NOT_FETCH_TICKET_CONVERSATIONS = 'Could not fetch ticket conversations. Please try later.'
    CONVERSATIONS_KEY = 'conversations'
    SERVICES_KEY = 'services'
    SERVICE_FIELD_KEY = 'service_field'
    SUBCATEGORY_ID_KEY = 'sub_cat_id'
    CARD_FIELDS_KEY = 'card_fields'
    CHECK_FIELDS_KEY = 'check_fields'
    STATUS_KEY = 'status'
    PERFORM_TEST = 'true'
    SPEEDTEST_RESULT = 'speedtest_result'
    SPEEDTEST_ID = 'speedtest_id'
    MBR_EXIST_KEY = 'mbr_exist'
    THANK_YOU_BACK_KEY = 'back_btn_text'
    THANK_YOU_LOGIN_TEXT = 'Go back to dashboard'
    THANK_YOU_NO_LOGIN_TEXT = 'Return to login'
    MBR_NUMBER_KEY = 'mbr_number'
    CAPTCHA_ERROR = 'Captcha is not verified.'
    CUSTOMER_EMAIL_KEY = 'customer_email'
    CUSTOMER_CELLPHONE_KEY = 'customer_cellphone'
    RESET_PASSWORD_PARAMETERS = 'Url is not valid'
    RESET_TOKEN_KEY = 'token'
    RESET_MEDIUM_KEY = 'medium'
    FORCE_RESET_KEY = 'force_reset_pass'
    PHONE_HOME_KEY = 'phonehome'
    CUSTOMER_NAME_KEY = 'customer_name'
    CALL_APPROVAL_TICKET_BODY = 'has requested to reset their Account Password for Customer Portal. Please contact at your earliest.'
    CUSTOMER_CONTACT_TICKET_BODY = 'has requested to reset their Account Password for Customer Portal.'
    CUSTOMER_CONTACT_IF_EMAIL_PHONE_EXIST = 'Their email address is {0} and phone number is {1}.'
    CUSTOMER_CONTACT_IF_EMAIL_EXIST = 'Their email address is {0}.'
    CUSTOMER_CONTACT_IF_PHONE_EXIST = 'Their phone number is {0}.'
    CUSTOMER_CONTACT_PLEASE = 'Please contact at your earliest.'
    AUTHORIZATION_KEY = "Authorization"
    BEARER = "Bearer "
    CONTENT_TYPE_KEY = 'content-type'
    CONTENT_TYPE_VALUE = 'application/json'
    CREATED_AT_KEY = 'created_at'
    BODY_KEY = 'body'
    IS_SHOW_IMAGE_KEY = 'is_show_image'
    SOURCE_KEY = 'source'
    SOURCE_TEXT_KEY = 'source_text'
    PUBLIC_NOTE_LABEL = 'Public Note'
    REPLIED_LABEL = 'Replied'
    PRIVATE_KEY = 'private'
    MAPPING_LEVEL_KEY = 'mapping_level'
    UPDATED_AT_KEY = 'updated_at'
    TYPE_KEY = 'type'
    STATUS_TEXT_KEY = 'status_text'
    HIGHLIGHT_KEY = 'highlight'
    ID_KEY = 'id'
    SUBJECT_KEY = 'subject'
    USER_ACCESS_LEVEL_KEY = 'user_access_level'
    SPEEDTEST_CALL_STATUS_KEY = 'speedtest_call_status'
    LAST_SPEEDTEST_TIME_KEY = 'last_speedtest_time'
    DATA_KEY = 'data'
    TIMEZONE_VALUE = 'US/Arizona'
    LOGIN_DATE_FORMAT = '%Y-%m-%d %H:%M:%S.%f'
    SPEEDTEST_DATE_FORMAT = '%Y-%m-%d %H:%M:%S.%f'
    AP_CONNECTION_UPTIME = 'AP Connection Uptime'
    DOWNLOAD_KEY = 'download'
    AVERAGE_KEY = 'average'
    MBPS = ' Mbps'
    UPLOAD_KEY = 'upload'
    FAILED = 'Failed'
    TIMESTAMP_KEY = 'timestamp'
    PING_KEY = 'ping'
    LATENCY_KEY = 'latency'
    UNPLUGGED = 'Unplugged'
    LANSPEED_KEY = 'lanSpeed'
    STARTUPDATE_KEY = 'startupdate'
    SUMMARY_KEY = 'summary'
    EMERALD_KEY = 'emerald'
    UNTESTED = 'Untested'
    BOLD_CONTAINER = '<div><b>{0}</b></div>'
    PASSWORD_KEY = 'password'
    SHALLOW_KEY = 'shallow'
    AUTHORIZED_KEY = 'authorized'
    COMPLETE_KEY = 'complete'
    SELECTED_PARENT_CATEGORY = 'selected_parent_category'
    OLD_PASSWORD_KEY = 'old_password'
    NEW_PASSWORD_KEY = 'new_password'
    RESP_STATUS_SUCCESS = 'Success'
    RESP_STATUS_FAILURE = 'Failure'
    CHANGE_PASSWORD_STATUS_KEY = 'change_password_status'
    NAME = 'name'
    EMAIL = 'email'
    DESCRIPTION = 'description'
    RECAPTCHA_RESPONSE = 'g-recaptcha-response'
    SUPPORT = 'support'
    MBR_CHECKBOX = 'mbr_checkbox'
    ADDRESS_TEXT = 'address_text'
    PHONE_NUMBER_TEXT = 'phone_number_text'
    LAST_FOUR_CC_DIGIT_TEXT = 'last_four_cc_digit_text'
    MESSAGE = 'message'
    TERMS_CONDITIONS = 'terms-conditions'
    TOKEN = 'token'
    LOSS = 'loss'
    MBPS_DL_FLAG = ' Mbps DL_FLAG'
    RADIO_RX = 'Radio RX: '
    PASSED = 'passed'
    CALL_STATUS = 'call_status'
    STATS_BY_CUSTOMER = 'stats_by_cus'
    RECENT_STATS_DICT = 'recent_stats_dict'
    TIMER_KEY = 'timer'
    ACCOUNT_CHANGE = 'Account Change'
    RESET_PASS_KEY = 'key'
    RESET_PASS_MEDIUM = 'medium'
    IS_PASSWORD_RESET = 'is_password_reset'
    LOGIN_KEY = 'login'
    LINK_EXPIRY_KEY = 'ex'
    FROM_SMS_VIEW_KEY = 'fr_sms'
    SEND_EMAIL = 'send_email'
    SEND_SMS = 'send_sms'
    SMS_CODE = 'code'
    VERIFY_SMS_CODE = 'verify_sms_code'
    TERMS_URL = 'terms_url'
    FORCE_RESET = 'force-reset'
    RADIOSTATS_CALL_STATUS = 'radiostats_call_status'
    AGENT_USERNAME = 'agent_username'
    VERSION_NUM = 'version_num'
    TEMP_SPEEDTEST_RESULT = 'temp_speedtest_result'
    TERMS_PDF = 'terms_pdf'
    CAPTCHA_KEY = 'captcha_key'
    CUSTOMER_INFO = 'customer_info'
    SESSION_FORCE_RESET = 'force_reset'
    RESET_TOKEN = 'reset_token'
    GROUP_ID = 'group_id'
    PRIORITY = 'priority'
    REQUESTER_ID = 'requester_id'
    MOBILE = 'mobile'
    PHONE_META = 'phone_meta'
    SECRET = 'secret'
    EMERALD_USER = 'emerald_user'
    EMERALD_PASSWORD = 'emerald_password'
    MBR_SEARCH = 'mbrsearch'
    CONNECTION = 'CONNECTION'
    EMERWEB = 'emerweb'
    CREDIT_CARD_LABEL = 'Credit Card'
    ECHECK_LABEL = 'E-Check'
    BILLING_REFUND = 'Billing/Credits/Refund'
    LAN_PLUGGED_KEY = 'lanPlugged'
    PAYMENT_BY_ECHECK = 'echeck'
    PROBLEM_CATEGORY = 'Problem Category'
    PROBLEM_SUB_CATEGORY = 'Problem Subcategory'
    CUSTOMER_ID_KEY = 'customer_id'
    ACCOUNT_HOLDER_NAME_KEY = 'account_hold_name'
    FD_CATEGORY_KEY = 'fd_category'
    FD_SUBCATEGORY_KEY = 'fd_subcategory'
    STATUS_STR_KEY = 'status_str'
    LAN_PLUGGED_KEY = 'lanPlugged'
    PAYMENT_BY_ECHECK = 'echeck'
    EXCEPT_MSG = 'message'
    CONNECTION_ERROR = 'Connection Error'
    REQUEST_UNSUCCESSFUL = 'Request Unsuccessful'

class LogActivity():
    CUSTOMER_LOGIN = 'Customer Login'
    AGENT_LOGIN = 'Agent Login'
    LOGOUT = 'Logout'
    
class ExceptionMsgs():
    GENERAL_EXCEPTION = 'Error occured. Please try later'
    ATTRIBUTE_EXCEPTION =  'Module does not have attribute'
    
class RequestMethod():
    GET = 'GET'
    POST = 'POST'    
    
class CustomerInfo():
    EMAIL = 'Email'
    PHONEHOME = 'PhoneHome'
    IS_PHONEHOME_LANDLINE = 'isPhoneHomeLandline'
    CUSTOMER = 'customer'
    IS_PASSWORD_RESET = 'isPasswordReset'
    CUSTOMER_NAME = 'CustomerName'
    CUSTOMER_ID = 'CustomerID'

class AppFormName():
    request_delay_form = 'Request a delay in payment due and grace period extension'

class MbrDetailKeys():
    MBR_KEY = 'MBR'
    FIRSTNAME_KEY = 'FIRSTNAME'
    LASTNAME_KEY  = 'LASTNAME'
    ADDRESS1_KEY = 'ADDRESS1'
    EMAIL_KEY = 'EMAIL'
    PHONEHOME_KEY = 'PHONEHOME'
    PHONEWORK_KEY = 'PHONEWORK'
    MBR_DETAIL = 'MBRDETAIL'
    LAST_FOUR = 'LASTFOUR'
    EXPIRATION_DATE = 'EXPIRATIONDATE'
    EFTACCOUNTLASTFOUR = 'EFTACCOUNTLASTFOUR'
    EFTROUTINGNUMBER = 'EFTROUTINGNUMBER'
    EFTAUTHNAME = 'EFTAUTHNAME'

class BaseUrl():
    TICKET_URL = config_local.TicketUrl
    LOGIN_URL = config_local.LoginUrl
    FORGOT_PASSWORD_URL = config_local.ForgotPasswordUrl
    ONLINECHECK_URL = config_local.OnlinecheckUrl
    MBR_URL = config_local.MbrUrl
    SPEEDTEST_URL = config_local.SpeedtestUrl
    CONTACT_URL = config_local.ContactUrl
    GCAPTCHA_URL = config_local.GcaptchaUrl
    AGENT_LOGIN_URL = config_local.AgentLoginUrl
    LOG_RESULT_URL = config_local.LogResultUrl
    ACCEPT_TERMS_URL = config_local.AcceptTermsUrl
    AB_SERVICE_URL = config_local.AbServiceUrl
    CUSTOMER_INFO_URL = config_local.CustomerInfoUrl
    SEND_EMAIL_URL = config_local.SendEmailUrl
    UPDATE_PASSWORD_URL = config_local.UpdatePasswordUrl
    SEND_SMS_URL = config_local.SendSmsUrl
    VERIFY_SMS_CODE_URL = config_local.VerifySmsCodeUrl
    CHANGE_PASSWORD_URL = config_local.ChangePasswordUrl
    
class CustomerInfoPayload():
        MBR = 'mbr'    
    
class ForgotPasswordPayload():
        ADDRESS_KEY = 'address'
        PHONE_KEY = 'phone'
        CREDIT_CARD = 'credit_card'

class SpeedtestPayload():
    ACCOUNT_KEY = 'account'
    SPEEDTEST_KEY = 'speedtest'
    
class CreateReplyPayload():
    BODY_KEY = 'body'
    USER_ID_KEY = 'user_id'
    
class UpdatedTicketPayload():
    CUSTOM_FIELDS_KEY = "custom_fields"
    IS_READ_KEY = "is_read"
    
class CustomFields():
    __NO_SUPPORT_TICKET_PAYLOAD = {'is_read': '1', 'is_support': '0'}
    __SUPPORT_TICKET_PAYLOAD = {'is_read': '1', 'is_support': '1'}
    
    def get_mbr_to_no_support_payload(self, mbr):
        self.__NO_SUPPORT_TICKET_PAYLOAD['phone_number'] = mbr
        return self.__NO_SUPPORT_TICKET_PAYLOAD
    
    def get_mbr_to_support_payload(self, mbr):
        self.__SUPPORT_TICKET_PAYLOAD['phone_number'] = mbr
        return self.__SUPPORT_TICKET_PAYLOAD
    
    
class FormatStrings():
    date_str = '%Y-%m-%dT%H:%M:%SZ'
    
class ConversationSource():
    REPLY = 0
    NOTE = 2
    CREATED_FROM_TWEETS = 5
    CREATED_FROM_SURVEY_FEEDBACK = 6
    CREATED_FROM_FACEBOOK_POST = 7
    CREATED_FROM_FORWARDED_EMAIL = 8
    CREATED_FROM_PHONE = 9
    CREATED_FROM_MOBIHELP = 10
    ECOMMERCE = 11