import os

BaseDir = ''
StaticfilesDirs = ''
StaticRoot = ''
FreshdeskEmail = ''
FreshdeskPassword = ''
EmeraldCredentials =  ''
DatabaseConfig = ''
CaptchaSecret = ''
CaptchaKey = ''
BackofficeApiKey = ''

TicketUrl = ''
LoginUrl = ''
ForgotPasswordUrl = ''
OnlinecheckUrl = ''
MbrUrl = ''
SpeedtestUrl = ''
ContactUrl = ''
GcaptchaUrl = ''
AgentLoginUrl = ''
LogResultUrl = ''
AcceptTermsUrl = ''
AbServiceUrl = ''
PdfUrl = ''
TestStr = ''
CustomerInfoUrl = ''
SendEmailUrl = ''
UpdatePasswordUrl = ''
SendSmsUrl = ''
VerifySmsCodeUrl = ''
HidePublicNotes = ''
ChangePasswordUrl = ''