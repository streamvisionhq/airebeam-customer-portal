from datetime import datetime
import pytz
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.SwitchCase import switch
from airebeam_customer_portal.SwitchCase import case


def savesessiondata(key,savedata, request):

    if not AppMsgs.DATA_KEY in request.session or not request.session[AppMsgs.DATA_KEY]:
        data = {}
        while switch(key):
            if case(AppMsgs.MBRKEY):
                data[AppMsgs.MBRKEY]       = savedata
                request.session[AppMsgs.DATA_KEY]  = data
                request.session.modified = True
                break
            
            if case(AppMsgs.MBR_DETAIL_KEY):
                data[AppMsgs.MBR_DETAIL_KEY]       = savedata
                request.session[AppMsgs.DATA_KEY]  = data
                request.session.modified = True
                break
            
            if case(AppMsgs.ONLINECHECK_KEY):
                data[AppMsgs.ONLINECHECK_KEY]      = savedata
                request.session[AppMsgs.DATA_KEY]  = data
                request.session.modified = True
                break
            
            if case(AppMsgs.FD_CUSTOMER_ID_KEY):
                data[AppMsgs.FD_CUSTOMER_ID_KEY]      = savedata
                request.session[AppMsgs.DATA_KEY]  = data
                request.session.modified = True
                break
            
            if case(AppMsgs.LOGIN_TIME_KEY):
                data[AppMsgs.LOGIN_TIME_KEY]      = savedata
                request.session[AppMsgs.DATA_KEY]  = data
                request.session.modified = True
                break
            
            if case(AppMsgs.STATLIST_KEY):
                stat_list = []
                stat_list.insert(0, savedata)
                data[AppMsgs.STATLIST_KEY]        = stat_list
                request.session[AppMsgs.DATA_KEY]  = data
                request.session.modified = True
                break

    else:
        data = request.session[AppMsgs.DATA_KEY]
        
        while switch(key):
            if case(AppMsgs.MBRKEY):
                data[AppMsgs.MBRKEY]       = savedata
                request.session[AppMsgs.DATA_KEY]  = data
                request.session.modified = True
                break

            if case(AppMsgs.MBR_DETAIL_KEY):
                data[AppMsgs.MBR_DETAIL_KEY]       = savedata
                request.session[AppMsgs.DATA_KEY]  = data
                request.session.modified = True
                break
    
    
            if case(AppMsgs.ONLINECHECK_KEY):
                data[AppMsgs.ONLINECHECK_KEY]            = savedata
                request.session[AppMsgs.DATA_KEY] =data
                request.session.modified = True
                request.session.save()
                request.session.modified       = True
                break
    
            if case(AppMsgs.FD_CUSTOMER_ID_KEY):
                data[AppMsgs.FD_CUSTOMER_ID_KEY]            = savedata
                request.session[AppMsgs.DATA_KEY] = data
                request.session.modified       = True
                break
            
            if case(AppMsgs.LOGIN_TIME_KEY):
                data[AppMsgs.LOGIN_TIME_KEY]            = savedata
                request.session[AppMsgs.DATA_KEY] = data
                request.session.modified       = True
                break
    
            if case(AppMsgs.STATLIST_KEY):
                if data.has_key(AppMsgs.STATLIST_KEY):
                    stat_list                = data[AppMsgs.STATLIST_KEY]
                    stat_list.insert(0, savedata)
                    data[AppMsgs.STATLIST_KEY]        = stat_list
                    request.session[AppMsgs.DATA_KEY]  = data
                    request.session.modified = True
                else:
                    stat_list = []
                    stat_list.insert(0, savedata)
                    data[AppMsgs.STATLIST_KEY]        = stat_list
                    request.session[AppMsgs.DATA_KEY]  = data
                    request.session.modified = True
                break
    return

def clear_user_data_keys(request):
    if request.session.has_key(AppMsgs.DATA_KEY):
        data = request.session[AppMsgs.DATA_KEY]
        if AppMsgs.MBRKEY in data:
            del data[AppMsgs.MBRKEY]
        if AppMsgs.FD_CUSTOMER_ID_KEY in data:
            del data[AppMsgs.FD_CUSTOMER_ID_KEY]
        if AppMsgs.ONLINECHECK_KEY in data:
            del data[AppMsgs.ONLINECHECK_KEY]
        if AppMsgs.STATLIST_KEY in data:
            del data[AppMsgs.STATLIST_KEY]
        if AppMsgs.MBR_DETAIL_KEY in data:
            del data[AppMsgs.MBR_DETAIL_KEY]
        request.session[AppMsgs.DATA_KEY] = data
        
def checksession(key, request):
    if request.session.has_key(AppMsgs.DATA_KEY):
        data = request.session[AppMsgs.DATA_KEY]
        if data.has_key(key):
            return True
        else:
            return False
    else:
        return False

def getfromSession(key,request):

    if checksession(key, request):
        data=request.session[AppMsgs.DATA_KEY]
        while switch(key):
            if case(AppMsgs.MBRKEY):
                return data[AppMsgs.MBRKEY]
                break
            if case(AppMsgs.MBR_DETAIL_KEY):
                return data[AppMsgs.MBR_DETAIL_KEY]
                break
            if case(AppMsgs.ONLINECHECK_KEY):
                return data[AppMsgs.ONLINECHECK_KEY]
                break
            if case(AppMsgs.FD_CUSTOMER_ID_KEY):
                return data[AppMsgs.FD_CUSTOMER_ID_KEY]
                break
            if case(AppMsgs.LOGIN_TIME_KEY):
                return data[AppMsgs.LOGIN_TIME_KEY]
                break
            if case(AppMsgs.STATLIST_KEY):
                return data[AppMsgs.STATLIST_KEY]
                break
            return None
            break

def convert_date_time(date_str, format_str):
    datetime_object = datetime.strptime(date_str, format_str)
#     datetime_object
    local_tz = pytz.timezone('US/Arizona')
    utc_time = pytz.utc.localize(datetime_object)
    local_time = utc_time.astimezone(local_tz)
    return local_time

def get_timer(speedtest_time):
    date_formater = '%Y-%m-%d %H:%M:%S.%f'
    speedtest_time = datetime.strptime(speedtest_time, date_formater)
    now = datetime.now()
    td = now - speedtest_time
    if td.seconds > 300:
        return 0
    else:
        remain_timer = 300 - td.seconds
        return remain_timer
    
    
def check_if_session_time_exceeds(login_time):
    date_formater = '%Y-%m-%d %H:%M:%S.%f'
    login_time = datetime.strptime(login_time, date_formater)
    now = datetime.now()
    td = now - login_time
    hours_diff = td.seconds//3600
    days = td.days
    if hours_diff >= 3:
        return True
    elif days >= 1:
        return True
    else:
        return False
    pass

def convert_into_MB(bytes):
    value = (float(bytes)) / 10 ** 6
    value = "%.6f MB" % value
    return value

def get_summary_string(summary):
    summary_str = ''
    summary = summary.split('\r\n')
    found_uptime = filter(lambda element: AppMsgs.AP_CONNECTION_UPTIME in element, summary)
    if len(found_uptime):
        uptime_index = summary.index(found_uptime[0])
        uptime_str = summary[uptime_index].split(':', 1)
        summary_str = uptime_str[1].strip()
    return summary_str

def get_download_string(radio_stat):
    download_avg_str = ''
    if AppMsgs.DOWNLOAD_KEY in radio_stat:
        download_avg = radio_stat[AppMsgs.DOWNLOAD_KEY][AppMsgs.AVERAGE_KEY]
        download_avg_str = str(download_avg) + AppMsgs.MBPS
    
    return download_avg_str

def get_download_settings_string(emerald):
    download_settings_str = ''
    if AppMsgs.DOWNLOAD_KEY in emerald:
        download_settings = emerald[AppMsgs.DOWNLOAD_KEY]
        download_settings_str = str(download_settings) + AppMsgs.MBPS
    
    return download_settings_str

def get_upload_settings_string(emerald):
    upload_settings_str = ''
    if AppMsgs.UPLOAD_KEY in emerald:
        upload_settings = emerald[AppMsgs.UPLOAD_KEY]
        upload_settings_str = str(upload_settings) + AppMsgs.MBPS
    
    return upload_settings_str

def get_upload_string(radio_stat):
    upload_avg_str = ''
    if AppMsgs.UPLOAD_KEY in radio_stat:
        upload_avg = radio_stat[AppMsgs.UPLOAD_KEY][AppMsgs.AVERAGE_KEY]
        if float(upload_avg) > 0:
            upload_avg_str = str(upload_avg) + AppMsgs.MBPS
        else:
            upload_avg_str = AppMsgs.FAILED
    
    return upload_avg_str

def get_logid_string(radio_stat):
    logid_str = ''
    if AppMsgs.ID_KEY in radio_stat:
        logid_str = radio_stat[AppMsgs.ID_KEY]
    return logid_str

def get_selected_radio_stats(stats_dict):
#   Removed in bytes and out bytes if it is not needed
    rs_download_settings = []
    rs_upload_settings = []
    rs_download = []
    rs_upload = []
    rs_ping = []
    rs_in_bytes = []
    rs_out_bytes = []
    rs_timestamp = []
    rs_lanstatus = []
    rs_antenna = []
    rs_uptime = []
    rs_logid = []
    for radio_stat in stats_dict:
        rs_timestamp.append(radio_stat[AppMsgs.TIMESTAMP_KEY])
        ping_status = str(radio_stat[AppMsgs.PING_KEY][AppMsgs.LATENCY_KEY]) + ' ms ' + str(radio_stat[AppMsgs.PING_KEY][AppMsgs.LOSS]) + '%  loss' 
        rs_ping.append(ping_status)
        lan_status = '<span class="unplugged_status">' +  AppMsgs.UNPLUGGED + '</span>'
        if AppMsgs.LAN_PLUGGED_KEY in radio_stat and int(radio_stat[AppMsgs.LAN_PLUGGED_KEY]) == 1:
            lan_status = str(radio_stat[AppMsgs.LANSPEED_KEY])
        rs_lanstatus.append(lan_status)
        rs_antenna.append(radio_stat[AppMsgs.STARTUPDATE_KEY])
        summary = radio_stat[AppMsgs.SUMMARY_KEY]
        rs_uptime.append(get_summary_string(summary))
        download_settings = radio_stat[AppMsgs.EMERALD_KEY]
        upload_settings = radio_stat[AppMsgs.EMERALD_KEY]
        rs_download_settings.append(get_download_settings_string(download_settings))
        rs_upload_settings.append(get_upload_settings_string(upload_settings))
        rs_download.append(get_download_string(radio_stat))
        rs_upload.append(get_upload_string(radio_stat))
        rs_logid.append(get_logid_string(radio_stat))
    
    rs_download[len(rs_download)-1] = AppMsgs.UNTESTED
    rs_upload[len(rs_upload)-1] = AppMsgs.UNTESTED
    rs_logid[len(rs_logid)-1] = AppMsgs.UNTESTED
    
    return rs_download, rs_upload, rs_in_bytes, rs_out_bytes, rs_timestamp, rs_lanstatus, rs_ping, rs_antenna, rs_uptime, rs_logid,\
            rs_download_settings, rs_upload_settings

def strip_radio_stats(description_text):
    separator_str = AppMsgs.BOLD_CONTAINER.format('*' * 40)
    split_description = description_text.split(separator_str)
    return split_description[0]

def safe_cast(val, to_type, default=None):
    try:
        return to_type(val)
    except (ValueError, TypeError):
        return default
    
def wrap_list_content_str(lst):
    return [str(lst.id), str(lst.activity), str(lst.method), str(lst.date), str(lst.status), str(lst.request), str(lst.response)]