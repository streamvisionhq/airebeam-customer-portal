from django import forms

class LoginStatus:
	SUCCESS = 1
	MBR_NOT_EXIST = 3
	AUTHENTICATION_FAILED = 4
	CUSTOMER_ACCESS_OK_INACTIVE = 5
	CUSTOMER_ACCESS_OK_ERROR = 6
	
class FieldsType:
	TEXT = 'text'
	SELECT = 'select'
	DATE = 'date'
	CHECKBOX = 'checkbox_select_multiple'
	RADIO = 'radio'
	PASSWORD = 'password'
	TEXTAREA = 'textarea'

form_fields_map = {'text':forms.CharField, 'password':forms.CharField, 'select':forms.ChoiceField,
					'date': forms.DateField, 'checkbox_select_multiple':forms.BooleanField,
					'radio':forms.ChoiceField, 'textarea':forms.CharField};
					
user_access_level = {'complete':1, 'shallow':2}

class SendEmailStatus:
	SUCCESS = 1
	
class SendSmsStatus:
	SUCCESS = 1
	TOKEN_NOT_MATCHED = 12
	
class ChangePasswordStatus:
	SUCCESS = 1
	INCORRECT_PASSWORD = 17

class ForgotPasswordStatus:
	SUCCESS = 1
	MISSING_REQUIRED_PARAMS = 2
	MBR_NOT_EXIST = 3
	PHONE_ADDRESS_MISSING = 8
	CREDIT_CARD_MISSING = 9
	
class AppStatus:
	SUCCESS = 'success'
	WARNING = 'warning'
	ERROR 	= 'failure'
	EXCEPTION = 'exception'
	
class AjaxCallStatus:
	NOT_STARTED = 0
	INPROGRESS = 1
	COMPLETED = 2
	
class PasswordResetMethod:
	BOTH_EMAIL_CELLPHONE = 1
	EMAIL = 2
	CELLPHONE = 3
	
class SHALLOWORRESET:
	DASHBOARD = 1
	RESET = 2
	ERROR = 3

class PasswordForceReset:
	NO_RESET = 0
	FORCE_RESET = 1
	
class FdMapLevel:
	RELIABLE_CONTACT = 1
	SEMI_RELIABLE_CONTACT = 2
	PORTAL_RELIABLE_CONTACT = 3
	
class ResetPasswordMedium:
	SMS = 'sms'
	
class Protocol:
	HTTPS = 'https'
	HTTP  = 'http'