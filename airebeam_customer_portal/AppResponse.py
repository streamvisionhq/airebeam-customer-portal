from enum import AppStatus
from static_values import AppMsgs

class AppResponse:
    @staticmethod
    def get_success_response(response=''):
        return {AppMsgs.MSG: AppStatus.SUCCESS, AppMsgs.RESP:response}
    
    @staticmethod
    def get_failure_reponse(response=''):
        return {AppMsgs.MSG: AppStatus.ERROR, AppMsgs.RESP:response}
    
    
    @staticmethod
    def get_exception_reponse(response=''):
        return {AppMsgs.MSG: AppStatus.EXCEPTION, AppMsgs.RESP:response}
    
    @staticmethod
    def get_logged_status_response(msg='', status=0):
        return {AppMsgs.MSG:AppStatus.ERROR, AppMsgs.STATUS_KEY:status}