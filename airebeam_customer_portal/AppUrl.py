from static_values import BaseUrl

class AppUrl:
    @staticmethod
    def get_requester_tickets_url(requesterId, page, perPage):
        url = BaseUrl.TICKET_URL + '?requester_id={0}' + '&order_by=updated_at&page={1}&per_page={2}'
        url = url.format(requesterId, page, perPage)
        return url
    
    @staticmethod
    def get_ticket_detail_url(ticketId):
        url = BaseUrl.TICKET_URL + '/{0}?include=stats'.format(ticketId)
        return url
    
    @staticmethod
    def get_ticket_url():
        url = BaseUrl.TICKET_URL
        return url
    
    @staticmethod
    def get_ticket_url_with_id(ticketId):
        url = BaseUrl.TICKET_URL + '/' + ticketId
        return url
    
    @staticmethod
    def get_login_url(mbr, password):
        url = BaseUrl.LOGIN_URL + '?mbr={0}&password={1}'.format(mbr, password)
        return url
    
    @staticmethod
    def get_accept_terms_url(mbr):
        url = BaseUrl.ACCEPT_TERMS_URL + '/{0}'.format(mbr)
        return url
    
    @staticmethod
    def get_forgot_password_url():
        url = BaseUrl.FORGOT_PASSWORD_URL
        return url
    
    @staticmethod
    def get_change_password_url():
        url = BaseUrl.CHANGE_PASSWORD_URL
        return url
    
    @staticmethod
    def get_onlinecheck_url(mbr):
        url = BaseUrl.ONLINECHECK_URL + '?mbr={0}'.format(str(mbr))
        return url
        
    @staticmethod
    def get_mbr_url(mbr):
        url = BaseUrl.MBR_URL + '?mbr={0}'.format(str(mbr))
        return url
    
    @staticmethod
    def get_speedtest_url():
        url = BaseUrl.SPEEDTEST_URL
        return url
    
    @staticmethod
    def get_create_conversation_url(ticketId):
        url = BaseUrl.TICKET_URL + ('/{0}/reply'.format(ticketId))
        return url
    
    @staticmethod
    def get_conversations_url(ticketId, page, perPage):
        url = BaseUrl.TICKET_URL + ('/{0}/conversations?page={1}&per_page={2}'.format(ticketId, page, perPage))
        return url
    
    @staticmethod
    def get_customer_url():
        url = BaseUrl.CONTACT_URL
        return url
    
    @staticmethod
    def get_search_customer_url(mbr):
        url = BaseUrl.CONTACT_URL + ('?phone={0}'.format(mbr)) 
        return url
    
    @staticmethod
    def get_airebeam_service_url(mbr):
        url = BaseUrl.AB_SERVICE_URL + ('/{0}'.format(mbr)) 
        return url
        
        
    @staticmethod
    def get_verify_captcha_url():
        url = BaseUrl.GCAPTCHA_URL
        return url
    
    @staticmethod
    def get_agent_login_url():
        url = BaseUrl.AGENT_LOGIN_URL
        return url
    
    @staticmethod
    def get_log_result_url():
        url = BaseUrl.LOG_RESULT_URL + '?xhr=true&logresults=true'
        return url
    
    @staticmethod
    def get_customer_info_url():
        url = BaseUrl.CUSTOMER_INFO_URL
        return url
    
    @staticmethod
    def get_send_email_url(mbr, resetPageUrl):
        url = BaseUrl.SEND_EMAIL_URL + '?mbr={0}&url={1}'.format(mbr, resetPageUrl)
        return url
    
    @staticmethod
    def get_update_password_url():
        url = BaseUrl.UPDATE_PASSWORD_URL
        return url
    
    @staticmethod
    def get_send_sms_url(mbr):
        url = BaseUrl.SEND_SMS_URL + '?mbr={0}'.format(mbr)
        return url
    
    @staticmethod
    def get_verify_sms_code_url(mbr, code):
        url = BaseUrl.VERIFY_SMS_CODE_URL + '?mbr={0}&code={1}'.format(mbr, code)
        return url