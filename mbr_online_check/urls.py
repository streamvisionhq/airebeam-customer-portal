from django.conf.urls import url
from mbr_online_check.views.MbrDetailsView import MbrDetailsView
from mbr_online_check.views.OnlineCheckView import OnlineCheckView
from mbr_online_check.views.MbrExistService import MbrExistService
from mbr_online_check.views.FetchMbrDetailsService import FetchMbrDetailsService

app_name='mbr_online_check'

urlpatterns = [
    url(r'^account$', MbrDetailsView.as_view(), name='mbrDetails'),
    url(r'^onlinecheck/$', OnlineCheckView.as_view(),name='onlineCheck'),
    url(r'^mbr_exist/$', MbrExistService.as_view(), name='mbr_exist'),
    url(r'^mbr_found_on_emerald/(?P<mbr_id>[0-9]+)$', FetchMbrDetailsService.as_view(), 
        name = 'mbr_found_on_emerald'),
 ]