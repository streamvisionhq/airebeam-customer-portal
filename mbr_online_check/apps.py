from __future__ import unicode_literals

from django.apps import AppConfig


class MbrOnlineCheckConfig(AppConfig):
    name = 'mbr_online_check'
