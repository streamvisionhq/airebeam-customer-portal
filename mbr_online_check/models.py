from __future__ import unicode_literals
import string
import requests
import random
import xmltodict
import json
from airebeam_customer_portal.utils import savesessiondata,checksession
from django.db import models
from airebeam_customer_portal.static_values import AppMsgs, MbrDetailKeys
from airebeam_customer_portal.AppUrl import AppUrl
from airebeam_customer_portal import config

# Create your models here.

# ================================ MBR DETAIL ============================================

class mbrDetail():
    detail = {}

    def getmbrdetails(self,mbr,request):
        inSession = checksession(AppMsgs.MBR_DETAIL_KEY, request)
        if inSession:

            data        = request.session[AppMsgs.DATA_KEY]
            self.detail = data[AppMsgs.MBR_DETAIL_KEY]

        else:
            mbr_url = AppUrl.get_mbr_url(mbr)
            response     = callapi(mbr_url, request)
            if AppMsgs.ERROR in response:
                details = AppMsgs.ERROR
                self.detail = details
            else:
                details      = response[MbrDetailKeys.MBR_DETAIL][MbrDetailKeys.MBR_KEY]
                self.detail = details
                savesessiondata(AppMsgs.MBR_DETAIL_KEY,self.detail, request)

#    Get Mbr details and saved in session
    def fetch_mbr(self,mbr,request):
        mbr_url = AppUrl.get_mbr_url(mbr)
        response     = callapi(mbr_url, request)
        if AppMsgs.ERROR in response:
            details = AppMsgs.ERROR
            self.detail = details
        else:
            details      = response[MbrDetailKeys.MBR_DETAIL][MbrDetailKeys.MBR_KEY]
            self.detail = details
            savesessiondata(AppMsgs.MBR_DETAIL_KEY,self.detail, request)



# =================================== ONLINE CHECK =======================================
class onlineCheck():
    online_check = {}

    def getOnlineCheck(self, mbr, request):
          inSession = checksession(AppMsgs.ONLINECHECK_KEY, request)

          if inSession:
               data              = request.session[AppMsgs.DATA_KEY]
               self.online_check = data[AppMsgs.ONLINECHECK_KEY]

          else:
                onlinecheck_url = AppUrl.get_onlinecheck_url(mbr)
                response     = callapi(onlinecheck_url, request)

                if AppMsgs.ERROR not in response.keys():
                    onlinecheck       = response[AppMsgs.MBR_SEARCH][AppMsgs.CONNECTION]
                    self.online_check = onlinecheck
                    savesessiondata(AppMsgs.ONLINECHECK_KEY, self.online_check, request)
                else:
                    self.online_check = response



#=========================================================================================================

def callapi( url, request):
    data     = config.EmeraldCredentials
    cookie   = {AppMsgs.EMERWEB: ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(14))}
    response = requests.post(url, data=data, cookies=cookie)
    cookie   = response.cookies
    response = requests.get(url, cookies=cookie)
    data     = checkresponse(response, request)
    return data

def checkresponse( response, request):
    if response.status_code == 200:
        xmldata            = response.content
        data               = xmltodict.parse(xmldata)
        return data
    else:
        return 'unable to get data'