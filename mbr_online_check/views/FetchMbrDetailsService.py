from django.http import HttpResponse
import json
from airebeam_customer_portal.utils import getfromSession, savesessiondata, \
clear_user_data_keys
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.static_values import AppMsgs, MbrDetailKeys
from users.views.UserView import UserView
from mbr_online_check.models import mbrDetail
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
from airebeam_customer_portal.enum import AppStatus
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast
from airebeam_customer_portal.AppServices.LoginService import LoginService

class FetchMbrDetailsService(UserView):
    def __init__(self):
        self.mbr = ''
    
    def get(self, request, mbr_id):
        '''
        Fetches mbr details if MBR is exist on emerald.
        @param request:
        @param mbr_id:
        '''
        self.mbr = mbr_id
        if self.get_agent_username() is not None:
#         Logout and redirect to login page
            login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
            is_session_exceded = check_if_session_time_exceeds(login_time)
    #         Logout and redirect to login page
#             Return error response in json
            if is_session_exceded:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.MBR_EXIST_KEY:False}
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            
#             Remove all customer data from session.
            clear_user_data_keys(request)
            mbr_detail_obj = mbrDetail()
            mbr_detail_obj.fetch_mbr(self.mbr, request)
            if mbr_detail_obj.detail == AppMsgs.ERROR:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.MBR_EXIST_KEY:False}
            else:
                request.session[AppMsgs.USERNAME_KEY] = mbr_detail_obj.detail[MbrDetailKeys.FIRSTNAME_KEY]
                savesessiondata(AppMsgs.MBRKEY,self.mbr,self.request)
                LoginService.search_customer_on_fd_by_phone(self, self.mbr)
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.MBR_EXIST_KEY:True}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
             
        else:
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.MBR_EXIST_KEY:False}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
        