from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from mbr_online_check.models import onlineCheck
from airebeam_customer_portal.utils import getfromSession,checksession
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.static_values import AppMsgs
from users.views.UserView import UserView

class MbrDetailsView(UserView):
    template_name = 'mbr_online_check/view_mbr_detail.html'
    def get(self, request):
        if checksession(AppMsgs.MBRKEY, request):
            login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
            is_session_exceded = check_if_session_time_exceeds(login_time)
    #         Logout and redirect to login page
            if is_session_exceded:
                return HttpResponseRedirect(reverse('users:logout'))
            error = AppMsgs.NOT_FETCH_MBR_DETAILS
            username=''
            try:
                mbrdetail = getfromSession(AppMsgs.MBR_DETAIL_KEY, request)
                mbr = getfromSession(AppMsgs.MBRKEY, request)
                o = onlineCheck()
                o.getOnlineCheck(mbr, request)
                onlinecheck = o.online_check
            except:
                pass
            
            context = {AppMsgs.MBR_DETAIL_KEY: mbrdetail, AppMsgs.ONLINECHECK_KEY: onlinecheck, 
                       AppMsgs.ERROR: error, AppMsgs.USERNAME_KEY: username}
            return render(request, self.template_name, context)
    
        else:
            return HttpResponseRedirect(reverse('users:login'))
