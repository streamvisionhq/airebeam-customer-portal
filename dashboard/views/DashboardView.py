from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import checksession
from airebeam_customer_portal.utils import getfromSession
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.enum import AppStatus, PasswordForceReset
from users.views.UserView import UserView
from airebeam_customer_portal import utils

class DashboardView(UserView):
    '''
    Shows dashboard view where customer can see their mbr details, radio stats
    details, tickets made by him.
    '''
    def get(self, request):
        try:    
            if checksession(AppMsgs.MBRKEY,request) or \
            self.get_agent_username() is not None:
                    login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
                    is_session_exceded = check_if_session_time_exceeds(login_time)
                    force_reset = utils.safe_cast(self.get_force_reset_pass(), int)
                    reset_pass_token = self.get_reset_pass_token()
            #         Logout and redirect to login page
                    if is_session_exceded:
                        return HttpResponseRedirect(reverse('users:logout'))
                 
                    error = ''
                    success_str = request.GET.get('s', '')
     
                    context = {AppMsgs.ERROR: error, AppMsgs.FORCE_RESET_KEY: "0", 
                               AppMsgs.MBRKEY: "", AppMsgs.RESET_TOKEN_KEY: ""}
                    if success_str != '':
                        context[AppStatus.SUCCESS] = int(success_str)

#                   Set force reset password in dom to show password reset pop up
                    if force_reset is not None and force_reset == PasswordForceReset.FORCE_RESET:
                        context[AppMsgs.FORCE_RESET_KEY] = force_reset
                     
#                   Set token in dom to use this when resetting password                        
                    if reset_pass_token is not None and reset_pass_token != "":
                        context[AppMsgs.RESET_TOKEN_KEY] = reset_pass_token
                        
                    if checksession(AppMsgs.MBRKEY,request):
                        context[AppMsgs.MBRKEY] = getfromSession(AppMsgs.MBRKEY, request)
                    
                    context[AppMsgs.MBR_EXIST_KEY] = checksession(AppMsgs.MBRKEY,request)
                    
                    return render(request, 'dashboard/index.html', context)
                 
            else:
                return HttpResponseRedirect(reverse('users:login'))
        except Exception as e:
            return render(request, 'abcp_exception.html')