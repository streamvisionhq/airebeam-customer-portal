from django.conf.urls import url
from dashboard.views.DashboardView import DashboardView

app_name = 'dashboard'

urlpatterns = [
    url(r'^$', DashboardView.as_view(), name='index'),
 ]
