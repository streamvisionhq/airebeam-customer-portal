from __future__ import unicode_literals

from django.db import models
from airebeam_customer_portal import utils
from airebeam_customer_portal.static_values import AppMsgs, LogActivity
import os
# import datetime
from datetime import datetime as dt
from datetime import date
import datetime
from django.utils import timezone
import csv

# Create your models here.
class Logger(models.Model):
    method = models.CharField(max_length=20)
    status = models.PositiveSmallIntegerField()
    request = models.TextField()
    params = models.TextField()
    response = models.TextField()
    date = models.DateTimeField()
    activity = models.CharField(max_length=50, default=None)
    
    class Meta:
        db_table = "logger"
        
    @classmethod
    def log_api(self, method, status, request, params, response, activity=''):
        logger_model = Logger(method=method, status=status, params=params, request=request, response=response, activity=activity, date=dt.now())
        logger_model.save()
        return logger_model.id
    
    @classmethod
    def made_log_as_csv(cls):
        log_dir_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/ActivityLogs/'
        log_filename = str(date.today()) + '-logs.csv'
        log_file_path = log_dir_path + log_filename
        today_min = dt.combine(date.today(), datetime.time.min)
        today_max = dt.combine(date.today(), datetime.time.max)
        
        last_log_id = 0
        if(os.path.exists(log_file_path)):
            with open(log_file_path, 'r') as my_file:
                reader = csv.reader(my_file, delimiter=str(u'\n').encode('utf-8'))
                log_list = list(reader)
                last_log_id = log_list[len(log_list)-1][0].split(',')[0]
        
        raw_query = ''
        if last_log_id != 0:
            raw_query = 'SELECT * FROM `logger` WHERE `logger`.`date` BETWEEN "' + str(today_min) + '" AND "' + str(today_max) + '" AND (`logger`.`activity` = "' + LogActivity.CUSTOMER_LOGIN + '" OR `logger`.`activity` = "' + LogActivity.LOGOUT + '") AND `logger`.`id` > ' + str(last_log_id)
        else:
            raw_query = 'SELECT * FROM `logger` WHERE `logger`.`date` BETWEEN "' + str(today_min) + '" AND "' + str(today_max) + '" AND (`logger`.`activity` = "' + LogActivity.CUSTOMER_LOGIN + '" OR `logger`.`activity` = "' + LogActivity.LOGOUT + '")'
        logger_data_set = Logger.objects.raw(raw_query)
          
        with open(log_file_path, "a") as output:
            if last_log_id == 0:
                log_header = ['id', 'activity', 'method', 'date', 'status', 'request', 'response']
                output.write(','.join(log_header))
                output.write('\n')
            for lds in logger_data_set:
                lst = utils.wrap_list_content_str(lds)
                output.write(','.join(lst))
                output.write('\n')
                
                