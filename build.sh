#!/bin/bash
# $1 values: 0 - dev, 1 - staging, 2 - UAT, 3 - prod
# Runs collectstatic Django script and reboots Apache
# check if we are deploying tostaging (1)

if [ "$1" -lt "2" ]; then
	# activate the python virtual environment
	source /home/svadmin/airebeam/bin/activate
	# run collectstatic 
	cd /home/svadmin/airebeam/airebeam_customer_portal
    python manage.py collectstatic --noinput
	
    # restart apache
	sudo service apache2 restart
fi