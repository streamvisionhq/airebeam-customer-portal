from django.http import HttpResponse
import json
from airebeam_customer_portal.utils import checksession, getfromSession
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.AppServices.AcceptTermsService import AcceptTermsService
from airebeam_customer_portal.enum import AppStatus
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast

class TermsAcceptedService(UserView):
    '''
    Calls service if customer accepts terms of services.
    '''
    def get(self, request):
        if checksession(AppMsgs.MBRKEY, request):
            accept_terms_object = AcceptTermsService()
            mbr = getfromSession(AppMsgs.MBRKEY, request)
            accept_terms_response = accept_terms_object.call_service(mbr)
            if accept_terms_response[AppMsgs.MSG] == AppStatus.SUCCESS:
#               Set terms url to None, so its pop up does not show again.
                self.set_terms_url(None)
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS}
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
        else:
            response_dict = AppResponse.get_logged_status_response()
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
        