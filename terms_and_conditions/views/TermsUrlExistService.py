from django.http import HttpResponse
import json
from airebeam_customer_portal.utils import checksession
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast

class TermsUrlExistService(UserView):
    '''
    To show terms pop up, check whether term's url is exist in session or not.
    '''
    def get(self, request):
        terms_url = self.get_terms_url()
        if terms_url is not None:
#         Logout and redirect to login page
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.TERMS_URL:terms_url}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
             
        else:
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.TERMS_URL:terms_url}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
    