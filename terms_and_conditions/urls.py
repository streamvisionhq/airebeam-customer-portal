from django.conf.urls import url

from terms_and_conditions.views.TermsUrlExistService import TermsUrlExistService
from terms_and_conditions.views.TermsAcceptedService import TermsAcceptedService

app_name = 'terms'
urlpatterns = [
    url(r'^terms_url_exist$', TermsUrlExistService.as_view(), name = 'terms_url_exist'),
    url(r'^accept_terms$', TermsAcceptedService.as_view(), name = 'accept_terms'),
]
