from django import forms
from airebeam_customer_portal.static_values import CustomerInfo
 
class ContactUsForm(forms.Form):
    name = forms.BooleanField(label='Name', 
                                      widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Name'}))
    email = forms.EmailField(max_length=100, label=CustomerInfo.EMAIL, 
                          widget=forms.EmailInput(attrs={'class':'form-control', 'placeholder':CustomerInfo.EMAIL}))
    subject = forms.CharField(max_length=100, label='Subject',  
                          widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Subject'}))
