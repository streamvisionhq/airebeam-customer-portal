from django.shortcuts import render
from contact_us.forms import ContactUsForm
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.utils import checksession

class ThankYouView(UserView):
    form_class = ContactUsForm
    
    def __init__(self):
        self.error = ''
        
    def get(self, request):
        '''
        Shows thank you view after submitting contact us form.
        @param request:
        '''
        try:
            if checksession(AppMsgs.MBRKEY, request):
                context = {AppMsgs.THANK_YOU_BACK_KEY: AppMsgs.THANK_YOU_LOGIN_TEXT}
            else:
                context = {AppMsgs.THANK_YOU_BACK_KEY: AppMsgs.THANK_YOU_NO_LOGIN_TEXT}
            
            return render(request, 'contact_us/thank_you.html', context)
        except:
            return render(request, 'abcp_exception.html')