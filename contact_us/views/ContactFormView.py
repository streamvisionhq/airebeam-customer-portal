from django.shortcuts import render
from contact_us.forms import ContactUsForm
from airebeam_customer_portal.enum import AppStatus
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.ServicePayload import ServicePayload
from users.views.UserView import UserView
from tickets.enum import TicketStatus, TicketPriority, group_id_mappings,\
TicketType
import json
from airebeam_customer_portal.AppServices.TicketService import TicketService
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from airebeam_customer_portal.AppServices.GCaptchaservice import GCaptchaservice
from airebeam_customer_portal import config

class ContactFormView(UserView):
    form_class = ContactUsForm
    form_template_name = 'contact_us/contact_us_form.html'
    
    def __init__(self):
        self.error = ''
        self.name = ''
        self.email = ''
        self.subject = ''
        self.message = ''
        self.g_captcha_response = ''
    
    def set_captcha(self):
        '''
        Stores captcha key in session.
        '''
        self.set_captcha_key(config.CaptchaKey)
    
    def get(self, request):
        '''
        Shows contact us form
        @param request:
        '''
        try:
            self.error = ''
            form = self.form_class()
            self.set_captcha()
            context = {AppMsgs.FORMKEY: form, AppMsgs.ERROR: self.error}
            success_str = request.GET.get('s', '')
            if success_str != '':
                context[AppStatus.SUCCESS] = int(success_str)
                
            return render(request, self.form_template_name, context)
        except:
            return render(request, 'abcp_exception.html')
        
    def post(self, request):
        '''
        Processes form, verify captcha and issue contact us ticket on fresh desk
        @param request:
        '''
        try:
            self.error = ''
            form = self.form_class(request.POST)
            if form.is_valid():
                self.name = form.data[AppMsgs.NAME]
                self.email = form.data[AppMsgs.EMAIL]
                self.subject = form.data[AppMsgs.SUBJECT_KEY]
                self.message = form.data[AppMsgs.DESCRIPTION]
                self.g_captcha_response = form.data[AppMsgs.RECAPTCHA_RESPONSE]
                captcha_service_object = GCaptchaservice()
#                 Verify if user passes captcha
                captcha_payload = ServicePayload.get_captcha_payload(config.CaptchaSecret, self.g_captcha_response)
                verify_captcha_response = captcha_service_object.verify_captcha(captcha_payload)
                if verify_captcha_response[AppMsgs.RESP][AppStatus.SUCCESS]:
                    status = TicketStatus.OPEN
                    priority = TicketPriority.LOW
                    fd_group_id = group_id_mappings[AppMsgs.SUPPORT]
                    ticket_type = TicketType.OTHER
                    payload = ServicePayload.get_general_ticket_payload(priority, self.message, 
                                                                        self.subject, status, self.email, fd_group_id, ticket_type)
                    payload = json.dumps(payload)
                    ticket_service_object = TicketService(request)
#                   Generate contact us ticket on fresh desk and redicrect to thank you page  
                    ts_response = ticket_service_object.create_ticket(payload)
                    url = reverse('contact_us:index')
                    if ts_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
                        url = reverse('contact_us:thank_you')
                        return HttpResponseRedirect(url)
                    else:
                        url += '?s=0'
                        return HttpResponseRedirect(url)
                else:
                    self.error = AppMsgs.CAPTCHA_ERROR
                            
            return render(request, self.form_template_name, {
                    AppMsgs.FORMKEY: form, AppMsgs.ERROR: self.error
                })
            
        except:
            return render(request, 'abcp_exception.html')