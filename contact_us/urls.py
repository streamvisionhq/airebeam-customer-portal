from django.conf.urls import url

from contact_us.views.ContactFormView import ContactFormView
from contact_us.views.ThankYouView import ThankYouView

app_name = 'contact_us'
urlpatterns = [
    url(r'thank_you$', ThankYouView.as_view(), name = 'thank_you'),
    url(r'$', ContactFormView.as_view(), name = 'index'),
]