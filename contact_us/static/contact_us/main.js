$( document ).ready(function() {

    $("input").attr("placeholder",'');

	$('#description').summernote({
		height: 300,   //set editable area's height
		focus: true,   //set focus editable area after Initialize summernote
		toolbar: [
		          // [groupName, [list of button]]
		          ['acp-font-style', ['bold', 'underline', 'clear']],
		          ['acp-font-color', ['color']],
		          ['acp-paragraph', ['ul', 'ol', 'paragraph']],
		          ['acp-insert', ['table', 'link']],
		          ['acp-help', ['help']]
		        ]
		});
	
	$("#description").on("summernote.change", function (e) {   // callback as jquery custom event
    	var description_text = $('#description').code();
    	$('#description_value').val(description_text);
    });
	setTimeout(hide_ticket_msg, 10000);
});

function hide_ticket_msg(){ 
	$('#ticket_submit_msg').hide("slow"); 
}

//Write this because we want to disable submit button
//Submit button makes problem if we hit more than one
function submit_form(){
	var $fp_form = $('#contact_form');
	var $fp_submit_btn = $('#contact_submit_btn');
	var is_valid = validate_fields();
	if(is_valid){
		$fp_submit_btn.prop('disabled', true);
		$fp_form.submit();
	}
}

function return_to_login(){
	window.location = '/'
}

function validate_fields(){
	var $desc_div = $('#description');
	if($desc_div.length){
		var $description = $('#description_value');
		var description = $description.val();
		var description_check = description + ' ';
		description_check = description.replace(/(&nbsp;)+/g, '');
		description_check = description_check.replace(/<p>/g, '');
		description_check = description_check.replace(/<\/p>/g, '');
		description_check = description_check.replace(/<br>/g, '');
		description_check = $.trim(description_check);
		if(description_check == ''){
			alert('Please fill description');
			return false;
		}
	}
	
	return true;
}


function go_to_dashboard(){
	window.location = '/';
}