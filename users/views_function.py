from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import checksession,getfromSession
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal import config

def check_session(request):
    # if request.session.has_key(AppMsgs.MBRKEY):
    if checksession(AppMsgs.MBRKEY,request):
        return True
    else:
        return False

def loggedin(request):
    if check_session( request):
        mbr = getfromSession(AppMsgs.MBRKEY,request)
        return render(request, 'users/loggedin.html', {AppMsgs.MBRKEY : mbr})
    else:
        return HttpResponseRedirect(reverse('users:login'))
 
def extend_base_view(request):
    return render(request, 'users/extend_from_base.html')
    
def get_pdf(request):
    import requests
    r = requests.get(config.PdfUrl, stream=True)
    data = r.content
    return HttpResponse(data, content_type='application/pdf')