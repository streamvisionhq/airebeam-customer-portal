from django.conf.urls import url

from users import views_function
from users.views.LoginFormView import LoginFormView
from users.views.LogoutView import LogoutView

app_name = 'users'
urlpatterns = [
	url(r'^loggedin$', views_function.loggedin, name = 'loggedin'),
	url(r'^login$', LoginFormView.as_view(), name='login'),
	url(r'^logout$', LogoutView.as_view(), name = 'logout'),
	url(r'^extend_base_view$', views_function.extend_base_view, name = 'extend_base_view'),
	url(r'^get_pdf$', views_function.get_pdf, name = 'get_pdf'),	
]