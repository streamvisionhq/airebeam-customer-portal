from django import forms
import json
from airebeam_customer_portal.enum import form_fields_map
 
class LoginForm(forms.Form):
    mbr = forms.CharField(max_length=100, label='Account Number', 
                          widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Account Number'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Password'}))
  

def get_class( self, kls ):
		parts = kls.split('.')
		module = ".".join(parts[:-1])
		m = __import__( module )
		for comp in parts[1:]:
			m = getattr(m, comp)            
		return m
