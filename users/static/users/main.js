$( document ).ready(function() {
	$( "#id_mbr" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     submit_form();
		}
	});
	
	$( "#id_password" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     submit_form();
		}
	});
	
});

// Write this because we want to disable submit button
// Submit button makes problem if we hit more than one
function submit_form(){
	var $login_form = $('#login_form');
	var $login_btn = $('#login_btn');
	$login_btn.prop('disabled', true);
	$login_form.submit();
	
}