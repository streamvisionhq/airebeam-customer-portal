from django.views import View
from airebeam_customer_portal.static_values import AppMsgs

class UserView(View):
    def __init__(self):
        super(UserView, self).__init__()
        
    def set_access_level(self, accessLevel):
        self.request.session[AppMsgs.USER_ACCESS_LEVEL_KEY] = accessLevel

    def get_access_level(self):
        return self.request.session[AppMsgs.USER_ACCESS_LEVEL_KEY]
    
    def set_speedtest_call_status(self, callStatus):
        self.request.session[AppMsgs.SPEEDTEST_CALL_STATUS_KEY] = callStatus
    
    def get_speedtest_call_status(self):
        return self.request.session[AppMsgs.SPEEDTEST_CALL_STATUS_KEY]
    
    def set_last_speedtest_time(self, time):
        self.request.session[AppMsgs.LAST_SPEEDTEST_TIME_KEY] = time
    
    def get_last_speedtest_time(self):
        if AppMsgs.LAST_SPEEDTEST_TIME_KEY in self.request.session:
            return self.request.session[AppMsgs.LAST_SPEEDTEST_TIME_KEY]
        else:
            return None
        
    def get_radiostats_call_status(self):
        return self.request.session[AppMsgs.RADIOSTATS_CALL_STATUS]
    
    def set_radiostats_call_status(self, callStatus):
        self.request.session[AppMsgs.RADIOSTATS_CALL_STATUS] = callStatus
        
    def set_agent_username(self, username):
        self.request.session[AppMsgs.AGENT_USERNAME] = username

    def get_agent_username(self):
        if AppMsgs.AGENT_USERNAME in self.request.session: 
            return self.request.session[AppMsgs.AGENT_USERNAME]
        else:
            return None
        
    def set_version_num(self, versionNum):
        self.request.session[AppMsgs.VERSION_NUM] = versionNum
        
    def set_temp_speedtest_result(self, temp_speedtest_result):
        self.request.session[AppMsgs.TEMP_SPEEDTEST_RESULT] = temp_speedtest_result

    def get_temp_speedtest_result(self):
        if AppMsgs.TEMP_SPEEDTEST_RESULT in self.request.session: 
            return self.request.session[AppMsgs.TEMP_SPEEDTEST_RESULT]
        else:
            return None
        
    def set_terms_url(self, termsUrl):
        self.request.session[AppMsgs.TERMS_URL] = termsUrl

    def get_terms_url(self):
        if AppMsgs.TERMS_URL in self.request.session: 
            return self.request.session[AppMsgs.TERMS_URL]
        else:
            return None
        
    def set_terms_pdf(self, termsPdf):
        self.request.session[AppMsgs.TERMS_PDF] = termsPdf

    def get_terms_pdf(self):
        if AppMsgs.TERMS_PDF in self.request.session: 
            return self.request.session[AppMsgs.TERMS_PDF]
        else:
            return None
        
    def set_captcha_key(self, captchaKey):
        self.request.session[AppMsgs.CAPTCHA_KEY] = captchaKey
        
    def set_customer_info(self, customerInfo):
        self.request.session[AppMsgs.CUSTOMER_INFO] = customerInfo
        
    def get_customer_info(self):
        if AppMsgs.CUSTOMER_INFO in self.request.session: 
            return self.request.session[AppMsgs.CUSTOMER_INFO]
        else:
            return None
        
    def set_force_reset_pass(self, forceReset):
        self.request.session[AppMsgs.SESSION_FORCE_RESET] = forceReset
        
    def get_force_reset_pass(self):
        if AppMsgs.SESSION_FORCE_RESET in self.request.session: 
            return self.request.session[AppMsgs.SESSION_FORCE_RESET]
        else:
            return None
        
    def set_reset_pass_token(self, resetToken):
        self.request.session[AppMsgs.RESET_TOKEN] = resetToken
        
    def get_reset_pass_token(self):
        if AppMsgs.RESET_TOKEN in self.request.session: 
            return self.request.session[AppMsgs.RESET_TOKEN]
        else:
            return None