from django.shortcuts import render,redirect
from users.forms import LoginForm
import base64
from airebeam_customer_portal.enum import LoginStatus, AppStatus, AjaxCallStatus
from airebeam_customer_portal.utils import savesessiondata,checksession
from airebeam_customer_portal.AppServices.LoginService import LoginService
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
import datetime
from mbr_online_check.models import mbrDetail
from airebeam_customer_portal.enum import user_access_level
from airebeam_customer_portal.static_values import AppMsgs, MbrDetailKeys,\
    ExceptionMsgs
from .UserView import UserView
from airebeam_customer_portal.settings import VERSION_FILE_PATH
from airebeam_customer_portal import config
from requests.exceptions import RequestException, ConnectionError
from airebeam_customer_portal.AcpException import AcpException

class LoginFormView(UserView):
    '''
    Login view for customers.
    '''
    form_class = LoginForm
    form_template_name = 'users/login_form.html'
    exception_template = 'abcp_exception.html'
    
    def get_version_num(self):
        '''
        Fetches customer portal version number.
        '''
        with open(VERSION_FILE_PATH) as f:
            version_num = f.read()
            self.set_version_num(version_num)
            
    def get_terms_pdf(self):
        '''
        Fetches and stores term's pdf url in session.
        '''
        
        self.set_terms_pdf(config.PdfUrl)
    
    def __init__(self):
        self.error = ''
        self.mbr = ''
        self.password = ''
    
    def get(self, request):
        '''
        Shows login form or redirect customer if they are already logged in
        @param request:
        '''
        try:
            self.get_version_num()
            self.get_terms_pdf()
            if checksession(AppMsgs.MBRKEY,request) or \
            self.get_agent_username() is not None:
                return redirect('dashboard:index')
            self.error = ''
            form = self.form_class()
            return render(request, self.form_template_name, {
                AppMsgs.FORMKEY:form, AppMsgs.ERROR:self.error
            })
            
        except IOError as e:
#            File does not contain variable
            file_name = e.filename.split('/')
            file_name = file_name[len(file_name) - 1]
            message = e.strerror + ' ' +  file_name
            except_dict = { AppMsgs.EXCEPT_MSG:message}   
            return render(request, self.exception_template, except_dict)
        
        except AttributeError as e:
#            File does not contain variable
            message = ExceptionMsgs.ATTRIBUTE_EXCEPTION
            except_dict = { AppMsgs.EXCEPT_MSG:message}   
            return render(request, self.exception_template, except_dict)
            
        except Exception as e:
            request.session.flush()
            message = ExceptionMsgs.GENERAL_EXCEPTION
            except_dict = { AppMsgs.EXCEPT_MSG:message}   
            return render(request, self.exception_template, except_dict)
    def post(self, request):
        '''
        Processes form parameters and redirect to dashboard.
        @param request:
        '''
        try:
            form = self.form_class(request.POST)
            # Process form if data is valid
            if form.is_valid():
                self.__set_form_fields(form)
                if(self.__login_customer_into_portal()):
                    return redirect('dashboard:index')
        
            return render(request, self.form_template_name, {
                AppMsgs.FORMKEY:form, AppMsgs.ERROR:self.error
            })
        
        except AttributeError as e:
#            File does not contain variable
            message = ExceptionMsgs.ATTRIBUTE_EXCEPTION
            except_dict = { AppMsgs.EXCEPT_MSG:message}   
            return render(request, self.exception_template, except_dict)
        except AcpException as e:
            request.session.flush()
            if len(e.args) > 1:
                if type(e.args[1]) is str:
                    message = e.args[1]
            else:
                message = ExceptionMsgs.GENERAL_EXCEPTION
            except_dict = { AppMsgs.EXCEPT_MSG:message}   
            return render(request, self.exception_template, except_dict)

    def __set_form_fields(self, form):
        '''
        Method set mbr and password attributes which is given by customer.
        @param form:
        '''
        self.mbr = form.cleaned_data[AppMsgs.MBRKEY]
        self.password = form.cleaned_data[AppMsgs.PASSWORD_KEY]
        self.password = base64.b64encode(self.password)
        
    def __login_customer_into_portal(self):
        '''
        Method calls login service and set customer data in session.
        '''
        login_service_object = LoginService()
        ls_response = login_service_object.call_service(self.mbr, self.password)
        if ls_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS and \
            int(ls_response[AppMsgs.RESP][AppMsgs.STATUS_KEY]) == LoginStatus.SUCCESS:
            response_obj = ls_response[AppMsgs.RESP]
            user_acl = user_access_level[AppMsgs.COMPLETE_KEY]
            LoginService.set_customer_data(self, response_obj, user_acl, self.mbr)
            mbr = LoginService.fetch_mbr_details(self, self.mbr)
            self.request.session[AppMsgs.USERNAME_KEY] = mbr.detail[MbrDetailKeys.FIRSTNAME_KEY]
            LoginService.search_customer_on_fd_by_phone(self, self.mbr)
            return True    
        elif ls_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS and \
            int(ls_response[AppMsgs.RESP][AppMsgs.STATUS_KEY]) == LoginStatus.MBR_NOT_EXIST:
            self.error = AppMsgs.LOGIN_CREDENTIALS_INVALID
        elif ls_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS and \
            int(ls_response[AppMsgs.RESP][AppMsgs.STATUS_KEY]) == LoginStatus.AUTHENTICATION_FAILED:
            self.error = AppMsgs.LOGIN_CREDENTIALS_INVALID
        elif ls_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS and \
            int(ls_response[AppMsgs.RESP][AppMsgs.STATUS_KEY]) == LoginStatus.CUSTOMER_ACCESS_OK_INACTIVE:
            self.error = AppMsgs.LOGIN_CREDENTIALS_INVALID
        elif ls_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS and \
            int(ls_response[AppMsgs.RESP][AppMsgs.STATUS_KEY]) == LoginStatus.CUSTOMER_ACCESS_OK_ERROR:
            self.error = AppMsgs.CUSTOMER_ACCESS_NOT_EXIST
        else:
            pass
        
        return False