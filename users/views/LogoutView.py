from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from .UserView import UserView
from logger.models import Logger
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.utils import getfromSession

class LogoutView(UserView):
    '''
    Destroy user session and redirect to customer login view.
    '''
    def get(self, request):
        try:
            mbr = getfromSession(AppMsgs.MBRKEY,request)
            params = {'mbr':str(mbr)}
            Logger.log_api('', 200, str(params), '', '', activity='Logout');
            Logger.made_log_as_csv()
            request.session.flush()
            return HttpResponseRedirect(reverse('users:login'))
        except:
            pass
        return HttpResponseRedirect(reverse('users:loggedin'))