from django.shortcuts import render,redirect
from users.forms import LoginForm
import base64
from airebeam_customer_portal.enum import LoginStatus, AppStatus, AjaxCallStatus
from airebeam_customer_portal.utils import savesessiondata,checksession
from airebeam_customer_portal.AppServices.LoginService import LoginService
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
import datetime
from mbr_online_check.models import mbrDetail
from airebeam_customer_portal.enum import user_access_level
from airebeam_customer_portal.static_values import AppMsgs
from .UserView import UserView
from airebeam_customer_portal.settings import VERSION_FILE_PATH
from airebeam_customer_portal import config

class Error404View(UserView):
    '''
    Responsible to show error404 page if given url is not exist
    '''
    form_class = LoginForm
    form_template_name = 'error404.html'
        
    def __init__(self):
        pass
    
    def get(self, request):
        try:
            return render(request, self.form_template_name)
        except:
            request.session.flush()
            return render(request, 'abcp_exception.html')
    