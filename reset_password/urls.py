from django.conf.urls import url

from reset_password.views.EmailApprovalView import EmailApprovalView
from reset_password.views.SmsApprovalView import SmsApprovalView
from reset_password.views.ExpiredLinkView import ExpiredLinkView
from reset_password.views.CallApprovalView import CallApprovalView
from reset_password.views.ResetPasswordView import ResetPasswordView
from reset_password.views.SendEmailApprovalService import SendEmailApprovalService
from reset_password.views.ResetPassService import ResetPassService
from reset_password.views.SendSmsApprovalService import SendSmsApprovalService
from reset_password.views.VerifyCodeService import VerifyCodeService
from reset_password.views.ChooseResetMethodView import ChooseResetMethodView
from reset_password.views.UserHasContactView import UserHasContactView
from reset_password.views.CustomerContactInfoView import CustomerContactInfoView
from reset_password.views.ThanksSubmitInfoView import ThanksSubmitInfoView

app_name = 'reset_password'
urlpatterns = [
    url(r'email_approval$', EmailApprovalView.as_view(), name = 'email_approval'),
    url(r'sms_approval$', SmsApprovalView.as_view(), name = 'sms_approval'),
    url(r'link_expired$', ExpiredLinkView.as_view(), name = 'link_expired'),
    url(r'call_approval$', CallApprovalView.as_view(), name = 'call_approval'),
    url(r'reset_view$', ResetPasswordView.as_view(), name = 'reset_view'),
    url(r'send_reset_pass_email$', SendEmailApprovalService.as_view(), name = 'send_reset_pass_email'),
    url(r'reset_pass_service$', ResetPassService.as_view(), name = 'reset_pass_service'),
    url(r'send_reset_pass_sms$', SendSmsApprovalService.as_view(), name = 'send_reset_pass_sms'),
    url(r'verify_sms_code$', VerifyCodeService.as_view(), name = 'verify_sms_code'),
    url(r'reset_method$', ChooseResetMethodView.as_view(), name = 'reset_method'),
    url(r'user_has_contact$', UserHasContactView.as_view(), name = 'user_has_contact'),
    url(r'customer_contact_info$', CustomerContactInfoView.as_view(), name = 'customer_contact_info'),
    url(r'thanks_submit_info$', ThanksSubmitInfoView.as_view(), name = 'thanks_submit_info'),
]