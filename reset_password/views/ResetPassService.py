from django.http import HttpResponse
import json
from airebeam_customer_portal.utils import getfromSession, savesessiondata, \
clear_user_data_keys
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.static_values import AppMsgs
from users.views.UserView import UserView
from mbr_online_check.models import mbrDetail
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
from warnings import catch_warnings
from airebeam_customer_portal.AppServices.SendEmailService import SendEmailService
from airebeam_customer_portal.enum import SendEmailStatus, Protocol
from airebeam_customer_portal.AppServices.UpdatePasswordService import UpdatePasswordService
from airebeam_customer_portal.ServicePayload import ServicePayload
import base64
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast

class ResetPassService(UserView):
    def __init__(self):
        pass
    
    def post(self, request):
        try:
            request_body = request.POST
            mbr = request_body[AppMsgs.MBRKEY]
            password = base64.b64encode(request_body[AppMsgs.PASSWORD_KEY])
            key = request_body[AppMsgs.RESET_PASS_KEY]
            medium = request_body[AppMsgs.RESET_PASS_MEDIUM]
            payload = ServicePayload.get_update_password_payload(mbr, password, key, medium)
            reset_pass_obj = UpdatePasswordService()
            reset_pass_response = reset_pass_obj.call_service(payload)
            reset_pass_status = int(reset_pass_response[AppMsgs.RESP][AppMsgs.STATUS_KEY])
            if  reset_pass_status == SendEmailStatus.SUCCESS:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.IS_PASSWORD_RESET:True}
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            else:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.IS_PASSWORD_RESET:False}
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
        except Exception as e:
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.IS_PASSWORD_RESET:False}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
