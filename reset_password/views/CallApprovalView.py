from django.shortcuts import render, redirect
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs, CustomFields,\
    MbrDetailKeys, CustomerInfo
from tickets.enum import TicketStatus, TicketPriority, group_id_mappings,\
    TicketType
from airebeam_customer_portal.ServicePayload import ServicePayload
import json
from airebeam_customer_portal.AppServices.TicketService import TicketService
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import checksession, getfromSession, savesessiondata
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
from airebeam_customer_portal import config
from airebeam_customer_portal.enum import AppStatus

class CallApprovalView(UserView):
    form_template_name = 'reset_password/call_approval.html'
    def __init__(self):
        self.error = ''
        
        
    def get(self, request):
        try:
            customer_info_obj = self.get_customer_info()
            if customer_info_obj is not None:
                phone_home = customer_info_obj[CustomerInfo.PHONEHOME]
                context = {AppMsgs.PHONE_HOME_KEY: phone_home}
                
                return render(request, self.form_template_name, context)
            else:
                return HttpResponseRedirect(reverse('forgot_password:index'))
        except:
            return render(request, 'abcp_exception.html')
        
    def post(self, request):
        try:
            mbr = getfromSession(AppMsgs.MBRKEY, request)
            if not checksession(AppMsgs.FD_CUSTOMER_ID_KEY, request):
                mbr_detail = getfromSession(AppMsgs.MBR_DETAIL_KEY,request)
                name = mbr_detail[MbrDetailKeys.FIRSTNAME_KEY] + ' ' + mbr_detail[MbrDetailKeys.LASTNAME_KEY]
                email =  mbr_detail[MbrDetailKeys.EMAIL_KEY]
                address = mbr_detail[MbrDetailKeys.ADDRESS1_KEY]
                phone_home = mbr_detail[MbrDetailKeys.PHONEHOME_KEY]
                mobile = mbr_detail[MbrDetailKeys.PHONEHOME_KEY]
                payload = ServicePayload.get_create_customer_payload(name, mbr, email, address, phone_home, mobile)
                payload = json.dumps(payload)
                customer_service_object = CustomerService()
                cs_response = customer_service_object.create_customer(payload)
                if cs_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
                    customer = cs_response[AppMsgs.RESP]
                    savedata = customer[AppMsgs.ID_KEY]
                    savesessiondata(AppMsgs.FD_CUSTOMER_ID_KEY, savedata, request)
                else:
                    url = reverse('dashboard:index')
                    url += '?s=-1'
                    return HttpResponseRedirect(url)
                
            if checksession(AppMsgs.FD_CUSTOMER_ID_KEY, request):
                customer_info_obj = self.get_customer_info()
                ticket_body = customer_info_obj[CustomerInfo.CUSTOMER_NAME] + ' ' + AppMsgs.CALL_APPROVAL_TICKET_BODY
                TestStr = config.TestStr
                ticket_title = AppMsgs.ACCOUNT_CHANGE
                mbr_detail = getfromSession(AppMsgs.MBR_DETAIL_KEY,request)
                ticket_subject = '{0}{1}, {2}, {3} - {4}'.format(TestStr, 
                                                           mbr_detail[MbrDetailKeys.MBR_KEY], 
                                                           mbr_detail[MbrDetailKeys.LASTNAME_KEY], 
                                                           mbr_detail[MbrDetailKeys.FIRSTNAME_KEY],
                                                           ticket_title)
                status = TicketStatus.OPEN
                fd_priority = TicketPriority.LOW
                fd_group = group_id_mappings[AppMsgs.SUPPORT]
                fd_type = TicketType.PORTAL_SETUP
                fd_customer_id = getfromSession(AppMsgs.FD_CUSTOMER_ID_KEY, request)
                custom_fields_obj = CustomFields()
                custom_fields = custom_fields_obj.get_mbr_to_no_support_payload(mbr)
                payload = ServicePayload.get_create_ticket_payload(fd_group, fd_type, fd_priority, 
                                                                   ticket_body, ticket_subject, status, 
                                                                   fd_customer_id, custom_fields)
                
                payload = json.dumps(payload)
                ticket_service_object = TicketService(request)
                ts_response = ticket_service_object.create_ticket(payload)
                url = reverse('dashboard:index')
                if ts_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
                    url += '?s=2'
                    response_dict = json.dumps(ts_response)
                    return redirect(url)
                else:
                    url += '?s=0'
                    return redirect(url)
            
        except Exception as e:
            return render(request, 'abcp_exception.html')