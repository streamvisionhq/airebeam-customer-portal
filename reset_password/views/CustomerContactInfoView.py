from django.shortcuts import render, redirect
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs, CustomFields,\
    MbrDetailKeys, ForgotPasswordPayload
from tickets.enum import TicketStatus, TicketPriority, group_id_mappings,\
    TicketType
from airebeam_customer_portal.ServicePayload import ServicePayload
import json
from airebeam_customer_portal.AppServices.TicketService import TicketService
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import checksession, getfromSession, savesessiondata,\
    check_if_session_time_exceeds
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
from airebeam_customer_portal import config
from airebeam_customer_portal.enum import AppStatus

class CustomerContactInfoView(UserView):
    form_template_name = 'reset_password/get_customer_contact_info.html'
    def __init__(self):
        self.error = ''
     
    def get(self, request):
        try:
            if checksession(AppMsgs.MBRKEY, request):
                login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
                is_session_exceded = check_if_session_time_exceeds(login_time)
        #         Logout and redirect to login page
                if is_session_exceded:
                    return HttpResponseRedirect(reverse('users:logout'))
            mbr = getfromSession(AppMsgs.MBRKEY,request)
            mbr_detail = getfromSession(AppMsgs.MBR_DETAIL_KEY,request)
            name = mbr_detail[MbrDetailKeys.FIRSTNAME_KEY] + ' ' + mbr_detail[MbrDetailKeys.LASTNAME_KEY]
            context = {AppMsgs.MBRKEY: mbr, AppMsgs.CUSTOMER_NAME_KEY: name}
            return render(request, self.form_template_name, context)
        except:
            return render(request, 'abcp_exception.html')
    
    def post(self, request):
        try:
            if checksession(AppMsgs.MBRKEY, request):
                login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
                is_session_exceded = check_if_session_time_exceeds(login_time)
        #         Logout and redirect to login page
                if is_session_exceded:
                    return HttpResponseRedirect(reverse('users:logout'))
                
                post_values = request.POST
                mbr = post_values[AppMsgs.MBRKEY]
                name = post_values[AppMsgs.NAME]
                email = post_values[AppMsgs.EMAIL]
                phone = post_values[ForgotPasswordPayload.PHONE_KEY] 
                status = TicketStatus.OPEN
                priority = TicketPriority.MEDIUM
                fd_group_id = group_id_mappings[AppMsgs.SUPPORT]
                ticket_type = TicketType.UPDATE_CUSTOMER_DATA
                ticket_title = AppMsgs.ACCOUNT_CHANGE
                fd_customer_id = getfromSession(AppMsgs.FD_CUSTOMER_ID_KEY, request)
                customer_info_obj = self.get_customer_info()
                TestStr = config.TestStr
                mbr_detail = getfromSession(AppMsgs.MBR_DETAIL_KEY,request)
                ticket_subject = '{0}{1}, {2}, {3} - {4}'.format(TestStr, 
                                                           mbr_detail[MbrDetailKeys.MBR_KEY], 
                                                           mbr_detail[MbrDetailKeys.LASTNAME_KEY], 
                                                           mbr_detail[MbrDetailKeys.FIRSTNAME_KEY],
                                                           ticket_title)
                description = AppMsgs.CUSTOMER_CONTACT_TICKET_BODY
                if email != '' and phone != '':
                    contact_desc = ' ' + AppMsgs.CUSTOMER_CONTACT_IF_EMAIL_PHONE_EXIST.format(email, phone)
                    description += contact_desc
                elif email != '':
                    contact_desc = ' ' + AppMsgs.CUSTOMER_CONTACT_IF_EMAIL_EXIST.format(email)
                    description += contact_desc
                elif phone != '':
                    contact_desc = ' ' + AppMsgs.CUSTOMER_CONTACT_IF_PHONE_EXIST.format(phone)
                    description += contact_desc
                    
                description += ' ' + AppMsgs.CUSTOMER_CONTACT_PLEASE 
                mbr_desc = '<div>MBR: {0}</div>'.format(mbr)
                customer_name_desc = '<div>Name: {0}</div>'.format(name)
                ticket_desc = '<div>{0} {1} {2}</div>'.format(mbr_detail[MbrDetailKeys.FIRSTNAME_KEY], mbr_detail[MbrDetailKeys.LASTNAME_KEY], description)
                ticket_body = '{0} {1} {2}'.format(mbr_desc, customer_name_desc, ticket_desc)
                
                payload = ServicePayload.get_customer_contact_info_payload(priority, ticket_body, 
                                                                           ticket_subject, status, fd_customer_id, fd_group_id, ticket_type)
                payload = json.dumps(payload)
                ticket_service_object = TicketService(request)
                ts_response = ticket_service_object.create_ticket(payload)
                url = reverse('dashboard:index')
                if ts_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
                    url += '?s=2'
                    response_dict = json.dumps(ts_response)
                    return HttpResponseRedirect(url)
                else:
                    url += '?s=0'
                    return HttpResponseRedirect(url)
        except:
            return render(request, 'abcp_exception.html')