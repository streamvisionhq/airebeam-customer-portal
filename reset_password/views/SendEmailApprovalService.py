from django.http import HttpResponse
import json
from airebeam_customer_portal.utils import getfromSession, savesessiondata, \
clear_user_data_keys
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.static_values import AppMsgs, CustomerInfo
from users.views.UserView import UserView
from mbr_online_check.models import mbrDetail
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
from warnings import catch_warnings
from airebeam_customer_portal.AppServices.SendEmailService import SendEmailService
from airebeam_customer_portal.enum import SendEmailStatus, Protocol
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast

class SendEmailApprovalService(UserView):
    def __init__(self):
        pass
    
    def get(self, request):
        try:
            hostname = request.get_host()
            customer_info_obj = self.get_customer_info()
            mbr = customer_info_obj[CustomerInfo.CUSTOMER_ID]
            protocol = SendEmailService.get_protocol(request)
            reset_page_url = protocol + '://' + hostname + '/reset_password/reset_view'
            send_email_obj = SendEmailService()
            send_email_response = send_email_obj.call_service(mbr, reset_page_url)
            send_email_status = int(send_email_response[AppMsgs.RESP][AppMsgs.STATUS_KEY])
            if  send_email_status == SendEmailStatus.SUCCESS:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.SEND_EMAIL:True}
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            else:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.SEND_EMAIL:False}
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
        except Exception as e:
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.SEND_EMAIL:False}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
        
