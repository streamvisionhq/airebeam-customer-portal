from django.shortcuts import render
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.utils import checksession
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

class ChooseResetMethodView(UserView):
    form_template_name = 'reset_password/choose_reset_method.html'
    def __init__(self):
        self.error = ''
        
        
    def get(self, request):
        try:
            customer_info_obj = self.get_customer_info()
            if customer_info_obj is not None:
                context = {}
                return render(request, self.form_template_name, context)
            else:
                return HttpResponseRedirect(reverse('forgot_password:index'))
        except:
            return render(request, 'abcp_exception.html')