from django.http import HttpResponse
import json
from airebeam_customer_portal.utils import getfromSession, savesessiondata, \
clear_user_data_keys
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.static_values import AppMsgs, CustomerInfo
from users.views.UserView import UserView
from mbr_online_check.models import mbrDetail
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
from warnings import catch_warnings
from airebeam_customer_portal.enum import SendEmailStatus, SendSmsStatus
from airebeam_customer_portal.AppServices.VerfiySmsCodeService import VerfiySmsCodeService
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast

class VerifyCodeService(UserView):
    def __init__(self):
        pass
    
    def get(self, request):
        try:
            customer_info_obj = self.get_customer_info()
            mbr = customer_info_obj[CustomerInfo.CUSTOMER_ID]
            code= request.GET[AppMsgs.SMS_CODE]
            verify_sms_code_obj = VerfiySmsCodeService()
            verify_code_response = verify_sms_code_obj.call_service(mbr, code)
            verify_code_status = int(verify_code_response[AppMsgs.RESP][AppMsgs.STATUS_KEY])
            if  verify_code_status == SendSmsStatus.SUCCESS:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.VERIFY_SMS_CODE:True}
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            elif  verify_code_status == SendSmsStatus.TOKEN_NOT_MATCHED:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.VERIFY_SMS_CODE:False, AppMsgs.STATUS_KEY:SendSmsStatus.TOKEN_NOT_MATCHED}
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            else:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.VERIFY_SMS_CODE:False}
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
        except Exception as e:
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.VERIFY_SMS_CODE:False}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
