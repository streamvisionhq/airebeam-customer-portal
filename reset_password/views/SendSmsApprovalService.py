from django.http import HttpResponse
import json
from airebeam_customer_portal.utils import getfromSession, savesessiondata, \
clear_user_data_keys
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.static_values import AppMsgs, CustomerInfo
from users.views.UserView import UserView
from mbr_online_check.models import mbrDetail
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
from warnings import catch_warnings
from airebeam_customer_portal.enum import SendEmailStatus, SendSmsStatus
from airebeam_customer_portal.AppServices.SendSmsCodeService import SendSmsCodeService
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast

class SendSmsApprovalService(UserView):
    def __init__(self):
        pass
    
    def get(self, request):
        try:
            customer_info_obj = self.get_customer_info()
            mbr = customer_info_obj[CustomerInfo.CUSTOMER_ID]
            send_sms_obj = SendSmsCodeService()
            send_sms_response = send_sms_obj.call_service(mbr)
            send_sms_status = int(send_sms_response[AppMsgs.RESP][AppMsgs.STATUS_KEY])
            if  send_sms_status == SendSmsStatus.SUCCESS:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.SEND_SMS:True}
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            else:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.SEND_SMS:False}
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
        except Exception as e:
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.SEND_SMS:False}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
