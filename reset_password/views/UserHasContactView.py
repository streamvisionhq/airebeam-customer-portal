from django.shortcuts import render, redirect
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs, CustomFields,\
    CustomerInfo
from tickets.enum import TicketStatus, TicketPriority, group_id_mappings,\
    TicketType
from airebeam_customer_portal.ServicePayload import ServicePayload
import json
from airebeam_customer_portal.AppServices.TicketService import TicketService
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import checksession, getfromSession, savesessiondata,\
    check_if_session_time_exceeds
from airebeam_customer_portal.AppServices.CustomerService import CustomerService

class UserHasContactView(UserView):
#     form_template_name = 'reset_password/call_approval.html'
    def __init__(self):
        self.error = ''
        
        
    def get(self, request):
        try:
            if checksession(AppMsgs.MBRKEY, request):
                login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
                is_session_exceded = check_if_session_time_exceeds(login_time)
        #         Logout and redirect to login page
                if is_session_exceded:
                    return HttpResponseRedirect(reverse('users:logout'))
                
            customer_info_obj = self.get_customer_info()
            if customer_info_obj is not None:
                phone_home = customer_info_obj[CustomerInfo.PHONEHOME]
                
                if phone_home != '':
#                     Redirect to call approval view
                    return HttpResponseRedirect(reverse('reset_password:call_approval'))
                else:
#                     Redirect to contact GetContactInfo view
                    return HttpResponseRedirect(reverse('reset_password:customer_contact_info'))
            else:
                return HttpResponseRedirect(reverse('forgot_password:index'))
        except:
            return render(request, 'abcp_exception.html')
        
