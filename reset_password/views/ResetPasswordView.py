from django.shortcuts import render, redirect
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs
from datetime import datetime
import base64
from airebeam_customer_portal.enum import ResetPasswordMedium

class ResetPasswordView(UserView):
    form_template_name = 'reset_password/reset_password_form.html'
    def __init__(self):
        self.error = ''
        
        
    def get(self, request):
        try:
            mbr = request.GET.get(AppMsgs.LOGIN_KEY, '')
            key = request.GET.get(AppMsgs.RESET_PASS_KEY, '')
            link_expiry = request.GET.get(AppMsgs.LINK_EXPIRY_KEY, '')
            from_sms_view = request.GET.get(AppMsgs.FROM_SMS_VIEW_KEY, '')
            context = {AppMsgs.ERROR: '', AppMsgs.MBRKEY: '', AppMsgs.RESET_TOKEN_KEY: ''}
            if from_sms_view == '1' and mbr != '' and key != '':
                context[AppMsgs.MBRKEY] = mbr
                context[AppMsgs.RESET_TOKEN_KEY] = key
                context[AppMsgs.RESET_MEDIUM_KEY] = ResetPasswordMedium.SMS
                return render(request, self.form_template_name, context)
            
            elif from_sms_view == '' and mbr != '' and key != '' and link_expiry != '':
                link_expiry = base64.b64decode(link_expiry)
                convert_expiry = datetime.strptime(link_expiry, '%Y-%m-%d %H:%M:%S')
                now_time = datetime.utcnow()
                time_diff = now_time - convert_expiry
#                     return to expired link page if link is older than 6 hrs                
                if time_diff.total_seconds() > 21600:
                    return redirect('reset_password:link_expired')
                else:
                    context[AppMsgs.MBRKEY] = mbr
                    context[AppMsgs.RESET_TOKEN_KEY] = key
                    context[AppMsgs.RESET_MEDIUM_KEY] = AppMsgs.EMAIL
                    return render(request, self.form_template_name, context)
            
            else:
                context[AppMsgs.ERROR] = AppMsgs.RESET_PASSWORD_PARAMETERS
                return render(request, self.form_template_name, context)
        except:
            return render(request, 'abcp_exception.html')