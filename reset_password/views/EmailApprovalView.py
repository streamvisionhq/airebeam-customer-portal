from django.shortcuts import render
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs, CustomerInfo
from airebeam_customer_portal.utils import checksession
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

class EmailApprovalView(UserView):
    form_template_name = 'reset_password/email_approval.html'
    def __init__(self):
        self.error = ''
        
        
    def get(self, request):
        try:
            customer_info_obj = self.get_customer_info()
            if customer_info_obj is not None:
                email = customer_info_obj[CustomerInfo.EMAIL]
                email_array = email.split('@')
                email_first_part = email_array[0]
                masked_email = email_first_part[0:2] + '******' + email_first_part[-2:] + '@' +  email_array[1]
                context = {AppMsgs.CUSTOMER_EMAIL_KEY: masked_email, 
                           AppMsgs.THANK_YOU_BACK_KEY: AppMsgs.THANK_YOU_NO_LOGIN_TEXT}
                
                return render(request, self.form_template_name, context)
            else:
                return HttpResponseRedirect(reverse('forgot_password:index'))
        except:
            return render(request, 'abcp_exception.html')