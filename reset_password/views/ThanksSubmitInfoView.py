from django.shortcuts import render, redirect
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs, CustomFields
from tickets.enum import TicketStatus, TicketPriority, group_id_mappings,\
    TicketType
from airebeam_customer_portal.ServicePayload import ServicePayload
import json
from airebeam_customer_portal.AppServices.TicketService import TicketService
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import checksession, getfromSession, savesessiondata,\
    check_if_session_time_exceeds
from airebeam_customer_portal.AppServices.CustomerService import CustomerService

class ThanksSubmitInfoView(UserView):
    form_template_name = 'reset_password/thanks_submit_contact_info.html'
    def __init__(self):
        self.error = ''
        
    def get(self, request):
        try:
            if checksession(AppMsgs.MBRKEY, request):
                login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
                is_session_exceded = check_if_session_time_exceeds(login_time)
        #         Logout and redirect to login page
                if is_session_exceded:
                    return HttpResponseRedirect(reverse('users:logout'))
            context = {}
            return render(request, self.form_template_name, context)
        except:
            return render(request, 'abcp_exception.html')