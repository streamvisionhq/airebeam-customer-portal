from django.shortcuts import render
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs, CustomerInfo
from airebeam_customer_portal.utils import checksession
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

class SmsApprovalView(UserView):
    form_template_name = 'reset_password/sms_approval.html'
    def __init__(self):
        self.error = ''
        
        
    def get(self, request):
        try:
            customer_info_obj = self.get_customer_info()
            if customer_info_obj is not None:
                mbr = customer_info_obj[CustomerInfo.CUSTOMER_ID]
                cell_phone = customer_info_obj[CustomerInfo.PHONEHOME]
                cell = [num for num in cell_phone if ord(num) >= 48 and ord(num)<= 57]
                cell_phone = ''.join(cell)
                cell_phone_masked = cell_phone[0:2] + '******' + cell_phone[-2:]
                context = {AppMsgs.CUSTOMER_CELLPHONE_KEY: cell_phone_masked, AppMsgs.MBRKEY: mbr,
                           AppMsgs.THANK_YOU_BACK_KEY: AppMsgs.THANK_YOU_NO_LOGIN_TEXT}
                
                return render(request, self.form_template_name, context)
            else:
                return HttpResponseRedirect(reverse('forgot_password:index'))
        except:
            return render(request, 'abcp_exception.html')