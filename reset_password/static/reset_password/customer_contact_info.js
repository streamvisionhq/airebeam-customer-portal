$(document).ready(function() {
	$("#id_phone").mask('(000)-000-0000');
	$("#id_phone").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
        	
        	if( e.which == 13 ) {
	   		     event.preventDefault();
	   		     submit_form();
	   		}
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
        
    });
	
	$('#customer_info_form').bootstrapValidator({
	    message: 'This value is not valid',
	    submitHandler: function(validator, form) {
	                // validator is the BootstrapValidator instance
	                // form is the jQuery object present the current form
//	    			var is_valid = validate_fields();
//	    			if(is_valid){
//	    				form.submit();
//	    			}
//	    			return false;
	            },
	            fields: {
	                email: {
	                    validators: {
	                    	emailAddress: {
	                            message: 'The input is not a valid email address'
	                        }
	                    }
	                }
	            }
	        });
	
	$( "#id_phone" ).keypress(press_submit);
	$( "#id_email" ).keypress(press_submit);
	
});

function press_submit( event ) {
	if( event.which == 13 ) {
	     event.preventDefault();
	     submit_form();
	}
}

function submit_form(){
	var $ci_form = $('#customer_info_form');
	var is_valid = validate_fields();
	if(is_valid){
		$ci_form[0].submit();
	}
}

function validate_fields(){
	var $phone_ref = $("input[name='phone']");
	var $email_ref = $("input[name='email']");
	var phone_val = $phone_ref.val().trim();
	var email_val = $email_ref.val().trim();
	if(phone_val == '' && email_val == ''){
//		show alert
		$('.atleast-one-req').show();
		return false;
	}
	
	return true;
	
}
