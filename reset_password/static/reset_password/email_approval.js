$( document ).ready(function() {
	$('#approve_send_email_btn').click(send_password_reset_email);
	$('#id_send_email_again').click({resend_code: true}, send_password_reset_email);
	
});

function send_password_reset_email(event){
	$(this).attr('disabled','');
	$(this).addClass("disabled");
	$.ajax({
    	cache: false,
        type : "get",
        url: '/reset_password/send_reset_pass_email',
        success: function(response){
        	if(response.send_email){
        		$('#reset_password_msg').show();
        		if(event.data != null){
//        			Show confirmation message
        			var $resend_div_ref = $('#resend_code_div');
        			$resend_div_ref.show();
        			setTimeout(hide_resend_code, 2000);
        		}
        	}
        	else{
        		$('#reset_password_msg_error').show();
        	}
        }
    });
}

function hide_resend_code(){ 
	var $resend_div_ref = $('#resend_code_div');
	$resend_div_ref.hide(); 
}