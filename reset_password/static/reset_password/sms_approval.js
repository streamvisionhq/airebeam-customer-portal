$( document ).ready(function() {
	
	$( "#sms_code_txt" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     verify_sms_code_reset();
		}
	});
	
	$('#approve_send_sms_btn').click(send_password_reset_code);
	$('#btn_verify_sms_code').click(verify_sms_code_reset);
	$('#id_send_code_again').click({resend_code: true}, send_password_reset_code);
});

function verify_sms_code_reset(event){
	var $sms_code_ref = $('#sms_code_txt');
	var sms_code_val = $.trim( $sms_code_ref.val() );
	var $mbr_ref = $('#mbr_value');
	var mbr_value = $mbr_ref.val();
	var $error_div_ref = $('#error_div');
	if(sms_code_val != ''){
		$('.site-holder').addClass('fixed-loadmask-msg');
		$('.site-holder').mask('<i class="  fa fa-refresh fa-spin"></i> Loading...');
		$('.site-holder .loadmask-msg').addClass('acp-loader-position');
		$('.site-holder .loadmask-msg div').addClass('acp-loader-font');
		$.ajax({
	    	cache: false,
	        type : "get",
	        url: '/reset_password/verify_sms_code?code=' + sms_code_val,
	        success: function(response){
	        	if(response.verify_sms_code){
//	        		Redirect to reset password
	        		window.location = '/reset_password/reset_view?fr_sms=1&login=' + mbr_value +  '&key=' + sms_code_val;
	        	}
	        	else if(response.status == 12){
	        		$error_div_ref.removeClass('no-display');
	        		$error_div_ref.html('You have inserted an invalid code. Please re-enter a valid code to proceed.');
	        	}
	        	else{
	        		$error_div_ref.removeClass('no-display');
	        		$error_div_ref.html('Error occurred. Please try again');
	        	}
	        	$('.site-holder .loadmask-msg').addClass('acp-loader-position');
	    		$('.site-holder .loadmask-msg div').addClass('acp-loader-font');
	        	$('.site-holder').unmask();
	        	$('.site-holder').removeClass('fixed-loadmask-msg');
	        }
	    });
	}
	else{
		alert('Please enter sms code');
	}
}

function send_password_reset_code(event){
	$(this).attr('disabled','');
	$(this).addClass("disabled");
	
	$.ajax({
    	cache: false,
        type : "get",
        url: '/reset_password/send_reset_pass_sms',
        success: function(response){
        	if(response.send_sms){
        		$('#reset_password_msg').show();
        		if(event.data != null){
//        			Show confirmation message
        			var $resend_div_ref = $('#resend_code_div');
        			$resend_div_ref.show();
        			setTimeout(hide_resend_code, 2000);
        		}
        	}
        	else{
        		$('#reset_password_msg_error').show();
        	}
        }
    });
}

function hide_resend_code(){ 
	var $resend_div_ref = $('#resend_code_div');
	$resend_div_ref.hide(); 
}