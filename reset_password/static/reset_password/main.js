$( document ).ready(function() {
	
	$( "#id_new_password" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     reset_password();
		}
	});
	
	$( "#id_confirm_new_password" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     reset_password();
		}
	});

});


//Write this because we want to disable submit button
//Submit button makes problem if we hit more than one
function reset_password(){
	var $fp_form = $('#reset_pass_form');
	var $rp_submit_btn = $('#rp_submit_btn');
	var is_valid = validate_fields();
	if(is_valid){
		$rp_submit_btn.prop('disabled', true);
		var $mbr_ref = $('#mbr_value');
		var $token_ref = $('#token_value');
		var $medium_ref = $('#medium_value');
		var $new_password_ref = $("[name='new_password']");
		var mbr = $mbr_ref.val();
		var key = $token_ref.val();
		var medium = $medium_ref.val();
		var password = $new_password_ref.val();
		reset_password_service(mbr, key, password, medium);
	}
}

function return_to_login(){
	window.location = '/'
}

function reset_password_service(mbr, key, password, medium){
	$.ajax({
    	cache: false,
        type : "post",
        url: '/reset_password/reset_pass_service',
        beforeSend: function(xhr, settings) {
            function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
    	},
    	data:{mbr:mbr, key:key, password:password, medium:medium},
        success: function(response){
        	var $error_div_ref = $('#error_div');
        	if(response.is_password_reset){
        		window.location = '/'
        	}
        	else{
        		var $rp_submit_btn = $('#rp_submit_btn');
        		$rp_submit_btn.prop('disabled', false);
        		$error_div_ref.removeClass('no-display');
        		$error_div_ref.html('Reset failed. Please try later');
        	}
        }
    });
}

function validate_fields(){
	var $new_password_ref = $("[name='new_password']");
	var $confirm_new_password_ref = $("[name='confirm_new_password']");
	var new_passwrod_val = $.trim( $new_password_ref.val() );
	var confirm_new_password = $.trim( $confirm_new_password_ref.val() );
	var $error_div_ref = $('#error_div');
	if(new_passwrod_val == "" || confirm_new_password == ""){
		$error_div_ref.removeClass('no-display');
		$error_div_ref.html('Please fill both passwords');
		return false;
	}
	else if(new_passwrod_val != confirm_new_password){
		$error_div_ref.removeClass('no-display');
		$error_div_ref.html('Passwords do not match');
		return false;
	}
	
	return true;
}


function go_to_dashboard(){
	window.location = '/';
}