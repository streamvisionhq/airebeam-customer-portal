$( document ).ready(function() {
	$('#rmethod_submit_btn').click(choose_reset_method);
});


function return_to_login(){
	window.location = '/'
}

function go_to_dashboard(){
	window.location = '/'
}

function choose_reset_method(event){
	var $reset_method_ref = $("[name='reset_method_dd']:checked");
	var reset_method_enum = {'Email':1, 'Sms':2};
	if(reset_method_enum.Email == $reset_method_ref.val()){
//		Redirect to email approval view
		window.location = '/reset_password/email_approval';
	}
	else{
//		Redirect to sms approval view
		window.location = '/reset_password/sms_approval';
	}
	
}