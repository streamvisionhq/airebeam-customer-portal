from __future__ import unicode_literals

from django.apps import AppConfig


class ResetPasswordConfig(AppConfig):
    name = 'reset_password'
