$( document ).ready(function() {
	$('.hide_old_pass').html('');
	$('.hide_account_pass').html('');
	$('.square-input').iCheck({
		checkboxClass: 'icheckbox_polaris',
		radioClass: 'iradio_polaris',
        increaseArea: '20%' // optional
	});
	
	$('.square-input').on('ifChecked', function(event){
		  $('#chk_btn').focus();
	});
	
	$('.square-input').on('ifUnchecked', function(event){
		  $('#chk_btn').focus();
	});

	$( "#id_address_text" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     submit_form();
		}
	});
	
	$( "#id_phone_number_text" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     submit_form();
		}
	});
	
	
	$("#id_last_four_cc_digit_text").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
        	
        	if( e.which == 13 ) {
	   		     event.preventDefault();
	   		     submit_form();
	   		}
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
        
    });
		
	$( "#chk_btn" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     submit_form();
		}
	});
	
	
	
});

//Write this because we want to disable submit button
//Submit button makes problem if we hit more than one
function submit_form(){
	var $fp_form = $('#fp_form');
	var $fp_submit_btn = $('#fp_submit_btn');
	$fp_submit_btn.prop('disabled', true);
	$fp_form.submit();
	
}

function return_to_login(){
	window.location = '/'
}
