from django.shortcuts import render, redirect
from forgot_password.forms import FpForm
from airebeam_customer_portal.enum import user_access_level, ForgotPasswordStatus, AppStatus,\
    AjaxCallStatus, PasswordResetMethod, SHALLOWORRESET
from airebeam_customer_portal.utils import savesessiondata, checksession
from mbr_online_check.models import mbrDetail
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
import datetime
from airebeam_customer_portal.AppServices.ForgotPasswordService import ForgotPasswordService
import base64
from airebeam_customer_portal.static_values import AppMsgs, MbrDetailKeys,\
    CustomerInfo
from airebeam_customer_portal.ServicePayload import ServicePayload
from users.views.UserView import UserView
from airebeam_customer_portal.AppServices.CustomerInfoService import CustomerInfoService
from airebeam_customer_portal.AppServices.LoginService import LoginService

class FpFormView(UserView):
    '''
    Forgot password view for customers.
    '''
    form_class = FpForm
    form_template_name = 'forgot_password/fp_form.html'
    
    def __init__(self):
        self.error = ''
        self.mbr = ''
        self.mbr_checkbox = ''
        self.address_text = ''
        self.phone_number_text = ''
        self.last_four_cc_digit_text = ''
        self.customer_info_obj = ''
        
    def get(self, request):
        '''
        Shows forgot password form.
        @param request:
        '''
        try:
            if checksession(AppMsgs.MBRKEY,request) or \
            self.get_agent_username() is not None:
                return redirect('dashboard:index')
            self.error = ''
            form = self.form_class()
            return render(request, self.form_template_name, {
                    AppMsgs.FORMKEY: form, AppMsgs.ERROR: self.error
                })
        except:
            return render(request, 'abcp_exception.html')
        
    def post(self, request):
        '''
        Processes form and redirect to view where customer can request for reset password
        through an email, sms or customer can select either by email or sms.
        @param request:
        '''
        try:
            self.error = ''
            form = self.form_class(request.POST)
            if form.is_valid():
                self.__set_form_fields(form)
                reset_or_dashboard = self.__login_customer_into_portal()
                if(reset_or_dashboard == SHALLOWORRESET.DASHBOARD):
                    return redirect('dashboard:index')
                elif (reset_or_dashboard == SHALLOWORRESET.RESET):
                    redirect_method = self.redirect_to_reset_approval()
                    if redirect_method == PasswordResetMethod.BOTH_EMAIL_CELLPHONE:
#         Redirect to reset method where we offer both email and cellphone
                        return redirect('reset_password:reset_method')
                        pass
                    elif redirect_method == PasswordResetMethod.EMAIL:
#         Redirect to reset method where we offer email
                        return redirect('reset_password:email_approval')
                        pass
                    elif redirect_method == PasswordResetMethod.CELLPHONE:
#         Redirect to reset method where we offer cellphone
                        return redirect('reset_password:sms_approval')
                        pass

            return render(request, self.form_template_name, {
                    AppMsgs.FORMKEY: form, AppMsgs.ERROR: self.error
                })
            
        except:
            return render(request, 'abcp_exception.html')
        
    def redirect_to_reset_approval(self):
        '''
        Method returns reset password by email if it has email, returns reset by sms if it 
        has valid cell phone number and returns both email and sms if it has both.
        '''
        if self.customer_info_obj[CustomerInfo.EMAIL] != '0' and self.customer_info_obj[CustomerInfo.PHONEHOME] != '' \
        and self.customer_info_obj[CustomerInfo.IS_PHONEHOME_LANDLINE] == '0':
            return PasswordResetMethod.BOTH_EMAIL_CELLPHONE
        
        elif self.customer_info_obj[CustomerInfo.EMAIL] != '0':
            return PasswordResetMethod.EMAIL
        
        elif self.customer_info_obj[CustomerInfo.PHONEHOME] != '' \
        and self.customer_info_obj[CustomerInfo.IS_PHONEHOME_LANDLINE] == '0':
            return PasswordResetMethod.CELLPHONE

    def __set_form_fields(self, form):
        '''
        Method sets form attributes which is given by customer
        @param form:
        '''
        self.mbr_checkbox = form.cleaned_data[AppMsgs.MBR_CHECKBOX]
        self.address_text = form.cleaned_data[AppMsgs.ADDRESS_TEXT]
        self.address_text = base64.b64encode(self.address_text)
        self.phone_number_text = form.cleaned_data[AppMsgs.PHONE_NUMBER_TEXT]
        self.last_four_cc_digit_text = form.cleaned_data[AppMsgs.LAST_FOUR_CC_DIGIT_TEXT]
        
    def __login_customer_into_portal(self):
        '''
        Method logins customer if they do not have email and cell phone number and
        they provide valid address or home phone number and valid credit card 
        last four digits.
        '''
        payload = ServicePayload.get_forgot_password_service_payload(self.address_text, 
                                                                             self.phone_number_text, 
                                                                             self.last_four_cc_digit_text)
        fp_service_object = ForgotPasswordService()
        fp_response = fp_service_object.call_service(payload)
        response_msg = fp_response[AppMsgs.MSG].lower()
        response_status = int(fp_response[AppMsgs.RESP][AppMsgs.STATUS_KEY])
        if response_msg == AppStatus.SUCCESS and\
        response_status == ForgotPasswordStatus.SUCCESS:
#         Call customer info service and check customer has email/cellphone
            response_obj = fp_response[AppMsgs.RESP]
            self.mbr = fp_response[AppMsgs.RESP][MbrDetailKeys.MBR_KEY]
            
            ci_object = CustomerInfoService()
            ci_payload = ServicePayload.get_customer_info_service_payload(self.mbr)
            ci_response = ci_object.call_service(ci_payload)
            customer_info = ci_response[AppMsgs.RESP][CustomerInfo.CUSTOMER]
            self.set_customer_info(customer_info)
            if customer_info[CustomerInfo.EMAIL] != '0' or (customer_info[CustomerInfo.PHONEHOME] != '' \
            and customer_info[CustomerInfo.IS_PHONEHOME_LANDLINE] == '0'):
#                 Redirect to page where we offer resetting password from both email and cellphone
                self.customer_info_obj = customer_info
                return SHALLOWORRESET.RESET
            else:
                self.set_force_reset_pass(customer_info[CustomerInfo.IS_PASSWORD_RESET])
                user_acl = user_access_level[AppMsgs.SHALLOW_KEY]
                LoginService.set_customer_data(self, response_obj, user_acl, self.mbr)
                mbr = LoginService.fetch_mbr_details(self, self.mbr)
                self.request.session[AppMsgs.USERNAME_KEY] = mbr.detail[MbrDetailKeys.FIRSTNAME_KEY]
                LoginService.search_customer_on_fd_by_phone(self, self.mbr)
                return SHALLOWORRESET.DASHBOARD

        elif response_msg == AppStatus.SUCCESS and\
            response_status == ForgotPasswordStatus.MISSING_REQUIRED_PARAMS:
            self.error = fp_response[AppMsgs.RESP][AppMsgs.MESSAGE]
        elif response_msg == AppStatus.SUCCESS and\
            response_status == ForgotPasswordStatus.MBR_NOT_EXIST:
            self.error = AppMsgs.ACCOUNT_NOT_FOUND
        elif response_msg == AppStatus.SUCCESS and\
            response_status == ForgotPasswordStatus.PHONE_ADDRESS_MISSING:
            self.error = AppMsgs.PHONE_AND_ADDRESS_MISSING
        elif response_msg == AppStatus.SUCCESS and\
            response_status == ForgotPasswordStatus.CREDIT_CARD_MISSING:
            self.error = fp_response[AppMsgs.RESP][AppMsgs.MESSAGE]
        else:
            pass
        
        return SHALLOWORRESET.ERROR
