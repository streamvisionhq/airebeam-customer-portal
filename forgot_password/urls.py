from django.conf.urls import url

from forgot_password.views.FpFormView import FpFormView

app_name = 'forgot_password'
urlpatterns = [
	url(r'$', FpFormView.as_view(), name = 'index'),
]