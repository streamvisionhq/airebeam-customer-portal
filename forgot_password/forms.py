from django import forms
 
class FpForm(forms.Form):
    mbr_checkbox = forms.BooleanField(label='I do not remember my account number', required=True, 
                                      widget=forms.CheckboxInput(attrs={'class':'square-input'}))
    address_text = forms.CharField(max_length=100, label='Address 1', required=False, 
                          widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Address 1'}))
    phone_number_text = forms.CharField(max_length=100, label='Home Phone Number', required=False, 
                          widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Home Phone Number'}))
    last_four_cc_digit_text = forms.CharField(max_length=100, label='Last 4 Credit Card Digits', required=True, 
                          widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Last 4 Credit Card Digits'}))
