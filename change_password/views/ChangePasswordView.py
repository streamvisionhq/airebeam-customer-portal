from django.shortcuts import render
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.utils import checksession, getfromSession,\
    check_if_session_time_exceeds
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from tickets.enum import TicketStatus, TicketPriority, group_id_mappings,\
    TicketType
from airebeam_customer_portal import config
from airebeam_customer_portal.ServicePayload import ServicePayload
import json
from airebeam_customer_portal.AppServices.TicketService import TicketService
from airebeam_customer_portal.AppServices.ChangePasswordService import ChangePasswordService
from airebeam_customer_portal.enum import ChangePasswordStatus
from django.http import HttpResponse
import base64
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast

class ChangePasswordView(UserView):
    form_template_name = 'change_password/change_password_form.html'
    def __init__(self):
        self.error = ''
        
        
    def get(self, request):
        '''
        Shows change password form.
        @param request:
        '''
        try:
            if checksession(AppMsgs.MBRKEY, request):
                login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
                is_session_exceded = check_if_session_time_exceeds(login_time)
        #         Logout and redirect to login page
                if is_session_exceded:
                    return HttpResponseRedirect(reverse('users:logout'))
                 
                context = {}
                return render(request, self.form_template_name, context)
        except:
            return render(request, 'abcp_exception.html')
        
    def post(self, request):
        '''
        Processes form, matches old password and changes customer password.
        @param request:
        '''
        try:
            post_values = request.POST
            mbr = getfromSession(AppMsgs.MBRKEY,request)
            old_password = post_values[AppMsgs.OLD_PASSWORD_KEY]
            new_password = post_values[AppMsgs.NEW_PASSWORD_KEY]
            old_password = base64.b64encode(old_password)
            new_password = base64.b64encode(new_password)
#           Get password payload json
            payload = ServicePayload.get_change_password_payload(mbr, old_password, new_password)
            change_pass_obj = ChangePasswordService()
            change_pass_response = change_pass_obj.call_service(payload)
            change_pass_status = int(change_pass_response[AppMsgs.RESP][AppMsgs.STATUS_KEY])
            if  change_pass_status == ChangePasswordStatus.SUCCESS:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.CHANGE_PASSWORD_STATUS_KEY:1}
#               Convert json string to object
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            elif  change_pass_status == ChangePasswordStatus.INCORRECT_PASSWORD:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.CHANGE_PASSWORD_STATUS_KEY:ChangePasswordStatus.INCORRECT_PASSWORD}
#               Convert json string to object
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            else:
                response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.CHANGE_PASSWORD_STATUS_KEY:0}
#               Convert json string to object
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
        
        except Exception as e:
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.CHANGE_PASSWORD_STATUS_KEY:0}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)