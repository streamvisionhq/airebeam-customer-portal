$( document ).ready(function() {
	$('#change_password_form').bootstrapValidator({
	    message: 'This value is not valid',
	    
	            fields: {
	            	current_password: {
	                    validators: {
	                        notEmpty: {
	                            message: 'Current password is required'
	                        }
	                    }
	                },
	                new_password: {
	                    validators: {
	                        notEmpty: {
	                            message: 'New password is required'
	                        }
	                    }
	                },
	                new_password2: {
	                    validators: {
	                    	notEmpty: {
	                            message: 'Confirm new password is required'
	                        }
	                    }
	                }
	            }
	        });
	
	$( "#id_current_password" ).keypress(press_submit);
	$( "#id_new_password" ).keypress(press_submit);
	$( "#id_new_password2" ).keypress(press_submit);
});

function press_submit( event ) {
	if( event.which == 13 ) {
	     event.preventDefault();
	     submit_form();
	}
}

function validate_fields(){
	var $new_password_ref = $("input[name='new_password']");
	var $new_password2_ref = $("input[name='new_password2']");
	var new_password_val = $new_password_ref.val().trim();
	var new_password2_val = $new_password2_ref.val().trim();
	
	if(new_password_val == "" || new_password2_val == ""){
		$('.error').show();
		return false;
	}
	
	else if(new_password_val != new_password2_val){
//		show alert
		$('.error').show();
		return false;
	}
	
	return true;
	
}

function submit_form(){
	var $cp_form = $('#change_password_form');
	var is_valid = validate_fields();
	if(is_valid){
		//$cp_form.submit();
        $('.error').hide();
		change_password();
	}
}

function show_logout_timer(){
	var $speedtest_btn_ref = null;
	var $container_speedtest_btn = null;
	var ten_seconds = 10,
    display = $('#logout_timer_container');
	display.show();
	var timer_name = 'logout';
	startTimer(ten_seconds, display, $container_speedtest_btn, $speedtest_btn_ref, timer_name);
}

function change_password(){
	var $current_password_ref = $('#id_current_password');
	var $new_password_ref = $('#id_new_password');
	var old_password = $current_password_ref.val().trim();
	var new_password = $new_password_ref.val().trim();
	$.ajax({
    	cache: false,
        type : "post",
        url: '/change_password/index',
        beforeSend: function(xhr, settings) {
            function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
    	},
    	data:{old_password:old_password, new_password:new_password},
        success: function(response){
        	var $error_div_ref = $('#error_div');
        	if(response.change_password_status == 1){
//        		Show logout message
        		var $pass_reset_msg_ref = $('#id_change_password_msg');
        		$error_div_ref.hide();
        		$pass_reset_msg_ref.show();
        		show_logout_timer();
        		
        	}
        	else if(response.change_password_status == 17){
//        		Show Incorrect password message
        		$error_div_ref.removeClass('no-display');
        		$error_div_ref.html('Incorrect old password');
        	}
        	else{
//        		Show service failure message
        		$error_div_ref.removeClass('no-display');
        		$error_div_ref.html('Change password failed. Please try later');
        	}
        }
    });
}