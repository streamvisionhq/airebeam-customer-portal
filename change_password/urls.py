from django.conf.urls import url

from change_password.views.ChangePasswordView import ChangePasswordView

app_name = 'change_password'
urlpatterns = [
    url(r'index$', ChangePasswordView.as_view(), name = 'index'),
]