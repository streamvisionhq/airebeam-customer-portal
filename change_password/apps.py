from __future__ import unicode_literals

from django.apps import AppConfig


class ChangePasswordConfig(AppConfig):
    name = 'change_password'
