from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Categories(models.Model):
	name = models.CharField(max_length=100)
	parent_id = models.IntegerField()
	date = models.DateTimeField(auto_now_add=True)

	@classmethod
	def get_parent_categories(self):
		return Categories.objects.filter(parent_id=0)

	@classmethod
	def get_sub_categories(self, cat_id):
		return Categories.objects.filter(parent_id=cat_id)

	class Meta:
		db_table = "categories"

class Forms(models.Model):
	name = models.CharField(max_length=100)
	fields = models.CharField(max_length=64400)
	cat_id = models.ForeignKey(Categories, on_delete=models.CASCADE)
	fd_group = models.IntegerField(default=0)
	fd_type = models.CharField(max_length=100, default='')
	fd_priority = models.IntegerField(default=2)
	
	@classmethod
	def get_form(self, form_id):
		return Forms.objects.filter(cat_id_id=form_id)
	
	class Meta:
		db_table = "forms"
		
class Additional_Subcat_Fields(models.Model):
	fields = models.CharField(max_length=64400)
	sub_cat_id = models.ForeignKey(Categories, on_delete=models.CASCADE)
	
	@classmethod
	def get_by_subcat_id(self, sub_cat_id):
		return Additional_Subcat_Fields.objects.filter(sub_cat_id=sub_cat_id)
	
	class Meta:
		db_table = "additional_subcat_fields"
		
class Services(models.Model):
	name = models.CharField(max_length=1024)
	group_id = models.IntegerField()
	date = models.DateTimeField(auto_now_add=True)
	cost = models.CharField(default='', max_length=9)
	tax = models.CharField(default='', max_length=50)
	description = models.CharField(default='', max_length=1024)
	setup_charge = models.CharField(default='', max_length=50)
	
	@classmethod
	def get_groups(self):
		return Services.objects.filter(group_id=0)
	
	@classmethod
	def get_services(self, grp_id):
		return Services.objects.filter(group_id=grp_id)
	
	@classmethod
	def get_service_by_id(self, id):
		return Services.objects.get(pk=id)
	
	
	class Meta:
		db_table = "services"
	