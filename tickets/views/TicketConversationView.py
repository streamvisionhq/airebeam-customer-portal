from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import checksession
from airebeam_customer_portal.static_values import AppMsgs
from users.views.UserView import UserView
from airebeam_customer_portal.AppServices.ConversationService import ConversationService
from airebeam_customer_portal.enum import AppStatus

class TicketConversationView(UserView):
    '''
    Fetches all ticket's comments and notes.
    '''
    template_name = 'tickets/view_ticket_conversations.html'
    
    def __init__(self):
        UserView.__init__(self)
        self.error = AppMsgs.NOT_FETCH_TICKET_CONVERSATIONS
        self.ticket_id = ''
        self.conversations = []
    
    def get(self, request, ticket_id):
        if checksession(AppMsgs.MBRKEY, request):
            self.ticket_id = str(ticket_id)
            self.__fetch_ticket_conversations(request)
            
            context = {AppMsgs.CONVERSATIONS_KEY: self.conversations, AppMsgs.ERROR:self.error}
            return render(request, self.template_name, context)
                
        else:
            return HttpResponseRedirect(reverse('users:login'))

    def __fetch_ticket_conversations(self, request):
        '''
        Fetches ticket's all conversations.
        @param request:
        '''
        conversation_service_object = ConversationService(request)
        cs_response = conversation_service_object.get_conversations_by_ticket_id(self.ticket_id)
        if cs_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
            self.conversations = cs_response[AppMsgs.RESP]
            self.error = ''
        else:
            self.conversations = []
    