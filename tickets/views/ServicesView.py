from django.shortcuts import render
from airebeam_customer_portal.static_values import AppMsgs
from users.views.UserView import UserView
from tickets.enum import ServicesGroup
from tickets.models import Services

class ServicesView(UserView):
    '''
    Fetches services from db and populate drop down with these on Account sevice's form.
    '''
    template_name = 'tickets/services.html'
    def get(self, request, group_id):
#         Group id must be in db
        if int(group_id) > ServicesGroup.NO_GROUP and int(group_id) <= ServicesGroup.EQUIPMENT_RENTAL:
            services = Services.get_services(group_id)
            context = {AppMsgs.SERVICES_KEY:services}
            return render(request, self.template_name, context)
        else:
            context = {}
            return render(request, self.template_name, context)
