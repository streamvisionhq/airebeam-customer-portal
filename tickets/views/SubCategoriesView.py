from django.shortcuts import render
from airebeam_customer_portal.static_values import AppMsgs
from users.views.UserView import UserView
from tickets.models import Categories

# This view fetches and return sub categories from db
class SubCategoriesView(UserView):
    '''
    Fetches and return sub categories from db on get support form.
    '''
    template_name = 'tickets/sub_categories.html'
    def get(self, request, sub_cat_id):
        if int(sub_cat_id) > 0 and int(sub_cat_id) <= 4:
            sub_categories = Categories.get_sub_categories(sub_cat_id)
            context = {AppMsgs.SUBCATEGORIES_KEY:sub_categories}
            return render(request, self.template_name, context)
        else:
            context = {}
            return render(request, self.template_name, context)
