from django.http import HttpResponse
import json
from airebeam_customer_portal.AppServices.ConversationService import ConversationService
from airebeam_customer_portal.utils import checksession, getfromSession
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from radiostats.models import Radiostat
from airebeam_customer_portal.enum import AppStatus
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.ServicePayload import ServicePayload
from users.views.UserView import UserView
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast

class TicketReplyView(UserView):
    '''
    Create reply on ticket.
    '''
    def post(self, request, ticket_id):
        if checksession(AppMsgs.MBRKEY, request) and \
            checksession(AppMsgs.FD_CUSTOMER_ID_KEY, request):
            login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
            is_session_exceded = check_if_session_time_exceeds(login_time)
    #         Logout and redirect to login page
            if is_session_exceded:
                response_dict = AppResponse.get_logged_status_response()
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            fd_customer_id = getfromSession(AppMsgs.FD_CUSTOMER_ID_KEY,request)
            is_support_ticket = request.POST[AppMsgs.IS_SUPPORT_KEY]
            reply_body = request.POST[AppMsgs.BODY_KEY]
            payload = ConversationService.prepare_ticket_reply_msg(self, fd_customer_id, is_support_ticket, reply_body)
            response_dict = self.__create_reply(ticket_id, payload)
            return HttpResponse(response_dict, content_type=AppMsgs.CONTENT_TYPE_VALUE)
    
        else:
            response_dict = AppResponse.get_logged_status_response()
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)

    def __prepare_reply_msg(self, fd_customer_id, is_support_ticket, reply_body):
        '''
        Prepare reply msg which is sent on ticket.
        @param fd_customer_id:
        @param is_support_ticket:
        @param reply_body:
        '''
        payload = ServicePayload.get_create_reply_payload(reply_body, fd_customer_id)
#         Should replace constant with ENUM
############# Block: Attach radio stats if it is support ticket ############
        if is_support_ticket != '' and int(is_support_ticket) == 1:
            radio_stat_obj = Radiostat()
            stats_summary = radio_stat_obj.get_stats_str(self.request)
            reply_body = reply_body + stats_summary
            payload = ServicePayload.get_create_reply_payload(reply_body, fd_customer_id)
########################### End Block #####################################            
        payload = json.dumps(payload)
        return payload
    
    def __create_reply(self, ticket_id, payload):
        '''
         Call freshdesk api to create reply on ticket
        @param ticket_id:
        @param payload:
        '''
        conversation_service_object = ConversationService(self.request)
        cs_response = conversation_service_object.create_reply(ticket_id, payload)
        if cs_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
            response_dict = json.dumps(cs_response)
        else:
            response_dict = json.dumps(cs_response)
        
        return response_dict
            



