from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.AppServices.TicketService import TicketService
from airebeam_customer_portal.utils import checksession, getfromSession, strip_radio_stats
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.enum import AppStatus, PasswordForceReset
from airebeam_customer_portal.static_values import AppMsgs, UpdatedTicketPayload
from users.views.UserView import UserView
from tickets.enum import ticket_string_map
from airebeam_customer_portal import utils

class TicketDetailView(UserView):
    '''
    Shows ticket's detail.
    '''
    template_name = 'tickets/view_ticket.html'
    
    def __init__(self):
        UserView.__init__(self)
        self.error = AppMsgs.NOT_FETCH_TICKET_DETAIL
        self.ticket_detail = {}
        self.is_support = 0    
    
    def get(self, request, ticket_id):
        if checksession(AppMsgs.MBRKEY, request):
            login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
            is_session_exceded = check_if_session_time_exceeds(login_time)
            force_reset = utils.safe_cast(self.get_force_reset_pass(), int)
    #         Logout and redirect to login page
            if is_session_exceded:
                return HttpResponseRedirect(reverse('users:logout'))

#           Redirect to dashboard if force reset password is set.            
            if force_reset == PasswordForceReset.FORCE_RESET or self.get_terms_url() is not None:
                return redirect('dashboard:index')
            
            try:
                self.__fetch_ticket_details(request, ticket_id)
            except:
                pass
                
            context = {AppMsgs.TICKET_DETAIL_KEY:self.ticket_detail, AppMsgs.ERROR:self.error, AppMsgs.IS_SUPPORT_KEY: self.is_support}             
            return render(request, self.template_name, context)
        else:
            return HttpResponseRedirect(reverse('users:login'))

    def __fetch_ticket_details(self, request, ticket_id):
        '''
        Fetches ticket details and modify some ticket info for the front end.
        @param request:
        @param ticket_id:
        '''
        ticket_service_object = TicketService(request)
        ts_response = ticket_service_object.get_ticket(ticket_id)
        if ts_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
            self.ticket_detail = ts_response[AppMsgs.RESP]
            self.is_support = self.ticket_detail[UpdatedTicketPayload.CUSTOM_FIELDS_KEY][AppMsgs.IS_SUPPORT_KEY]
            ticket_description = self.ticket_detail[AppMsgs.DESCRIPTION]
            ticket_subject = self.ticket_detail[AppMsgs.SUBJECT_KEY]
            ticket_number = '[Ticket#{0}]'.format(ticket_id)
            ticket_title = ticket_number + ' ' + ticket_subject
            ticket_status = self.ticket_detail[AppMsgs.STATUS_KEY]
            self.ticket_detail[AppMsgs.STATUS_STR_KEY] = ticket_string_map[int(ticket_status)]
            self.ticket_detail[AppMsgs.SUBJECT_KEY] = ticket_title
            self.ticket_detail[AppMsgs.DESCRIPTION] = strip_radio_stats(ticket_description)
            self.error = ''


