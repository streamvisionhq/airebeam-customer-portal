from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.AppServices.TicketService import TicketService
from airebeam_customer_portal.utils import checksession, getfromSession
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.enum import AppStatus
from airebeam_customer_portal.static_values import AppMsgs
from users.views.UserView import UserView

class CustomerAllTicketsView(UserView):
    '''
    Fetches all tickets and shows on dashboard.
    '''
    template_name = 'tickets/index.html'
    
    def __init__(self):
        UserView.__init__(self)
        self.error = AppMsgs.NOT_FETCH_TICKETS
        self.tickets_list = []
    
    def get(self, request):
        if checksession(AppMsgs.MBRKEY, request):
            login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
            is_session_exceded = check_if_session_time_exceeds(login_time)
    #         Logout and redirect to login page
            if is_session_exceded:
                return HttpResponseRedirect(reverse('users:logout'))

            try:
                self.__fetch_all_tickets_list(request)
            except:
                pass
                    
            context = {AppMsgs.TICKETS_DICT_KEY:self.tickets_list, AppMsgs.ERROR:self.error}             
            return render(request, self.template_name, context)
        else:
            return HttpResponseRedirect(reverse('users:login'))    

    def __fetch_all_tickets_list(self, request):
        '''
        Get all ticket's list of customer
        @param request:
        '''
        if checksession(AppMsgs.FD_CUSTOMER_ID_KEY, request):
            fd_customer_id = getfromSession(AppMsgs.FD_CUSTOMER_ID_KEY, request)
            ticket_service_object = TicketService(request)
            ts_response = ticket_service_object.get_all_tickets_list(fd_customer_id)
            if ts_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
                tickets_lst = ts_response[AppMsgs.RESP]
#                 Shows error if ticket's list is empty
                if len(tickets_lst) <= 0:
                    self.error = AppMsgs.DO_NOT_HAVE_OPEN_TICKETS
                else:
                    self.error = ''
                
                self.tickets_list = tickets_lst
        else:
            self.error = AppMsgs.DO_NOT_HAVE_OPEN_TICKETS
            self.tickets_list = []

