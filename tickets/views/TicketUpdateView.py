from django.http import HttpResponse
import json
from airebeam_customer_portal.AppServices.TicketService import TicketService
from airebeam_customer_portal.utils import checksession, getfromSession
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.enum import AppStatus
from airebeam_customer_portal.static_values import AppMsgs
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.ServicePayload import ServicePayload
from users.views.UserView import UserView
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast


class TicketUpdateView(UserView):
    '''
    Update ticket's attributes on fresh desk after customer opens it.
    '''
    def get(self, request, ticket_id):
        if checksession(AppMsgs.MBRKEY, request) and checksession(AppMsgs.FD_CUSTOMER_ID_KEY, request):
            login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
            is_session_exceded = check_if_session_time_exceeds(login_time)
    #         Logout and redirect to login page
            if is_session_exceded:
                response_dict = AppResponse.get_logged_status_response()
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            ticket_service_object = TicketService(request)
#             Should replace constant with ENUM
            payload = ServicePayload.get_updated_ticket_payload("1")
            payload = json.dumps(payload)
            ts_response = ticket_service_object.update_ticket_by_id(payload, ticket_id)
            if ts_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
                response_dict = json.dumps(ts_response)
                return HttpResponse(response_dict, content_type=AppMsgs.CONTENT_TYPE_VALUE)
            else:
                response_dict = json.dumps(ts_response)
                return HttpResponse(response_dict, content_type=AppMsgs.CONTENT_TYPE_VALUE)
            
    
        else:
            response_dict = AppResponse.get_logged_status_response()
            response_dict = json.dumps(response_dict)
            return HttpResponse(response_dict, content_type=AppMsgs.CONTENT_TYPE_VALUE)


