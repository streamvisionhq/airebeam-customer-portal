from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from tickets.forms import DynamicForm
from tickets.enum import TicketStatus, ParentCategories
import json
from django.core.urlresolvers import reverse
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
from airebeam_customer_portal.AppServices.TicketService import TicketService
from airebeam_customer_portal.utils import checksession, getfromSession, savesessiondata
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from radiostats.models import Radiostat
import tickets.enum as enum
from airebeam_customer_portal.enum import user_access_level, AppStatus
from airebeam_customer_portal.static_values import AppMsgs, CustomFields,\
    MbrDetailKeys, ExceptionMsgs
from airebeam_customer_portal.AppResponse import AppResponse
from airebeam_customer_portal.ServicePayload import ServicePayload
from users.views.UserView import UserView
from airebeam_customer_portal.SimpleNamespace import SimpleNamespace
import ast

class TicketCreateFormView(UserView):
    '''
    Shows and create get support form.
    '''
    def get(self, request, sub_cat_id):
        if checksession(AppMsgs.MBRKEY, request):
            login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
            is_session_exceded = check_if_session_time_exceeds(login_time)
    #         Logout and redirect to login page
            if is_session_exceded:
                response_dict = AppResponse.get_logged_status_response()
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            
################ Block: Retrieve customer details from session to populate form ################ 
            mbr = getfromSession(AppMsgs.MBRKEY,request)
            mbr = str(mbr)
            mbr_detail = getfromSession(AppMsgs.MBR_DETAIL_KEY,request)
            form = DynamicForm(request, sub_cat_id, mbr_detail)
            name = mbr_detail[MbrDetailKeys.FIRSTNAME_KEY] + ' ' + mbr_detail[MbrDetailKeys.LASTNAME_KEY]
            address = mbr_detail[MbrDetailKeys.ADDRESS1_KEY]
            email_address = mbr_detail[MbrDetailKeys.EMAIL_KEY]
            phone_number = mbr_detail[MbrDetailKeys.PHONEHOME_KEY]
            eft_account_last_four = mbr_detail[MbrDetailKeys.EFTACCOUNTLASTFOUR]
            eft_routing_number = mbr_detail[MbrDetailKeys.EFTROUTINGNUMBER]
            eft_auth_name = mbr_detail[MbrDetailKeys.EFTAUTHNAME]
            last_four_digits = mbr_detail[MbrDetailKeys.LAST_FOUR]
            expiration_date = mbr_detail[MbrDetailKeys.EXPIRATION_DATE]
            user_acl_enum = user_access_level[AppMsgs.COMPLETE_KEY]
            user_access = self.get_access_level()
            missing_card_fields = False
            missing_check_fields = False
            
            if last_four_digits is None or expiration_date is None:
                missing_card_fields = True
                
            if eft_account_last_four is None or eft_routing_number is None or eft_auth_name is None:
                missing_check_fields = True
                
#             Populate customer data on form if user has complete access to portal
            if user_access == user_acl_enum:
                form.set_customer_id(mbr)
                
            if user_access == user_acl_enum and name is not None:
                form.set_account_holder_name(name)
            
            if user_access == user_acl_enum and address is not None:
                form.set_address(address)
                
            if user_access == user_acl_enum and email_address is not None:
                form.set_email_address(email_address)
                
            if user_access == user_acl_enum and phone_number is not None:
                form.set_phone_number(phone_number)
                
            if eft_account_last_four is not None:
                form.set_eft_account_last_four(eft_account_last_four)
            
            if eft_routing_number is not None:
                form.set_eft_routing_number(eft_routing_number)
                
            if eft_auth_name is not None:
                form.set_eft_auth_name(eft_auth_name)
                
            if last_four_digits is not None:
                form.set_last_four(last_four_digits)
                
            if expiration_date is not None:
                form.set_expiration_date(expiration_date)
                
####################### End Block ################
                
            return render(request, 'tickets/create_form.html', {AppMsgs.FORMKEY: form, 
                                                            AppMsgs.SUBCATEGORY_ID_KEY:sub_cat_id, 
                                                            AppMsgs.CARD_FIELDS_KEY: enum.card_field_names,
                                                            AppMsgs.CHECK_FIELDS_KEY: enum.check_fields_names,
                                                            'missing_check_fields':missing_check_fields,
                                                            'missing_card_fields':missing_card_fields})
            
        else:
            response_dict = AppResponse.get_logged_status_response()
            response_dict = json.dumps(response_dict)
            return HttpResponse(response_dict, content_type=AppMsgs.CONTENT_TYPE_VALUE)
    
    def post(self, request, sub_cat_id):
        try:
            payload = {
                'emerald_user': 'asimapi',
                'emerald_password': 'qw8jq08qw8jq08',
                'action': 'payment_cc',
                'CustomerID': request.POST['customer_id'],
                'AuthName': request.POST['account_hold_name'],
                'CardExpire': request.POST['card_expiry'],
                'CardNumber': request.POST['card_number'],
                'Amount': request.POST['payment_amount'],
            }

            customer_service_object = CustomerService()
            response = customer_service_object.one_time_credit_card_charge(payload)

            if response['RESPONSE']['RETCODE'] == '0':
                response_dict = {AppMsgs.STATUS_KEY: AppMsgs.RESP_STATUS_SUCCESS, AppMsgs.MSG:'Payment request logged successfully'}
                # Convert json string to object
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)
            else:
                response_dict = {AppMsgs.STATUS_KEY: AppMsgs.RESP_STATUS_FAILURE, AppMsgs.MSG:response['RESPONSE']['MESSAGE']}
                # Convert json string to object
                response_dict = SimpleNamespace(**response_dict)
                return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)

        except Exception as e:
            response_dict = {AppMsgs.MSG:AppMsgs.RESP_STATUS_FAILURE, AppMsgs.MSG:ExceptionMsgs.GENERAL_EXCEPTION}
            response_dict = SimpleNamespace(**response_dict)
            return HttpResponse(json.dumps(ast.literal_eval(str(response_dict))), content_type=AppMsgs.CONTENT_TYPE_VALUE)