from django.shortcuts import render, redirect
from users.views.UserView import UserView
from airebeam_customer_portal.static_values import AppMsgs, CustomFields,\
    MbrDetailKeys, CustomerInfo
from tickets.enum import TicketStatus, TicketPriority, group_id_mappings,\
    TicketType
from airebeam_customer_portal.ServicePayload import ServicePayload
import json
from airebeam_customer_portal.AppServices.TicketService import TicketService
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import checksession, getfromSession, savesessiondata
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
from airebeam_customer_portal import config
from airebeam_customer_portal.enum import AppStatus
from tickets.models import Forms
from airebeam_customer_portal.enum import user_access_level

class GenerateUpdatePaymentTicket(UserView):
    
    def __init__(self):
        self.error = ''
        self.request = ''
        
    def __prepare_ticket_body(self, value_list, payment_by_echeck, mbr_detail):
        account_number = mbr_detail[MbrDetailKeys.MBR_KEY]
        account_holder_name = mbr_detail[MbrDetailKeys.LASTNAME_KEY] + ' ' + mbr_detail[MbrDetailKeys.FIRSTNAME_KEY]
        contact_number = mbr_detail[MbrDetailKeys.PHONEHOME_KEY]
        contact_email = mbr_detail[MbrDetailKeys.EMAIL_KEY]
        eft_account_last_four = mbr_detail[MbrDetailKeys.EFTACCOUNTLASTFOUR]
        eft_routing_number = mbr_detail[MbrDetailKeys.EFTROUTINGNUMBER]
        eft_auth_name = mbr_detail[MbrDetailKeys.EFTAUTHNAME]
        new_payment_method = AppMsgs.CREDIT_CARD_LABEL
        last_four_digits = mbr_detail[MbrDetailKeys.LAST_FOUR]
        expiration_date = mbr_detail[MbrDetailKeys.EXPIRATION_DATE]
        
        ticket_body = ''
        
        user_acl = user_access_level[AppMsgs.SHALLOW_KEY]
        desc_body_heading = TicketService.ticket_desc_heading(self.request, user_acl)
        ticket_body += desc_body_heading
        
        label_list = ['Account Number(MBR)', 'Account Hold Name', 'Contact Phone Number', 
                      'Contact email address', 'New Payment Method']
        
        if last_four_digits is not None:
            label_list.append('Last four digits of credit card on file')
        if expiration_date is not None:
            label_list.append('Expiration date')
        
        if payment_by_echeck is not None and payment_by_echeck == 1:
            label_list = ['Account Number(MBR)', 'Account Hold Name', 'Contact Phone Number', 
                          'Contact email address', 'New Payment Method']
            if eft_auth_name is not None:
                label_list.append('Account Holder Name')
            if eft_account_last_four is not None: 
                label_list.append('Account Number')
            if eft_routing_number is not None:
                label_list.append('Routing Code')
                
        for count, label in enumerate(label_list):
            ticket_body += '<div> {0} : {1} </div>'.format(label, value_list[count])
            
        return ticket_body
    
    def get(self, request):
        try:
            self.request = request
            mbr = getfromSession(AppMsgs.MBRKEY, request)
            payment_by_echeck = request.GET.get(AppMsgs.PAYMENT_BY_ECHECK, None)
            if not checksession(AppMsgs.FD_CUSTOMER_ID_KEY, request):
                mbr_detail = getfromSession(AppMsgs.MBR_DETAIL_KEY,request)
                name = mbr_detail[MbrDetailKeys.FIRSTNAME_KEY] + ' ' + mbr_detail[MbrDetailKeys.LASTNAME_KEY]
                email =  mbr_detail[MbrDetailKeys.EMAIL_KEY]
                address = mbr_detail[MbrDetailKeys.ADDRESS1_KEY]
                phone_home = mbr_detail[MbrDetailKeys.PHONEHOME_KEY]
                mobile = mbr_detail[MbrDetailKeys.PHONEHOME_KEY]
                payload = ServicePayload.get_create_customer_payload(name, mbr, email, address, phone_home, mobile)
                payload = json.dumps(payload)
                customer_service_object = CustomerService()
                cs_response = customer_service_object.create_customer(payload)
                if cs_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
                    customer = cs_response[AppMsgs.RESP]
                    savedata = customer[AppMsgs.ID_KEY]
                    savesessiondata(AppMsgs.FD_CUSTOMER_ID_KEY, savedata, request)
                else:
                    url = reverse('dashboard:index')
                    url += '?s=-1'
                    return HttpResponseRedirect(url)
                
            if checksession(AppMsgs.FD_CUSTOMER_ID_KEY, request):
                form_fields_row = Forms.get_form(21)
                mbr_detail = getfromSession(AppMsgs.MBR_DETAIL_KEY,request)
                account_number = mbr_detail[MbrDetailKeys.MBR_KEY]
                account_holder_name = mbr_detail[MbrDetailKeys.LASTNAME_KEY] + ' ' + mbr_detail[MbrDetailKeys.FIRSTNAME_KEY]
                contact_number = mbr_detail[MbrDetailKeys.PHONEHOME_KEY]
                contact_email = mbr_detail[MbrDetailKeys.EMAIL_KEY]
                eft_account_last_four = mbr_detail[MbrDetailKeys.EFTACCOUNTLASTFOUR]
                eft_routing_number = mbr_detail[MbrDetailKeys.EFTROUTINGNUMBER]
                eft_auth_name = mbr_detail[MbrDetailKeys.EFTAUTHNAME]
                new_payment_method = AppMsgs.CREDIT_CARD_LABEL
                last_four_digits = mbr_detail[MbrDetailKeys.LAST_FOUR]
                expiration_date = mbr_detail[MbrDetailKeys.EXPIRATION_DATE]
                TestStr = config.TestStr
                ticket_title = form_fields_row[0].fd_type
                ticket_subject = '{0}{1}, {2}, {3} - {4}'.format(TestStr, 
                                                           mbr_detail[MbrDetailKeys.MBR_KEY], 
                                                           mbr_detail[MbrDetailKeys.LASTNAME_KEY], 
                                                           mbr_detail[MbrDetailKeys.FIRSTNAME_KEY],
                                                           ticket_title)
                status = TicketStatus.OPEN
                fd_priority = form_fields_row[0].fd_priority
                fd_group = form_fields_row[0].fd_group
                fd_type = form_fields_row[0].fd_type
                fd_customer_id = getfromSession(AppMsgs.FD_CUSTOMER_ID_KEY, request)
                custom_fields_obj = CustomFields()
                custom_fields = custom_fields_obj.get_mbr_to_no_support_payload(mbr)
                
#                 Prepare update payment method ticket body
                ticket_value_list = [account_number, account_holder_name, contact_number, contact_email, 
                                     new_payment_method]
                
                if last_four_digits is not None:
                    ticket_value_list.append(last_four_digits)
                if expiration_date is not None:
                    ticket_value_list.append(expiration_date)
                 
                if payment_by_echeck is not None and int(payment_by_echeck) == 1:
                    payment_by_echeck = int(payment_by_echeck)
                    new_payment_method = AppMsgs.ECHECK_LABEL
                    ticket_value_list = [account_number, account_holder_name, contact_number, contact_email, 
                                     new_payment_method]
                    if eft_auth_name is not None:
                        ticket_value_list.append(eft_auth_name)
                    if eft_account_last_four is not None: 
                        ticket_value_list.append(eft_account_last_four)
                    if eft_routing_number is not None:
                        ticket_value_list.append(eft_routing_number)
                        
                ticket_body = TicketService.prepare_update_payment_ticket_body(request, ticket_value_list, 
                                                                               payment_by_echeck, 
                                                                               mbr_detail)
                payload = ServicePayload.get_create_ticket_payload(fd_group, fd_type, fd_priority, 
                                                                   ticket_body, ticket_subject, status, 
                                                                   fd_customer_id, custom_fields)
                  
                payload = json.dumps(payload)
                ticket_service_object = TicketService(request)
                ts_response = ticket_service_object.create_ticket(payload)
                url = reverse('dashboard:index')
                if ts_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
                    return redirect('tickets:thanks_update_payment')
                else:
                    url += '?s=0'
                    return redirect(url)
             
        except Exception as e:
            return render(request, 'abcp_exception.html')