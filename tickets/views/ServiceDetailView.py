from django.shortcuts import render
from airebeam_customer_portal.static_values import AppMsgs
from users.views.UserView import UserView
from tickets.models import Services

class ServiceDetailView(UserView):
    '''
    Fetches services details from db and shows on Account sevice's form.
    '''
    template_name = 'tickets/service_description.html'
    def get(self, request, service_id):
        service_field = Services.get_service_by_id(service_id)
        if service_field:
            context = {AppMsgs.SERVICE_FIELD_KEY: service_field}
            return render(request, self.template_name, context)
        else:
            context = {AppMsgs.SERVICE_FIELD_KEY: {}}
            return render(request, self.template_name, context)
