from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from airebeam_customer_portal.utils import checksession, getfromSession
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from airebeam_customer_portal.static_values import AppMsgs
from users.views.UserView import UserView
from tickets.models import Categories
from airebeam_customer_portal.enum import PasswordForceReset
from airebeam_customer_portal import utils

class CategoriesView(UserView):
    '''
    Fetches and return parent categories from db on get support form.
    '''
    template_name = 'tickets/categories.html'
    def get(self, request):
        if checksession(AppMsgs.MBRKEY, request):
            login_time = getfromSession(AppMsgs.LOGIN_TIME_KEY, request)
            is_session_exceded = check_if_session_time_exceeds(login_time)
            force_reset = utils.safe_cast(self.get_force_reset_pass(), int)
    #         Logout and redirect to login page
            if is_session_exceded:
                return HttpResponseRedirect(reverse('users:logout'))

#           Redirect to dashboard if force reset password is set.  
            if force_reset == PasswordForceReset.FORCE_RESET or self.get_terms_url() is not None:
                return redirect('dashboard:index')
            
            parent_categories = Categories.get_parent_categories()
            sub_categories = Categories.get_sub_categories(3)
            
            context = {AppMsgs.PARENT_CATEGORIES_KEY:parent_categories, AppMsgs.SUBCATEGORIES_KEY:sub_categories}

            success_str = request.GET.get('s', '')
            if success_str != '':
                context['success'] = int(success_str)

            return render(request, self.template_name, context)
        else:
            return HttpResponseRedirect(reverse('users:logout'))
