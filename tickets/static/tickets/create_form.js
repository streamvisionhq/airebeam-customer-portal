//	Enable Webshim
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
		  // Chrome
	  } else {
		  webshim.activeLang('en');
		  webshims.polyfill('forms');
		  webshims.cfg.no$Switch = true; // Safari
	  }
	}
	hide_payment_options();
	disable_submit_btn();
	$('.square-input').parent().addClass("abcp-align-fields-with-label");
	store_parent_category();
	
	$(".acp-number").keydown(function(event) {
	    // Allow: backspace, delete, tab, escape, and enter
	    if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
	         // Allow: Ctrl+A
	        (event.keyCode == 65 && event.ctrlKey === true) || 
	         // Allow: home, end, left, right
	        (event.keyCode >= 35 && event.keyCode <= 39)) {
	             // let it happen, don't do anything
	             return;
	    }
	    else {
	        // Ensure that it is a number and stop the keypress
	        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	            event.preventDefault(); 
	        }
	    }
	});
	
	$(".acp-number").keyup(function(event) {
		calculate_price_of_cable();
	});
	
	$('#description').summernote({
	height: 300,   //set editable area's height
	focus: true,   //set focus editable area after Initialize summernote
	toolbar: [
	          // [groupName, [list of button]]
	          ['acp-font-style', ['bold', 'underline', 'clear']],
	          ['acp-font-color', ['color']],
	          ['acp-paragraph', ['ul', 'ol', 'paragraph']],
	          ['acp-insert', ['table', 'link']],
	          ['acp-help', ['help']]
	        ]
	});
    
    $('.square-input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
              increaseArea: '20%' // optional
            });
    
    var $date_input_ref = $('.abcp-date-control');
    $date_input_ref.daterangepicker({
    	singleDatePicker: true,
    	startDate: moment(),
    	endDate: moment(),
    });
    
    $date_input_ref.val(moment().format('MM/DD/YYYY'));

    $("#description").on("summernote.change", function (e) {   // callback as jquery custom event
    	var description_text = $('#description').code();
    	$('#description_value').val(description_text);
    });    

var PAYMENT_VALUES = (function() {
    var private = {
        'Electronic_Check': 1,
        'Credit_Card': 2
    };

    return {
       get: function(name) { return private[name]; }
   };
})();

var IGNORE_PAYMENT_METHOD = (function() {
    var private = {
        'YES': 1,
        'NO': 0
    };

    return {
       get: function(name) { return private[name]; }
   };
})();

function hide_payment_options(){
	var $ignore_payment_method_ref = $("[name='ignore_payment_method']");
	if($ignore_payment_method_ref.length && $ignore_payment_method_ref.val() == IGNORE_PAYMENT_METHOD.get('YES') ){
		$('.abcp-payment-method').hide();
	}
}

function show_payment_options(){
	var $ignore_payment_method_ref = $("[name='ignore_payment_method']");
	if($ignore_payment_method_ref.length && $ignore_payment_method_ref.val() == IGNORE_PAYMENT_METHOD.get('NO') ){
		$('.abcp-payment-method').show();
	}
}

function calculate_price_of_cable(){
	var $acp_number_ref = $('.acp-number');
	var number_val = $acp_number_ref.val();
	var $equipment_total_cost_ref = $('#equipment_total_cost');
	var cost = 1.10;
	var foot_label = ' Foot';
	var currency_symbol = '$';
	if(number_val != undefined && number_val !=''){
		var total_price = parseFloat(parseFloat(number_val) * cost).toFixed(2);
		$('#length_div').html(number_val + foot_label);
		$equipment_total_cost_ref.html(currency_symbol + total_price);
	}
	else{
		$('#length_div').html(foot_label);
		$equipment_total_cost_ref.html(currency_symbol);
	}
}

function clear_check_fields(){
	$("[name='card_holder_or_account_holder']").val('');
	$("[name='account_number']").val('');
	$("[name='routing_code']").val('');
}

function clear_card_fields(){
	$("[name='last_four_digits_of_credit_card_on_file']").val('');
	$("[name='expiration_date_if_card']").val('');
}

function submit_form(){
	var is_valid = validate_fields();
	
	
	if(is_valid){
		var payment_method = $("select[name='new_payment_method']");
		var payment_method_value = payment_method.val();
		var $e_check_ref = $("[name='i_agree_to_pay_through_my_echeck_on_file']");
		var $pay_through_card_ref = $("[name='i_agree_to_pay_through_my_card_on_file']");
		var $modify_card_ref = $("[name='i_agree_to_modify_my_card_payment_details']");
		var $modify_e_check_ref = $("[name='i_agree_to_modify_my_echeck_payment_details']");
		var $ignore_payment_method_ref = $("[name='ignore_payment_method']");
		
		
		
		if(payment_method_value == PAYMENT_VALUES.get('Electronic_Check') ){
			$("[name='last_four_digits_of_credit_card_on_file']").val('');
			$("[name='expiration_date_if_card']").val('');
		}
		else if(payment_method_value == PAYMENT_VALUES.get('Credit_Card') ){
			$("[name='card_holder_or_account_holder']").val('');
			$("[name='account_number']").val('');
			$("[name='routing_code']").val('');
		}
		
		if($ignore_payment_method_ref.length && $ignore_payment_method_ref.val() == IGNORE_PAYMENT_METHOD.get('YES') ){
			payment_method.removeAttr( "required" );
			payment_method.val('');
			clear_check_fields();
			clear_card_fields();
		}
		
		
		$e_check_ref.removeAttr( "required" );
		$pay_through_card_ref.removeAttr( "required" );
		$modify_e_check_ref.removeAttr( "required" );
		$modify_card_ref.removeAttr( "required" );
		$('input[type=submit]').trigger("click");
	}
}

function validate_fields(){
	//Card fields
	var $card_holder_or_account_holder = $("input[name='card_holder_or_account_holder']");
	var $expiration_date = $("input[name='expiration_date']");
	var $cvv = $("input[name='cvv']");
	
	//Check fields
	var $account_number = $("input[name='account_number']");
	var $routing_code = $("input[name='routing_code']");
	
	var new_payment_method = $("select[name='new_payment_method']");
	var new_payment_method_value = new_payment_method.val();
	
	var $one_time_payment_method_ref = $("select[name='one_time_payment_method']");
	var one_time_payment_method_value = $one_time_payment_method_ref.val();
	
	var payment_method = $("select[name='payment_method']");
	var payment_method_value = payment_method.val();
	var $desc_div = $('#description');
	var $ignore_payment_method_ref = $("[name='ignore_payment_method']");
	var $payment_extension_ref = $("[name='payment_extension_days']");
	
	if( $payment_extension_ref.length && $payment_extension_ref.val() == '' ){
		alert('Please select payment extension days');
		return false;	
	}
	
//	$ignore_payment_method_ref.length < 1 == NOT FOUND
	if(($ignore_payment_method_ref.length < 1) || 
			($ignore_payment_method_ref.length && $ignore_payment_method_ref.val() == IGNORE_PAYMENT_METHOD.get('NO')) ){
		
		if( ($one_time_payment_method_ref.length && one_time_payment_method_value == '') ||
				(new_payment_method.length && new_payment_method_value == '') ){
			alert('Please select payment method');
			return false;	
		}
		else if( ($one_time_payment_method_ref.length && one_time_payment_method_value == PAYMENT_VALUES.get('Electronic_Check')) || 
				(new_payment_method.length && new_payment_method_value == PAYMENT_VALUES.get('Electronic_Check')) ){
	//		check account holder 
	//		check account number
	//		check routing code
			if($card_holder_or_account_holder.length && $card_holder_or_account_holder.val().trim() == ''){
				alert('Please fill account holder name');
				return false;
			}
			else if($account_number.length && $account_number.val().trim() == ''){
				alert('Please fill account number');
				return false;
			}
			else if($routing_code.length && $routing_code.val().trim() == ''){
				alert('Please fill routing');
				return false;
			}
			
			
		}
	}
	
	
	if($desc_div.length){
		var $description = $('#description_value');
		var description = $description.val();
		var description_check = description + ' ';
		description_check = description.replace(/(&nbsp;)+/g, '');
		description_check = description_check.replace(/<p>/g, '');
		description_check = description_check.replace(/<\/p>/g, '');
		description_check = description_check.replace(/<br>/g, '');
		description_check = $.trim(description_check);
		if(description_check == ''){
			alert('Please fill description');
			return false;
		}
	}
	
	
	var payment_enum = {"Add_New_Card":"Add New Card", "Electronic_Check":"Electronic Check"}
	
	if(payment_method.length && payment_method_value == ''){
		alert('Please select payment method');
		return false;	
	}
	
	else if(payment_method.length && payment_method_value == payment_enum.Electronic_Check){
//		check account holder 
//		check account number
//		check routing code
		if($card_holder_or_account_holder.length && $card_holder_or_account_holder.val().trim() == ''){
			alert('Please fill account holder name');
			return false;
		}
		else if($account_number.length && $account_number.val().trim() == ''){
			alert('Please fill account number');
			return false;
		}
		else if($routing_code.length && $routing_code.val().trim() == ''){
			alert('Please fill routing');
			return false;
		}
		
		
	}
	
	return true;
	
}

$("#id_card_expiry").attr('placeholder', 'MM / YYYY');

jQuery("#create_ticket_form").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);
    var url = form.attr('action');
    
    $.ajax({
           type: "POST",
           url: url,
           data: form.serialize(),
           success: function(data) {
				jQuery('#ticket_submit_msg').removeClass('alert-success').removeClass('alert-danger').removeClass('no-display');
				jQuery('#ticket_submit_msg').text(data.msg);

				if(data.status == 'Failure') {
					jQuery('#ticket_submit_msg').addClass('alert-danger');
				} else {
					jQuery('#ticket_submit_msg').addClass('alert-success');
				}
           }
    });

});

function store_parent_category(){
	var $parent_category_dd = $('#parent_category_select');
	var selected_parent_category = $parent_category_dd.val();
	$('#selected_parent_category').val(selected_parent_category);
}

function hide_card_related_fields(){
//	$('.abcp-account-holder').hide();
	$('.abcp-card').hide();
}

function hide_account_card_related_fields(){
	$('.abcp-card-account-fields').hide();
}

function show_account_card_related_fields(){
	$('.abcp-card-account-fields').show();
}	

function show_check_related_fields(){
	var $card_holder_ref = $('.abcp-account-holder label.col-lg-3 label');
	var $missing_check_fields_ref = $('#id_missing_check_fields');
	$card_holder_ref.text('Account Holder Name:');
	$('.abcp-account-holder').show();
	if(!eval($missing_check_fields_ref.val())){
		$('.abcp-check').show();
	}
	else{
		var $missing_check_field_msg_ref = $('#id_missing_check_field_msg_div');
		$missing_check_field_msg_ref.show();
	}
}

function hide_check_related_fields(){
	$('.abcp-account-holder').hide();
	$('.abcp-check').hide();
}

function apply_icheck_handler_if_cc_or_echeck_exist(){
	var last_four_digit_ref = $("[name='last_four_digits_of_credit_card_on_file']"); 
	var $missing_card_fields_ref = $('#id_missing_card_fields');
	var $missing_check_fields_ref = $('#id_missing_check_fields');
	if(	!eval($missing_card_fields_ref.val()) || !eval($missing_check_fields_ref.val())  ){
		var $submit_btn_ref = $('.abcp-submit-btn');
		var $pay_through_card_ref = $("[name='i_agree_to_pay_through_my_card_on_file']");
		var $modify_card_ref = $("[name='i_agree_to_modify_my_card_payment_details']");
		var $modify_e_check_ref = $("[name='i_agree_to_modify_my_echeck_payment_details']");
		var $e_check_ref = $("[name='i_agree_to_pay_through_my_echeck_on_file']");
		
		apply_check_uncheck_handler($e_check_ref, $submit_btn_ref);
		apply_check_uncheck_handler($pay_through_card_ref, $submit_btn_ref);
		apply_check_uncheck_handler($modify_card_ref, $submit_btn_ref);
		apply_check_uncheck_handler($modify_e_check_ref, $submit_btn_ref);
	}
}

function disable_form_btn(){
	var $submit_btn_ref = $('.abcp-submit-btn');
	$submit_btn_ref.addClass('disabled');
}

function apply_check_uncheck_handler($elem_ref, $submit_btn_ref){
	$($elem_ref).on('ifChecked', function(event){
		$submit_btn_ref.removeClass('disabled');
	});
	
	$($elem_ref).on('ifUnchecked', function(event){
		$submit_btn_ref.addClass('disabled');
	});
}

function enable_form_btn_if_other(){
	var $submit_btn_ref = $('.abcp-submit-btn');
	$submit_btn_ref.removeClass('disabled');
}


function show_card_related_fields(){
	var $missing_card_fields_ref = $('#id_missing_card_fields');
	if(!eval($missing_card_fields_ref.val())){
		$('.abcp-card').show();
	}
	else{
		var $missing_card_field_msg_ref = $('#id_missing_card_field_msg_div');
		$missing_card_field_msg_ref.show();
	}
}	

function hide_missing_card_field_msg(){
	var $missing_card_field_msg_ref = $('#id_missing_card_field_msg_div');
	$missing_card_field_msg_ref.hide();
}

function hide_missing_check_field_msg(){
	var $missing_check_field_msg_ref = $('#id_missing_check_field_msg_div');
	$missing_check_field_msg_ref.hide();
}

function hide_cable_length(){
	$('.abcp-cable-length').hide();
}

function show_cable_length(){
	$('.abcp-cable-length').show();
}

function show_equipment_info(){
	$('#new_equipment_info').show();
}
function hide_equipment_info(){
	$('#new_equipment_info').hide();
}

function on_equipment_click(){
	var $new_order_equipment = $("select[name='equipment']");
	if($new_order_equipment.val() == 7){
		show_cable_length();
		show_equipment_info();
	}
	else{
		hide_cable_length();
		hide_equipment_info();
	}
}

function populate_div(url,div){
  $.ajax({
        type : "get",
        url: url,
        success: function(response){
        	$(div).html(response);
    }});
}

function on_new_services_group_click(){
	var $new_services_group_ref = $("[name='desired_new_services_group']");
	var new_services_group_val = $new_services_group_ref.val(); 
	if(new_services_group_val != ''){
		//Call ajax and get services page and populate desired_services_div
		$('#service_description').html('');
		populate_div('/tickets/services/' + new_services_group_val,'#desired_services_div');
	}
	else{
		$('#desired_services_div').html('');
		$('#service_description').html('');
	}
}

function stop_interval(interval_var) {
	console.log('------Stop');
    clearInterval(interval_var);
}

var radio_stats_enum = {'NOT_STARTED':0, 'INPROGRESS':1, 'COMPLETED':2}

function Check_if_radiostats_completed(interval_var){
	console.log('------Start');
	
	$.ajax({
        type : 'get',
        url: '/radiostats/get_radiostats/',
		success: function(response){
			if(response.call_status == radio_stats_enum.COMPLETED){
				var $submit_btn_ref = $('.abcp-submit-btn');
				var $submit_container_ref = $('#id_submit_container');
				$submit_container_ref.prop('title', '');
				$submit_btn_ref.removeClass('disabled');
				stop_interval(interval_var);
			}
		}
    });
}

function run_radiostats(){
	$.ajax({
        type : 'get',
        url: '/radiostats/get_radiostats/',
        success: function(response){
     		//console.log('-------Call speed test');   
    }});
}

function disable_submit_btn(){
	var parent_category_enum = {
			'Technical_Support':1,
			'Account_Services':2,
			'Billing':3,
			'Genreal':4
	}
	var $parent_category_dd = $('#parent_category_select');
	var selected_parent_category = $parent_category_dd.val();
	if(selected_parent_category == parent_category_enum.Technical_Support){
		var $submit_btn_ref = $('.abcp-submit-btn');
		var $submit_container_ref = $('#id_submit_container');
		$submit_container_ref.prop('title', 'Please wait...Radio stats is in progress');
		$submit_btn_ref.addClass('disabled');
		$.ajax({
	        type : 'get',
			url: '/radiostats/is_radio_stats_completed',
			success: function(response){
				if(response.call_status == radio_stats_enum.NOT_STARTED){
					//console.log('-----------Running speedtest');
					run_radiostats();
					var interval_var = setInterval(function(){ Check_if_radiostats_completed(interval_var) }, 2000);
				}
				else if(response.call_status == radio_stats_enum.INPROGRESS){
					var interval_var = setInterval(function(){ Check_if_radiostats_completed(interval_var) }, 2000);
				}
				else if(response.call_status == radio_stats_enum.COMPLETED){
					var $submit_btn_ref = $('.abcp-submit-btn');
					var $submit_container_ref = $('#id_submit_container');
					$submit_container_ref.prop('title', '');
					$submit_btn_ref.removeClass('disabled');

				}
				
			}
	    });
		
	}
}
//# sourceURL=create_form.js