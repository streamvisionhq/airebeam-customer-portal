apply_config('all_tickets');
apply_config('recent_tickets_table');

function apply_config(id){
	$('#' + id).dataTable({
		"sPaginationType": "bootstrap",
		aaSorting: [[3, 'desc']],
		"oLanguage": {
			"sSearch": "_INPUT_",
		}
	});

	$('.dataTables_filter input').attr("placeholder", "Search");
}