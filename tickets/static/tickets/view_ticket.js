var view_ticket_module = (function () {
  
	var radiostats_enum = {'NOT_STARTED':0, 'INPROGRESS':1, 'COMPLETED':2}
	
	var _run_radiostats = function(){
		$.ajax({
	        type : 'get',
	        url: '/radiostats/get_radiostats/',
	        success: function(response){
	        	//console.log('-------Call speed test');   
	    }});
	};
	
	var _stop_interval = function(interval_var) {
		console.log('------Stop');
	    clearInterval(interval_var);
	}

	var _Check_if_radiostats_completed = function(interval_var){
		console.log('------Start');
		
		$.ajax({
	        type : 'get',
	        url: '/radiostats/get_radiostats/',
			success: function(response){
				if(response.call_status == radiostats_enum.COMPLETED){
					var $submit_btn_ref = $('#id_reply_btn');
					var $submit_container_ref = $('#id_reply_container');
					$submit_container_ref.prop('title', '');
					$submit_btn_ref.removeClass('disabled');
					_stop_interval(interval_var);
				}
			}
	    });
	}
	
	var _disable_submit_btn = function(){
		var $is_support_ticket_ref = $('#is_support');
		var $is_support_ticket_val = parseInt($is_support_ticket_ref.val());
		if($is_support_ticket_val){
			var $submit_btn_ref = $('#id_reply_btn');
			var $submit_container_ref = $('#id_reply_container');
			$submit_container_ref.prop('title', 'Please wait...Radiostats is in progress');
			$submit_btn_ref.addClass('disabled');
			$.ajax({
		        type : 'get',
	            url: '/radiostats/is_radio_stats_completed',
				success: function(response){
					if(response.call_status == radiostats_enum.NOT_STARTED){
						//console.log('-----------Running speedtest');
						_run_radiostats();
						var interval_var = setInterval(function(){ _Check_if_radiostats_completed(interval_var) }, 2000);
					}
					else if(response.call_status == radiostats_enum.INPROGRESS){
						var interval_var = setInterval(function(){ _Check_if_radiostats_completed(interval_var) }, 2000);
					}
					else if(response.call_status == radiostats_enum.COMPLETED){
						var $submit_btn_ref = $('#id_reply_btn');
						var $submit_container_ref = $('#id_reply_container');
						$submit_container_ref.prop('title', '');
						$submit_btn_ref.removeClass('disabled');
					}
					
				}
		    });
			
		}
	};
  
	var _populate_conversations = function(type, url, div){
		$('.site-holder').mask('<i class="  fa fa-refresh fa-spin"></i> Loading...');
		$('.site-holder .loadmask-msg').addClass('acp-loader-position');
		$('.site-holder .loadmask-msg div').addClass('acp-loader-font');
	    $.ajax({
	        type : type,
	        url: url,
	        success: function(response){
	        	$('#summernote').code('');
	        	$(div).html('');
	        	$(div).html(response);
	        	$('.site-holder .loadmask-msg').addClass('acp-loader-position');
	        	$('.site-holder .loadmask-msg div').addClass('acp-loader-font');
	        	$('.site-holder').unmask();
	      }
	    });
	};

	var _get_ticket_conversations = function(){
		var $ticket_id = $('#ticket_id');
		var url = "/tickets/get_ticket_conversation" + "/" + $ticket_id.val();
		var type = "get";
		var ticket_conv_div_id = '#ticket_conv_container';
		_populate_conversations(type, url, ticket_conv_div_id);
	};
  
	var _update_ticket_service = function(type, url, div){
	    $.ajax({
	        type : type,
	        url: url,
	        success: function(response){
	        	console.log(response);
	        	console.log('Ticket updated');
	      }
	    });
	};

	var _update_ticket = function(){
		var $ticket_id = $('#ticket_id');
		var url = "/tickets/update_ticket" + "/" + $ticket_id.val();
		var type = "get";
		var $is_read_ref = $('#is_read');
		var is_read_val = $is_read_ref.val();
		var read_enum = {"read":1, "unread":0}
		if(parseInt(is_read_val) == read_enum.unread){
			_update_ticket_service(type, url);
		}
	};
	
	var _get_csrf_token =  function(xhr, settings) {
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            // Only send the token to relative URLs i.e. locally.
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
	};
	
	var _make_reply_service = function(type, url, msg, is_support){
		$('.site-holder').mask('<i class="  fa fa-refresh fa-spin"></i> Loading...');
		$('.site-holder .loadmask-msg').addClass('acp-loader-position');
    	$('.site-holder .loadmask-msg div').addClass('acp-loader-font');
		$.ajax({
			beforeSend: _get_csrf_token,
		
	        type : type,
			url: url,
			data:{body:msg, is_support:is_support},
			success: function(response){
				if(response.status == 0){
					$('.site-holder .loadmask-msg').addClass('acp-loader-position');
					$('.site-holder .loadmask-msg div').addClass('acp-loader-font');
					$('.site-holder').unmask();
					window.location = '/users/logout'
				}
				else{
					_get_ticket_conversations();
				}
				
			}
	    });
	};
	
	var _reply_btn_click = function(event){
		event.preventDefault();
		var description_editior_id = 'summernote';  
		var description_storage_id = 'reply_text';
		var ticket_field_id = 'ticket_id';
		var is_support = 'is_support';
		
		get_and_store_description(description_editior_id, description_storage_id);
		var $ticket_id = $('#' + ticket_field_id);
		var $ticket_attachment = $('#' + is_support);
		var ticket_attachment_value = $ticket_attachment.val();
		var url = "/tickets/create_reply" + "/" + $ticket_id.val();
		var type = "post";
		var msg = $('#'+ description_storage_id).val();
		msg= $.trim(msg)
		var msg_check = msg + ' ';
		msg_check = msg.replace(/(&nbsp;)+/g, '');
		msg_check = msg_check.replace(/<p>/g, '');
		msg_check = msg_check.replace(/<\/p>/g, '');
		msg_check = msg_check.replace(/<br>/g, '');
		msg_check = $.trim(msg_check);
		if(msg_check != ''){
			_make_reply_service(type, url, msg, ticket_attachment_value)
		}
		else{
			alert('Please fill description in reply');
		}
	};
	
	var _cancel_btn_click = function(){
		go_to_dashboard();
	}
	
	var _bind_elements = function() {
	    $("button#id_reply_btn").on("click", _reply_btn_click);
	    $("button#id_cancel_btn").on("click", _cancel_btn_click);
	};
	  
	var init = function(){
		_bind_elements();
//		Disable reply button if radio stats have not fetched yet
		_disable_submit_btn();
//		Hide password from customer
		$('.hide_old_pass').html('');
		$('.hide_account_pass').html('');
//		Hide radio stats failed meesage from customer
		$('.stats-failed-msg').html('');
		$('.site-holder').addClass('fixed-loadmask-msg');
		setTimeout(function(){
			_get_ticket_conversations();			
			_update_ticket();
		}, 1000);
	}
  
	return {
		init: init,
	};

})();

$( document ).ready(function() {
//	Initialize view ticket module
	view_ticket_module.init();
});