$('#summernote').summernote({
  height: 200,
  focus: true,   //set focus editable area after Initialize summernote
	toolbar: [
	          // [groupName, [list of button]]
	          ['acp-font-style', ['bold', 'underline', 'clear']],
	          ['acp-font-color', ['color']],
	          ['acp-paragraph', ['ul', 'ol', 'paragraph']],
	          ['acp-insert', ['table', 'link']],
	          ['acp-help', ['help']]
	        ]
});