var PAYMENT_VALUES = (function() {
     var private = {
         'Electronic_Check': 1,
         'Credit_Card': 2
     };

     return {
        get: function(name) { return private[name]; }
    };
})();

var IGNORE_PAYMENT_METHOD = (function() {
    var private = {
        'YES': 1,
        'NO': 0
    };

    return {
       get: function(name) { return private[name]; }
   };
})();

function GetSupport(type, url) {
	  return {
	    populate_form: function(){
		  	$.ajax({
		        type : type,
				url: url,
				success: function(response){
					if(response.status == 0){
						window.location = '/users/logout'
					}
					else{
						$("#dynamic_form").html(response);
						$("#id_card_expiry").unmask().mask('00/0000');
						var $sub_category_selected_text = $('#sub_categories_select option:selected').text().trim();
						var $parent_category_selected_text = $('#parent_category_select option:selected').text().trim();
						$('[name="fd_category"]').val($parent_category_selected_text);
						$('[name="fd_subcategory"]').val($sub_category_selected_text);
					}
					
				}
		    });
		  },
		  
	   populate_sub_categories: function(){
		   $.ajax({
		        type : type,
				url: url,
				success: function(response){
					$("#sub_category_dd").html(response);
				}
		    });
	   },
	   
	   populate_service_description: function(){
		   $.ajax({
		        type : type,
				url: url,
				success: function(response){
					$("#service_description").html(response);
				}
		    });
	   }
	  
	  };
	}

function show_form(id){
	var $sub_category = $('#' + id);
	var sub_cat_id = $sub_category.val();
	var get_support_obj = GetSupport("get", "/tickets/get_form" + "/" + sub_cat_id);
	get_support_obj.populate_form();
	
	
}

function populate_sub_categories(){
	remove_form('dynamic_form');
	var $parent_category_dd = $('#parent_category_select');
	var selected_parent_category = $parent_category_dd.val();
	var get_support_obj = GetSupport("get", '/tickets/sub_categories/' + selected_parent_category);
	get_support_obj.populate_sub_categories();
}

function remove_form(id){
	$('#'+ id).html('');
}
// Remove this function and change storage mechanism on view ticket page
function get_and_store_description(id, storage_id){
	var $description = $("#" + id);
//	Get summernote editor value and store it in hidden field
	var description = $description.code();
	$('#' + storage_id).val(description);
}

function show_service_description(){
	var $service_select = $('.service_select')
	var service_id = $service_select.val();
	if(service_id != ''){
		$('#service_description').html('');
		endpoint_url = "/tickets/service_description" + "/" + service_id
		var get_support_obj = GetSupport("get", endpoint_url);
		get_support_obj.populate_service_description();
	}
	else{
		$('#service_description').html('');
	}
}

function show_correponding_fields(){
	var payment_method = $("select[name='new_payment_method']");
	var payment_method_value = payment_method.val();
	
	if(payment_method_value == PAYMENT_VALUES.get('Electronic_Check')){
		hide_card_related_fields();
		hide_missing_card_field_msg();
		show_check_related_fields();
		disable_form_btn();
		apply_icheck_handler_if_cc_or_echeck_exist();
		show_account_card_related_fields();
	}
	else if(payment_method_value ==  PAYMENT_VALUES.get('Credit_Card') ){
		hide_check_related_fields();
		hide_missing_check_field_msg();
		show_card_related_fields();	
		disable_form_btn();
		apply_icheck_handler_if_cc_or_echeck_exist();
		show_account_card_related_fields();
	}
	else{
		hide_card_related_fields();
		hide_missing_card_field_msg();
		hide_missing_check_field_msg();
		hide_check_related_fields();
		hide_account_card_related_fields();
	}
}

function show_one_time_method_fields(){
	var payment_method = $("select[name='one_time_payment_method']");
	var payment_method_value = payment_method.val();
	var $one_time_cc_div_ref = $('#one_time_cc_msg_div');
	var $one_time_echeck_div_ref = $('#one_time_echeck_msg_div');
	
	var $one_time_card_fields = $('.abcp-card-fields');

	if(payment_method_value == PAYMENT_VALUES.get('Electronic_Check')){
		hide_one_time_payment_msg($one_time_cc_div_ref);
		hide_one_time_payment_msg($one_time_card_fields);
		show_one_time_payment_msg($one_time_echeck_div_ref);
		show_account_card_related_fields();
	}
	else if(payment_method_value == PAYMENT_VALUES.get('Credit_Card') ){
		hide_check_related_fields();
		hide_one_time_payment_msg($one_time_echeck_div_ref);
		show_one_time_payment_msg($one_time_cc_div_ref);
		show_one_time_payment_msg($one_time_card_fields);
		show_account_card_related_fields();
	}
	else{
		hide_one_time_payment_msg($one_time_echeck_div_ref);
		hide_one_time_payment_msg($one_time_cc_div_ref);
		hide_one_time_payment_msg($one_time_card_fields);
		hide_check_related_fields();
		hide_account_card_related_fields();
	}
}

function hide_one_time_payment_msg($div_ref){
	$div_ref.hide();
}

function show_one_time_payment_msg($div_ref){
	$div_ref.show();
}

function show_payment_method_dd(){
	var $payment_extension_days_ref = $("[name='payment_extension_days']");
	var payment_extension_days_val = $payment_extension_days_ref.val();
	var payment_extension_enum = {"ONE_TO_FOUR_DAYS": 1, "FIVE_DAYS_OR_LONGER": 2};
	var $ignore_payment_method_ref = $("[name='ignore_payment_method']");
	var payment_method = $("select[name='new_payment_method']");
	var $submit_btn_ref = $('.abcp-submit-btn');
	
	if(payment_extension_days_val > payment_extension_enum.ONE_TO_FOUR_DAYS){
		$ignore_payment_method_ref.val(IGNORE_PAYMENT_METHOD.get('NO'));
		show_payment_options();
	}
	else{
		$ignore_payment_method_ref.val(IGNORE_PAYMENT_METHOD.get('YES'));
		hide_payment_options();
		payment_method.val('');
		hide_card_related_fields();
		hide_check_related_fields();
		//clear_check_fields();
		$submit_btn_ref.removeClass('disabled');		
	}
	
}



function show_payment_method_fields(){
	var payment_enum = {"Add_New_Card":"Add New Card", "Electronic_Check":"Electronic Check"}
	var payment_method = $("select[name='payment_method']");
	var payment_method_value = payment_method.val();
	if(payment_method_value == payment_enum.Electronic_Check){
		show_check_related_fields();
	}
	else{
		hide_check_related_fields();
	}
}

function switch_account(input_mbr_id){
	var input_mbr_ref = $('#' + input_mbr_id);
	var mbr = input_mbr_ref.val();
	if(mbr.trim() != ''){
		$('.site-holder').addClass('fixed-loadmask-msg');
		$('.site-holder').mask('<i class="  fa fa-refresh fa-spin"></i> Loading...');
		$('.site-holder .loadmask-msg').addClass('acp-loader-position');
		$('.site-holder .loadmask-msg div').addClass('acp-loader-font');
		$.ajax({
	    	cache: false,
	        type : "get",
	        url: '/mbr_online_check/mbr_found_on_emerald/' + input_mbr_ref.val(),
	        success: function(response){
	        	$('.site-holder .loadmask-msg').addClass('acp-loader-position');
	    		$('.site-holder .loadmask-msg div').addClass('acp-loader-font');
	        	$('.site-holder').unmask();
	        	$('.site-holder').removeClass('fixed-loadmask-msg');
	        	window.location = '/';
	      }
	    });
	}
	else{
		alert('Please fill MBR number');
	}
	
}

function mbr_exist(url){
    $.ajax({
    	cache: false,
        type : "get",
        url: url,
        success: function(response){
        	if(response.mbr_exist){
        		$('.acp-mbr-error.hide-until-populated').hide();
        		$('.main-content.hide-until-populated').addClass('show');
        		$('.acp-mbr-dependent.hide-until-populated').addClass('show');
        	}
        	else{
//        		Hide main content div
        		$('.acp-mbr-error.hide-until-populated').addClass('show');
        	}
      }
    });
 }

$(document).ready(function() {
	mbr_exist('/mbr_online_check/mbr_exist/');
	$("#input_mbr").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
        	
        	if( e.which == 13 ) {
	   		     event.preventDefault();
	   		     switch_account('input_mbr')
	   		}
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
        
    });

 });
