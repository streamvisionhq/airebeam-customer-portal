class TicketStatus:
    OPEN = 2
    PENDING = 3
    RESOLVED = 4
    CLOSED = 5
    Waiting_on_Customer = 6
    Waiting_on_Third_Party = 7
    Dispatched = 8
    
class TicketPriority:
    LOW = 1
    MEDIUM = 2 
    HIGH = 3
    URGENT = 4

ticket_string_map = {2:'Open', 3:'Pending', 4:'Resolved', 5:'Closed', 6:'Waiting on Customer',
                     7:'Waiting on Third Party', 8:'Dispatched'}

new_payment_method_string_map = {1:'Electronic Check', 2:'Credit Card'}

payment_method_string_map = { 2:'Add New Card', 3:'Electronic Check' }

equipment_string_map = {1:"Wireless Router - $99.95 including delivery, tax and installation", 
                        2:"Wireless Extender - $99.95 including delivery, tax and installation", 
                        3:"5-Port Ethernet Switch - $49.95 including delivery, tax and installation", 
                        4:"8-Port Ethernet Switch - $69.96 including delivery, tax and installation", 
                        5:"Ethernet Cable - 5' - $5.95 including tax",
                        6:"Ethernet Cable - 10' - $10.95 including tax", 
                        7:"Ethernet Cable - custom length - $1.10 / foot including tax"}

payment_extension_string_map = {1:'1-4 days Free', 2:'5 days or longer, $5'}

services_and_prices_map = {1:'Roku 2 Rental', 2:'Wireless Router Rental', 3:'Internet',
                           4:'Begin Summer Break', 5:'DVR Basic', 6:"DVR Storage - 100 hrs Adt'l",
                           7:'DVR Whole Home', 8:'End Summer Break', 9:'ePhone - Canada',
                           10:'ePhone - International', 11:'ePhone - Local/US', 12:'P0105',
                           13:'P0201', 14:'P0402', 15:'P0603', 16:'P0804', 17:'P1005',
                           18:'P1608', 19:'P2010', 20:'StreamVisionGold'}

desired_new_services_group_map = {1:'Internet Package', 2:'ePhone', 3:'Summer Break',
                                  4:'StreamVision', 5:'Equipment Rental'}


class ParentCategories:
    Support = 1
    Account_Services = 2
    Billing = 3
    General = 4
    
card_field_names = ['cvv', 'expiration_date_if_card', 'last_four_digits_of_credit_card_on_file', 
                    'i_agree_to_pay_through_my_card_on_file', 'i_agree_to_modify_my_card_payment_details']

check_fields_names = ['card_holder_or_account_holder','account_number', 'routing_code', 'i_agree_to_pay_through_my_echeck_on_file',
                      'i_agree_to_modify_my_echeck_payment_details']

group_id_mappings = {'support':1000097090}

class TicketType:
    OTHER = 'Other'
    PORTAL_SETUP = 'Portal Setup'
    UPDATE_CUSTOMER_DATA = 'Update Customer Data'
      

class ServicesGroup:
    NO_GROUP = 0
    INTERNET_SERVICES = 1
    EPHONE = 2
    STREAMVISION = 4
    EQUIPMENT_RENTAL = 5 
