from django.conf.urls import url

from tickets import views_function
from tickets.views.CustomerTicketsView import CustomerTicketsView
from tickets.views.CustomerAllTicketsView import CustomerAllTicketsView
from tickets.views.TicketDetailView import TicketDetailView
from tickets.views.CategoriesView import CategoriesView
from tickets.views.SubCategoriesView import SubCategoriesView
from tickets.views.TicketConversationView import TicketConversationView
from tickets.views.ServicesView import ServicesView
from tickets.views.ServiceDetailView import ServiceDetailView
from tickets.views.TicketUpdateView import TicketUpdateView
from tickets.views.TicketReplyView import TicketReplyView
from tickets.views.TicketCreateForm import TicketCreateFormView 
from tickets.views.GenerateUpdatePaymentTicket import GenerateUpdatePaymentTicket
from tickets.views.ThanksUpdatePaymentView import ThanksUpdatePaymentView

app_name = 'tickets'
urlpatterns = [
	url(r'^categories$', CategoriesView.as_view(), name = 'categories'),
	url(r'^get_form/(?P<sub_cat_id>[0-9]+)$', TicketCreateFormView.as_view(), name = 'get_form'),
	url(r'^index$', CustomerAllTicketsView.as_view(), name = 'index'),
	url(r'^view_ticket/(?P<ticket_id>[0-9]+)$', TicketDetailView.as_view(), name = 'view_ticket'),
	url(r'^create_reply/(?P<ticket_id>[0-9]+)$', TicketReplyView.as_view(), name = 'create_reply'),
	url(r'^customer_recent_tickets$', CustomerTicketsView.as_view(), name = 'customer_recent_tickets$'),
	url(r'^sub_categories/(?P<sub_cat_id>[0-9]+)$', SubCategoriesView.as_view(), name = 'sub_categories'),
	url(r'^get_ticket_conversation/(?P<ticket_id>[0-9]+)$', TicketConversationView.as_view(), name = 'get_ticket_conversation'),
	url(r'^service_description/(?P<service_id>[0-9]+)$', ServiceDetailView.as_view(), name = 'service_description'),
	url(r'^updated_wh', views_function.updated_wh, name = 'updated_webhook'),
	url(r'^update_ticket/(?P<ticket_id>[0-9]+)$', TicketUpdateView.as_view(), name = 'update_ticket'),
	url(r'^services/(?P<group_id>[0-9]+)$', ServicesView.as_view(), name = 'services'),
	url(r'^generate_up_ticket$', GenerateUpdatePaymentTicket.as_view(), name = 'generate_up_ticket'),
	url(r'^thanks_update_payment$', ThanksUpdatePaymentView.as_view(), name = 'thanks_update_payment'),
	
]