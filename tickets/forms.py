from django import forms
import json
from models import Forms, Services
from airebeam_customer_portal.enum import form_fields_map, FieldsType, AppStatus
import datetime
from enum import new_payment_method_string_map, equipment_string_map, payment_extension_string_map
from airebeam_customer_portal.utils import getfromSession
from enum import payment_method_string_map, services_and_prices_map
from airebeam_customer_portal.User import User
from airebeam_customer_portal.enum import user_access_level
from airebeam_customer_portal.AppServices.UserEmeraldServicesApi import UserEmeraldServicesApi
from tickets.enum import desired_new_services_group_map
from airebeam_customer_portal.AppUrl import AppUrl
from airebeam_customer_portal import config
from airebeam_customer_portal.static_values import AppMsgs, MbrDetailKeys,\
    AppFormName
from airebeam_customer_portal.AppServices.TicketService import TicketService

class DynamicForm(forms.Form):
    mbr_detail = ''
    __request = ''
    def __init__(self, request, sub_cat_id, mbr_detail, *args, **kwargs):
        super(DynamicForm, self).__init__(*args, **kwargs)
        output = ''
        self.mbr_detail = mbr_detail
        self.__request = request
        form_fields_row = Forms.get_form(sub_cat_id)
        self.__process_form_fields_json(form_fields_row)
    
    def set_eft_account_last_four(self, eft_last_four_digits):
        if 'account_number' in self.fields:
            self.fields['account_number'].initial = str(eft_last_four_digits)
            self.fields['account_number'].widget.attrs['readonly'] = True
    
    def set_eft_routing_number(self, routing_number):
        if 'routing_code' in self.fields:
            self.fields['routing_code'].initial = str(routing_number)
            self.fields['routing_code'].widget.attrs['readonly'] = True
            
    def set_eft_auth_name(self, auth_name):
        if 'card_holder_or_account_holder' in self.fields:
            self.fields['card_holder_or_account_holder'].initial = str(auth_name)
            self.fields['card_holder_or_account_holder'].widget.attrs['readonly'] = True
        
    def set_last_four(self, last_four_digits):
        if 'last_four_digits_of_credit_card_on_file' in self.fields:
            self.fields['last_four_digits_of_credit_card_on_file'].initial = str(last_four_digits)
            self.fields['last_four_digits_of_credit_card_on_file'].widget.attrs['readonly'] = True
            
    def set_expiration_date(self, expiration_date):
        if 'expiration_date_if_card' in self.fields:
            self.fields['expiration_date_if_card'].initial = str(expiration_date)
            self.fields['expiration_date_if_card'].widget.attrs['readonly'] = True
    
    def set_customer_id(self, id):
        if 'customer_id' in self.fields:
            self.fields['customer_id'].initial = str(id)
            self.fields['customer_id'].widget.attrs['readonly'] = True
    
    def set_account_holder_name(self, name):
        if 'account_hold_name' in self.fields:
            self.fields['account_hold_name'].initial = str(name)
            self.fields['account_hold_name'].widget.attrs['readonly'] = True
            
    def set_address(self, address):
        if 'street_address_number' in self.fields:
            self.fields['street_address_number'].initial = str(address)
            self.fields['street_address_number'].widget.attrs['readonly'] = True
        elif 'current_address' in self.fields:
            self.fields['current_address'].initial = str(address)
            self.fields['current_address'].widget.attrs['readonly'] = True
            
    def set_email_address(self, email_address):
        if 'contact_email_address' in self.fields:
            self.fields['contact_email_address'].initial = str(email_address)
            self.fields['contact_email_address'].widget.attrs['readonly'] = True
        elif 'email_address' in self.fields:
            self.fields['email_address'].initial = str(email_address)
            self.fields['email_address'].widget.attrs['readonly'] = True
            
    def set_phone_number(self, phone_number):
        if 'contact_phone_number' in self.fields:
            self.fields['contact_phone_number'].initial = str(phone_number)
            self.fields['contact_phone_number'].widget.attrs['readonly'] = True

    def __process_form_fields_json(self, form_fields_row):
        user_acl = user_access_level[AppMsgs.SHALLOW_KEY]
        user_access = User.get_access_level(self.__request)
        form_name = ''
        other_form = False
        delay_form = False
        if form_fields_row:
            for e in  form_fields_row:
                output = e.fields
                data = json.loads(output)
                form_name = data[AppMsgs.NAME].lower()
                form_elements = data['form_elements']
                for fe in form_elements:
                    is_shallow = fe[AppMsgs.SHALLOW_KEY]
                    if user_acl == user_access and is_shallow == 'hidden':
                        continue
                    self.__add_form_field(fe)
            
            if form_name == 'other-form':
                other_form = True
            elif form_name == AppFormName.request_delay_form.lower():
                delay_form = True
            
            self.__add_hidden_fields(form_fields_row, other_form, delay_form)
            
    def __set_required_attr(self, element_attr):
        optional_fields_lst = ['card_holder_or_account_holder', 
                                        'cvv', 'account_number',
                                        'routing_code', 'cable_length']
        if element_attr[AppMsgs.NAME] in optional_fields_lst:
            self.fields[element_attr[AppMsgs.NAME]].required = False
        else:
            self.fields[element_attr[AppMsgs.NAME]].required = True

    def __add_form_field(self, form_element):
        element_attr = json.loads(form_element['plugin_data'])
        if form_element['plugin_uid'] == FieldsType.TEXT:
            if element_attr[AppMsgs.NAME] == 'cable_length':
                self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']](max_length = 6,
                                                    label=element_attr['label'],
                                                    widget=forms.TextInput(attrs={'class':'form-control acp-number'}))
            
            else:
                self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']](max_length = 100,
                                                    label=element_attr['label'],
                                                    widget=forms.TextInput(attrs={'class':'form-control'}))
            self.__set_required_attr(element_attr)
                
        elif form_element['plugin_uid'] == FieldsType.PASSWORD: 
            self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']](max_length = 100,
                                                required=True,
                                                label=element_attr['label'],
                                                widget=forms.PasswordInput(attrs={'class':'form-control'}))
        elif form_element['plugin_uid'] == FieldsType.SELECT:
            dd_options = [['', 'Select']]
            if element_attr['choices'] != "":
                dd_choices = element_attr['choices']
                dd_choices_lst = dd_choices.split('_')
                dd_options += [dd_choices_lst[r:r+2] for r in range(0,len(dd_choices_lst),2)]
            
            if 'desired_new_services' == element_attr[AppMsgs.NAME]:
                pass
            elif 'new_payment_method' == element_attr[AppMsgs.NAME]:
                self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']]\
                                                    (required=True, choices=dd_options, 
                                                     label=element_attr['label'],
                                                     widget=forms.Select(attrs={'class':'form-control', 
                                                                                'onchange':'show_correponding_fields()'}))
                                                    
            elif 'one_time_payment_method' == element_attr[AppMsgs.NAME]:
                self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']]\
                                                    (required=True, choices=dd_options, 
                                                     label=element_attr['label'],
                                                     widget=forms.Select(attrs={'class':'form-control', 
                                                                                'onchange':'show_one_time_method_fields()'}))
            elif 'payment_extension_days' == element_attr[AppMsgs.NAME]:
                self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']]\
                                                    (required=True, choices=dd_options, 
                                                     label=element_attr['label'],
                                                     widget=forms.Select(attrs={'class':'form-control', 
                                                                                'onchange':'show_payment_method_dd()'}))
            
#            Remove if it is not used
            elif 'payment_method' == element_attr[AppMsgs.NAME]:
                account_last_four_digit = self.mbr_detail['LASTFOUR']
                if account_last_four_digit is not None:
                    account_last_text = 'xxxx-xxxx-' + str(account_last_four_digit) 
                    dd_options += [[account_last_text, account_last_text]]
                dd_options += [['Add New Card', 'Add New Card'], ['Electronic Check', 'Electronic Check']]
                self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']]\
                                                    (required=True, choices=dd_options, 
                                                     label=element_attr['label'],
                                                     widget=forms.Select(attrs={'class':'form-control service_select', 
                                                                                'onchange':'show_payment_method_fields()'}))
                
            elif 'services_and_prices' == element_attr[AppMsgs.NAME]:
                self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']]\
                                                    (required=True, choices=dd_options, 
                                                     label=element_attr['label'],
                                                     widget=forms.Select(attrs={'class':'form-control service_select', 
                                                                                'onchange':'show_service_description()'}))
            elif 'equipment' == element_attr[AppMsgs.NAME]:
                self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']]\
                                                    (required=True, choices=dd_options, 
                                                     label=element_attr['label'],
                                                     widget=forms.Select(attrs={'class':'form-control new_equipment_select', 
                                                                                'onchange':'on_equipment_click()'}))
            elif 'existing_services' == element_attr[AppMsgs.NAME]:
                mbr = self.mbr_detail[MbrDetailKeys.MBR_KEY]
                url = AppUrl.get_airebeam_service_url(mbr)
                user_services_object = UserEmeraldServicesApi(url)
                user_service_response = user_services_object.call_service()
                filter_services = ['0-CustomerAccessOK']
                if user_service_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
                    user_services = user_service_response[AppMsgs.RESP]['services']
                    for s in user_services:
                        if s not in filter_services:
                            service = [s, s]
                            dd_options.append(service)
                        
                self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']]\
                                                    (required=True, choices=dd_options, 
                                                     label=element_attr['label'],
                                                     widget=forms.Select(attrs={'class':'form-control' }))
                                                    
            elif 'desired_new_services_group' == element_attr[AppMsgs.NAME]:
                service_name_class = 'service-class'
                if 'new' in element_attr['label'].lower():
                    service_name_class = 'new-service-class'
                groups = Services.get_groups()
                dd_options += [[grp.id, grp.name] for grp in groups]
                self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']]\
                                                    (required=True, choices=dd_options, 
                                                     label=element_attr['label'],
                                                     widget=forms.Select(attrs={'class':'form-control new_services_group_select ' 
                                                                                + service_name_class,
                                                                                'onchange':'on_new_services_group_click()' }))                                        
            
            else:
                self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']]\
                                                    (required=True, choices=dd_options, 
                                                     label=element_attr['label'],
                                                     widget=forms.Select(attrs={'class':'form-control'}))
        elif form_element['plugin_uid'] == FieldsType.DATE:
            year_list = range(2016,2050)
            self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']]\
                                                (label=element_attr['label'], widget=forms.SelectDateWidget(years=year_list))
        elif form_element['plugin_uid'] == FieldsType.CHECKBOX:
            self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']](label=element_attr['label'],
                                                                                            required=True,
                                                                                            widget=forms.CheckboxInput\
                                                                                            (attrs={'class':'square-input'}))
        elif form_element['plugin_uid'] == FieldsType.RADIO:
            radio_choices = element_attr['choices']
            radio_choices_lst = radio_choices.split(',')
            radio_options = [radio_choices_lst[r:r+2] for r in range(0,len(radio_choices_lst),2)]
            self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']](label=element_attr['label'],
                                                                                            required=True,
                                                                                            choices=radio_options,
                                                                                            widget=forms.RadioSelect\
                                                                                            (attrs={'class':'square-input'}))
        elif form_element['plugin_uid'] == FieldsType.TEXTAREA: 
            self.fields[element_attr[AppMsgs.NAME]] = form_fields_map[form_element['plugin_uid']](label=element_attr['label'],
                                                                                            widget=forms.Textarea)
            
    def __add_hidden_fields(self, form_fields_row, other_form, delay_form):
        TestStr = config.TestStr
        category_name = form_fields_row[0].name.encode('utf-8')
        ticket_type = form_fields_row[0].fd_type.encode('utf-8')
        ticket_type = ticket_type.replace("&amp;", "&")
        ticket_subject = ''
        if other_form == False:
            ticket_subject = '{0}{1}, {2}, {3} - {4}'.format(TestStr, 
                                                           self.mbr_detail[MbrDetailKeys.MBR_KEY], 
                                                            self.mbr_detail[MbrDetailKeys.LASTNAME_KEY], 
                                                            self.mbr_detail[MbrDetailKeys.FIRSTNAME_KEY],
                                                            ticket_type)
        self.fields[AppMsgs.SUBJECT_KEY] = forms.CharField(initial=ticket_subject, widget=forms.HiddenInput)
        self.fields['fd_group'] = forms.CharField(initial=form_fields_row[0].fd_group, widget=forms.HiddenInput)
        self.fields['fd_type'] = forms.CharField(initial=form_fields_row[0].fd_type, widget=forms.HiddenInput)
        self.fields['fd_priority'] = forms.CharField(initial=form_fields_row[0].fd_priority, widget=forms.HiddenInput)
        self.fields['fd_category'] = forms.CharField(initial='', widget=forms.HiddenInput)
        self.fields['fd_subcategory'] = forms.CharField(initial='', widget=forms.HiddenInput)
        
        if delay_form:
            self.fields['ignore_payment_method'] = forms.CharField(initial=1, widget=forms.HiddenInput)
            
    
    @classmethod
    def __get_form_elements(self, sub_cat_id):
        form_fields_row = Forms.get_form(sub_cat_id)
        
        form_elements = []
        if form_fields_row:
            form_fields_row = form_fields_row[0]
            form_fields = json.loads(form_fields_row.fields)
            form_elements = form_fields['form_elements']
            
        return form_elements
        
    @classmethod
    def process_form(self, request, post_values, sub_cat_id):
        form_elements = DynamicForm.__get_form_elements(sub_cat_id)
        fd_group = 0 
        fd_type = ''
        fd_priority = 0
        date= ''
        ticket_body = ''
        subject = ''
        payment_method = ''
        subject = post_values[AppMsgs.SUBJECT_KEY] if AppMsgs.SUBJECT_KEY in post_values else ''
        if subject == '' and 'other_subject' in post_values:
            mbr_detail = getfromSession(AppMsgs.MBR_DETAIL_KEY,request)
            TestStr = config.TestStr
            ticket_type = post_values['fd_type']
            ticket_type = ticket_type.replace("&amp;", "&")
            ticket_subject = '{0}{1}, {2}, {3} - {4}'.format(TestStr,
                                                            mbr_detail[MbrDetailKeys.MBR_KEY], 
                                                            mbr_detail[MbrDetailKeys.LASTNAME_KEY], 
                                                            mbr_detail[MbrDetailKeys.FIRSTNAME_KEY],
                                                            ticket_type)
            subject = ticket_subject
        subject = subject.replace("R & R", "R&R")
        fd_group = int(post_values['fd_group']) if 'fd_group' in post_values else ''
        fd_type = post_values['fd_type'] if 'fd_type' in post_values else ''
        fd_priority = int(post_values['fd_priority']) if 'fd_priority' in post_values else ''
        user_acl = user_access_level[AppMsgs.SHALLOW_KEY]
        field_filter_lst = ['desired_new_services_group' , 'other_subject']
        
#   Process form values
        if form_elements and subject != '' and fd_group != '' and fd_type != '' \
            and fd_priority != '':
#             Show shallow access message/Submitted by agent name in the first line of description
            desc_body_heading = TicketService.ticket_desc_heading(request, user_acl)
            ticket_body += desc_body_heading

            for fe in form_elements:
                element_attr = json.loads(fe['plugin_data'])
                element_name = element_attr[AppMsgs.NAME]
                element_label = element_attr['label']
                if element_name not in field_filter_lst\
                    and element_name in post_values:
                    value = post_values[element_name]
                    if value != '':
                        if  element_name == 'new_payment_method' or element_name == 'one_time_payment_method':
                            value = new_payment_method_string_map[int(post_values[element_name])]
                        elif element_name == 'equipment':
                            value = equipment_string_map[int(post_values[element_name])]
                        elif element_name == 'payment_extension_days':
                            value = payment_extension_string_map[int(post_values[element_name])]
                        elif element_name == 'services_and_prices':
                            value = services_and_prices_map[int(post_values[element_name])]
                        elif element_name == 'desired_new_services':
                            service_field = Services.get_service_by_id(int(post_values[element_name]))
                            value = service_field.name
                        else:
                            value = post_values[element_name].encode('utf-8')
                        
                        if element_name == AppMsgs.OLD_PASSWORD_KEY:
                            ticket_body += '<div class="hide_old_pass">'
                        elif element_name == 'account_password':
                            ticket_body += '<div class="hide_account_pass">'
                        elif element_name == AppMsgs.ACCOUNT_HOLDER_NAME_KEY:
                            ticket_body += '<div><b>'
                            ticket_body += element_label + '</b> : ' +  value + ' '
                            ticket_body += '</div>'
                            ticket_body += '<div>&nbsp;</div>'
                            ticket_body += '<div><b>' + AppMsgs.PROBLEM_CATEGORY + '</b> : ' + post_values[AppMsgs.FD_CATEGORY_KEY] + '</div>'
                            ticket_body += '<div><b>' + AppMsgs.PROBLEM_SUB_CATEGORY + '</b> : ' + post_values[AppMsgs.FD_SUBCATEGORY_KEY] + '</div>'
                            ticket_body += '<div>&nbsp;</div>'
                            
                        
                        else:
                            ticket_body += '<div><b>'
                            ticket_body += element_label + '</b> : ' +  value + ' '
                            ticket_body += '</div>'
            
        return fd_group, fd_type, fd_priority, ticket_body, subject
    