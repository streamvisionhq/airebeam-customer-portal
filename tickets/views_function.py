from django.shortcuts import render
from models import Categories, Additional_Subcat_Fields
from django.http import HttpResponse, HttpResponseRedirect
from forms import DynamicForm
from enum import TicketStatus, ParentCategories, group_id_mappings
import json
from django.core.urlresolvers import reverse
from airebeam_customer_portal.AppServices.CustomerService import CustomerService
from airebeam_customer_portal.AppServices.TicketService import TicketService
from airebeam_customer_portal.AppServices.ConversationService import ConversationService
from airebeam_customer_portal.utils import checksession, getfromSession, savesessiondata,\
	strip_radio_stats
from airebeam_customer_portal.utils import check_if_session_time_exceeds
from radiostats.models import Radiostat
import enum
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from airebeam_customer_portal.enum import user_access_level, AppStatus
from tickets.models import Services
from airebeam_customer_portal.static_values import AppMsgs, CustomFields
from .enum import ServicesGroup
from airebeam_customer_portal.AppResponse import AppResponse
from tickets.enum import ticket_string_map
from airebeam_customer_portal.ServicePayload import ServicePayload
	
@csrf_exempt
@require_POST
def updated_wh(request):
	wh_response = request.body
# 	wh_response = "{\"id\": \"83213\", \"status\": \"Open\", \"priority\": \"Medium\", \"is_read\": \"1\"}"
	wh_response = json.loads(wh_response)
	ticket_id = wh_response[AppMsgs.ID_KEY]
	ticket_service_object = TicketService(request)
	payload = ServicePayload.get_updated_ticket_payload("0")
	payload = json.dumps(payload)
	ts_response = ticket_service_object.update_ticket_by_id(payload, ticket_id)
	if ts_response[AppMsgs.MSG].lower() == AppStatus.SUCCESS:
		pass
	return HttpResponse('')
	