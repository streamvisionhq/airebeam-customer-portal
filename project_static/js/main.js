var is_speedtest_running = false;
var link_href = '';
$(document).ready(function() {
	var ua = navigator.userAgent.toLowerCase(); 
	if (ua.indexOf('safari') != -1) { 
	  if (ua.indexOf('chrome') > -1) {
		  // Chrome
	  } else {
		  var avail_width = window.screen.availWidth;
		  if(avail_width <= 380){
			  $('.modal .modal-body.min-height80vh').addClass('iphone-min-height80vh');
			  $('.modal .modal-body.min-height80vh').removeClass('min-height80vh');
		  }
	  }
	}
	
	
	$( "#id_new_password" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     on_modal_rp_submitclick();
		}
	});
	
	$( "#id_confirm_new_password" ).keypress(function( event ) {
		if( event.which == 13 ) {
		     event.preventDefault();
		     on_modal_rp_submitclick();
		}
	});
	
	 $('.page_links').click(check_speedtest_inprogress);  
	
	var $force_reset_pass_ref = $('#force_reset_pass_value');
	var force_reset_val = $force_reset_pass_ref.val(); 
	if(force_reset_val == 1){
		$('.left-sidebar').addClass('acp-zero-zindex');
		$('#force_reset_pass_modal').modal({
			show: true,
			backdrop: 'static', 
			keyboard: false
		});
	}
	else{
		enable_ichk_box();
		terms_url_exist('/terms/terms_url_exist');
	}
	
	$("#input_mbr").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
        	
        	if( e.which == 13 ) {
	   		     event.preventDefault();
	   		     switch_account('input_mbr')
	   		}
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
        
    });
	
	mbr_exist('/mbr_online_check/mbr_exist/');
    setTimeout(hide_ticket_msg, 10000);
    
 });

function on_modal_logout_submitclick(){
	window.location = '/users/logout';
}

function on_modal_rp_submitclick(){
	reset_password();
}

function reset_password_service(mbr, key, password, medium){
	$.ajax({
    	cache: false,
        type : "post",
        url: '/reset_password/reset_pass_service',
        beforeSend: function(xhr, settings) {
            function getCookie(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie != '') {
                    var cookies = document.cookie.split(';');
                    for (var i = 0; i < cookies.length; i++) {
                        var cookie = jQuery.trim(cookies[i]);
                        // Does this cookie string begin with the name we want?
                        if (cookie.substring(0, name.length + 1) == (name + '=')) {
                            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                            break;
                        }
                    }
                }
                return cookieValue;
            }
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
    	},
    	data:{mbr:mbr, key:key, password:password, medium:medium},
        success: function(response){
        	var $error_div_ref = $('#error_div');
        	if(response.is_password_reset){
        		var $pass_reset_msg_ref = $('#id_password_reset_msg');
        		$pass_reset_msg_ref.show();
        		show_logout_timer();
        	}
        	else{
        		var $rp_submit_btn = $('#rp_submit_btn');
        		$rp_submit_btn.prop('disabled', false);
        		$error_div_ref.removeClass('no-display');
        		$error_div_ref.html('Reset failed. Please try later');
        	}
        }
    });
}

function reset_password(){
	var $rp_submit_btn = $('#rp_submit_btn');
	var is_valid = validate_fields();
	if(is_valid){
		$rp_submit_btn.prop('disabled', true);
		var $mbr_ref = $('#mbr_value');
		var $token_ref = $('#token_value');
//		var $medium_ref = $('#medium_value');
		var $new_password_ref = $("[name='new_password']");
		var mbr = $mbr_ref.val();
		var key = $token_ref.val();
		var medium = 'test';
		var password = $new_password_ref.val();
		reset_password_service(mbr, key, password, medium);
	}
}

function validate_fields(){
	var $new_password_ref = $("[name='new_password']");
	var $confirm_new_password_ref = $("[name='confirm_new_password']");
	var new_passwrod_val = $.trim( $new_password_ref.val() );
	var confirm_new_password = $.trim( $confirm_new_password_ref.val() );
	var $error_div_ref = $('#error_div');
	if(new_passwrod_val == "" || confirm_new_password == ""){
		$error_div_ref.removeClass('no-display');
		$error_div_ref.html('Please fill both passwords');
		return false;
	}
	else if(new_passwrod_val != confirm_new_password){
		$error_div_ref.removeClass('no-display');
		$error_div_ref.html('Passwords do not match');
		return false;
	}
	
	return true;
}

function hide_ticket_msg(){ 
	$('#ticket_submit_msg').hide("slow"); 
}

function switch_account(input_mbr_id){
	var input_mbr_ref = $('#' + input_mbr_id);
	var mbr = input_mbr_ref.val();
	if(mbr.trim() != ''){
		$('.site-holder').addClass('fixed-loadmask-msg');
		$('.site-holder').mask('<i class="  fa fa-refresh fa-spin"></i> Loading...');
		$('.site-holder .loadmask-msg').addClass('acp-loader-position');
		$('.site-holder .loadmask-msg div').addClass('acp-loader-font');
		$.ajax({
	    	cache: false,
	        type : "get",
	        url: '/mbr_online_check/mbr_found_on_emerald/' + input_mbr_ref.val(),
	        success: function(response){
	        	$('.site-holder .loadmask-msg').addClass('acp-loader-position');
	    		$('.site-holder .loadmask-msg div').addClass('acp-loader-font');
	        	$('.site-holder').unmask();
	        	$('.site-holder').removeClass('fixed-loadmask-msg');
	        	window.location = '/';
	      }
	    });
	}
	else{
		alert('Please fill MBR number');
	}
	
}

function mbr_exist(url){
    $.ajax({
    	cache: false,
        type : "get",
        url: url,
        success: function(response){
        	if(response.mbr_exist){
        		$('.acp-mbr-error.hide-until-populated').hide();
        		$('.main-content.hide-until-populated').addClass('show');
        		$('.acp-mbr-dependent.hide-until-populated').addClass('show');
	        	get_tickets('/tickets/customer_recent_tickets','#tickets-div');
	            getmbrdata('/mbr_online_check/account','#mbr_details');
	            getstatsdata('/radiostats/get_radiostats/','#rstats');
        	}
        	else{
//        		Hide main content div
        		$('.acp-mbr-error.hide-until-populated').addClass('show');
        	}
      }
    });
 }

function get_timer(url){
	var $speedtest_btn_ref = $('#run_speedtest_btn');
	var $container_speedtest_btn = $('#speedtest_btn_container');
	$container_speedtest_btn.prop('title', 'You can run speedtest in 5 minutes');
	$speedtest_btn_ref.addClass('disabled');
	$.ajax({
        type : "get",
        url: url,
        success: function(response){
	        	
	        	if(response.timer > 0){
		        	var remain_timer = parseInt(response.timer);
		            var display = $('#timer_container');
		        	display.show();
		        	var timer_name = 'speedtest';
		        	startTimer(remain_timer, display, $container_speedtest_btn, $speedtest_btn_ref, timer_name);
	        	}
	        	else{
	        		$container_speedtest_btn.prop('title', '');
	        		$speedtest_btn_ref.removeClass('disabled');
	        	}
        }
    });
}

function refreshStats() {
		var $speedtest_btn_ref = $('#run_speedtest_btn');
		var $container_speedtest_btn = $('#speedtest_btn_container');
		$container_speedtest_btn.prop('title', 'You can run speedtest in 5 minutes');
		$speedtest_btn_ref.addClass('disabled');
		//getspeedtestdata('/radiostats/radstats/','#rstats');
		var radio_url = '/radiostats/get_radiostats/?stats_by_cus=1';
		var dl_url = '/radiostats/download_stats/';
		var ul_url = '/radiostats/upload_stats/';
		var radiostats_ref = '#rstats';
//		var dl_ref = '#dl_st_result';
//		var ul_ref = '#ul_st_result';
		getspeedtestdata(radio_url, dl_url, ul_url, radiostats_ref)
    }

function show_logout_timer(){
	var $speedtest_btn_ref = null;
	var $container_speedtest_btn = null;
	var ten_seconds = 10,
    display = $('#logout_timer_container');
	display.show();
	var timer_name = 'logout';
	startTimer(ten_seconds, display, $container_speedtest_btn, $speedtest_btn_ref, timer_name);
}

function show_timer(){
	var $speedtest_btn_ref = $('#run_speedtest_btn');
	var $container_speedtest_btn = $('#speedtest_btn_container');
	var fiveMinutes = 60 * 5,
    display = $('#timer_container');
	display.show();
	var timer_name = 'speedtest';
	startTimer(fiveMinutes, display, $container_speedtest_btn, $speedtest_btn_ref, timer_name);
}

function get_tickets(url,div){
    $(div).mask('<i class="  fa fa-refresh fa-spin"></i> Loading...');
    $('#tickets-div .loadmask-msg').addClass('acp-loader-position');
	$('#tickets-div .loadmask-msg div').addClass('acp-loader-font');
    $.ajax({
    	cache: false,
        type : "get",
        url: url,
        success: function(response){
        	$(div).html(response);
        	$('#tickets-div .loadmask-msg div').addClass('acp-loader-font');
        	$(div).unmask();
        	$('.page_links').click(check_speedtest_inprogress);
      }
    });
 }

function view_all_tickets(){
	get_tickets('/tickets/index','#tickets-div');
 }

// Function no longer used
function getonlinecheckdata(url,div){
	$(div).mask('<i class="  fa fa-refresh fa-spin"></i> Running Speed Test...');
	$('#rstats .loadmask-msg').addClass('acp-loader-position');
	$('#rstats .loadmask-msg div').addClass('acp-loader-font');
	
	$.ajax({
        type : "get",
        url: url,
        success: function(response){
        	$('#rstats .loadmask-msg div').addClass('acp-loader-font');
        	getstatsdata('/radiostats/speedtest_result/','#rstats');
    }});
}

function on_modal_yesclick(){
	$('#speedtest_warn_modal').modal('toggle');
	console.log
	window.location = link_href;
}

function on_modal_noclick(){
	$('#speedtest_warn_modal').modal('toggle');
	link_href = '';
}

function check_speedtest_inprogress(event){
	if(is_speedtest_running){
		link_href =  $( this ).attr( "href" );
		event.preventDefault();
		//Show modal
		$('#speedtest_warn_modal').modal({
			show: true,
			backdrop: 'static', 
			keyboard: false
		});
	}
	else{
		
	}
	
}

function getspeedtestdata(radio_url, dl_url, ul_url, radiostats_ref){
	is_speedtest_running = true;
	$(radiostats_ref).mask('<i class="  fa fa-refresh fa-spin"></i> Loading...');
	$('#rstats .loadmask-msg').addClass('acp-loader-position');
	$('#rstats .loadmask-msg div').addClass('acp-loader-font');
	var dl_progress_bar_html = '<div id="acp-dl-progress-bar" class="progress-bar progress-bar-striped fixed-transition"' + 
		'role="progressbar" style="width: 0%" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100"></div>' ;
		
	$.ajax({
        type : "get",
        url: radio_url,
        success: function(response){
        	if(response.search("Connection not found") == -1){
		        $(radiostats_ref).html(response);
		        $('#rstats table tbody tr:nth-child(2) > td:nth-child(2)').html('Untested');
		        $('#rstats table tbody tr:nth-child(6) > td:nth-child(2)').html('Untested');
		        $('#rstats .loadmask-msg div').addClass('acp-loader-font');
	            $(radiostats_ref).unmask();
	            $('#rstats table tbody tr:nth-child(4) > td:nth-child(2)').html(dl_progress_bar_html);
	            $( "#acp-dl-progress-bar" ).animate({
	          	   width: "100%"
	          	}, {
	          	   duration: 30000,
	          	   step: function( now, fx ) {
	          		   $(this).text(Math.floor(now)+'%')
	          	   }
	          	}
	          	);
	            
		        $.ajax({
		            type : "get",
		          //url of download speedtest
		            url: dl_url,	
		            success: function(response){
		            	var resp = response;
		            	var ul_progress_bar_html = '<div id="acp-ul-progress-bar" class="progress-bar progress-bar-striped fixed-transition"' + 
		 			  		'role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>';
		            	$('#rstats table tbody tr:nth-child(4) > td:nth-child(2)').html(response);
		            	$('#rstats table tbody tr:nth-child(6) > td:nth-child(2)').html(ul_progress_bar_html);
		            	$( "#acp-ul-progress-bar" ).animate({
		               	   width: "100%"
		               	}, {
		               	   duration: 30000,
		               	   step: function( now, fx ) {
		               		   $(this).text(Math.floor(now)+'%')
		               	   }
		               	}
		               	);
		            	$.ajax({
		    	            type : "get",
		    	          //url of upload speedtest
		    	            url: ul_url,	
		    	            success: function(response){
		    	            	is_speedtest_running = false;
		    	            	var resp = response;
		    	            	$('#rstats table tbody tr:nth-child(6) > td:nth-child(2)').html(response);
		    	            	var speedtest_id = $('#rstats table tbody tr:nth-child(6) > td:nth-child(2) input#speedtest_id').val()
		    	            	$('#rstats table tbody tr:nth-child(2) > td:nth-child(2)').html(speedtest_id);
			    	            show_timer();
		    	            }
		    	        });
		            }
		        });
        	}
        	else{
        		$(radiostats_ref).html(response);
        		$(radiostats_ref).unmask();
        	}
        }
	});
}


function getmbrdata(url,div){
 	$(div).mask('<i class="  fa fa-refresh fa-spin"></i> Loading...');
	$('#mbr_details .loadmask-msg').addClass('acp-loader-position');
	$('#mbr_details .loadmask-msg div').addClass('acp-loader-font');
	
	$.ajax({
        type : "get",
        url: url,
        success: function(response){
        $(div).html(response);
        $('#mbr_details .loadmask-msg div').addClass('acp-loader-font');
        $(div).unmask();
    }});
}

 function getstatsdata(url,div){
	 	var $speedtest_btn_ref = $('#run_speedtest_btn');
	 	$speedtest_btn_ref.addClass('disabled');
	 	$(div).mask('<i class="  fa fa-refresh fa-spin"></i> Loading...');
		$('#rstats .loadmask-msg').addClass('acp-loader-position');
		$('#rstats .loadmask-msg div').addClass('acp-loader-font');
		
		$.ajax({
	        type : "get",
	        url: url,
	        success: function(response){
	        	$speedtest_btn_ref.addClass('disabled');
	        	str_search = "Connection not found";
	        	is_error_found = response.search(str_search);
	        	if( is_error_found < 0 ){
	        		get_timer('/radiostats/get_speedtest_timer/');
	        	}
		        $(div).html(response);
		        $('#rstats .loadmask-msg div').addClass('acp-loader-font');
		        $(div).unmask();
	    }});
 }
 
 
 function on_refresh_getstatsdata(url,div){
	   $(div).mask('<i class="  fa fa-refresh fa-spin"></i> Running Speed Test...');
	   $('#rstats .loadmask-msg').addClass('acp-loader-position');
	   $('#rstats .loadmask-msg div').addClass('acp-loader-font');
	  $.ajax({
	        type : "get",
	        url: url,
	        success: function(response){
	        
	        $(div).html(response);
	        $('#rstats .loadmask-msg div').addClass('acp-loader-font');
	        $(div).unmask();
	    }});
	 }

 function getonlinecheck(url,div){

      $(div).mask('<i class="  fa fa-refresh fa-spin"></i> Loading...');
      $.ajax({
        type : "get",
        url: url,
        success: function(response){
        $(div).unmask();
        $(div).html(response);
      }
    });
 }
 function getrecentdata(url,div){
      $.ajax({
        type : "get",
        url: url,
        success: function(response){
        $(div).html(response);
        }
        });
}

function getmbr(url,div){
     $(div).mask('<i class="  fa fa-refresh fa-spin"></i> Loading...');
    $.ajax({
        type : "get",
        url: url,
        async: false,
        success: function(response){
        $(div).unmask();
        $(div).html(response);
        $('.username').html($('#mbr-username').html());
         document.cookie="username=" + $('#mbr-username').html();
      }
    });
 }
