function enable_ichk_box(){
	$('.square-input').iCheck({
		checkboxClass: 'icheckbox_polaris',
		radioClass: 'iradio_polaris',
        increaseArea: '20%', // optional
	});
	
	$('.square-input').iCheck('disable');
	
	$('.square-input').on('ifChecked', function(event){
		  $('#modal_accept_btn').removeClass('disabled');
	});
	
	$('.square-input').on('ifUnchecked', function(event){
		$('#modal_accept_btn').addClass('disabled');
	});
}

function on_modal_cancelclick(){
	var msg = "Are you sure you want to Cancel? You will be logged out of current session.";
    if (confirm(msg) == true) {
//    	Logout
    	window.location = '/users/logout';
    	$('#myModal').modal('toggle');
    } 
}

function on_modal_submitclick(){
//	Call update service
//	Remove acp-zero-zindex class from left-sidebar
	var mbr_number_ref = $('#mbr_number_hidden');
	var mbr_number_value = mbr_number_ref.val();
	var url = "/terms/accept_terms";
	if(mbr_number_value != ''){
		$.ajax({	
	        type : "get",
			url: url,
			success: function(response){
				console.log(response);
				console.log('success');
				$('.left-sidebar').removeClass('acp-zero-zindex');
				$('#myModal').modal('toggle');
			}
	    });
	}
	
}

function on_modal_viewtermsclick(){
	show_terms_modal();
}

function chk_scroll(e) {
    var elem = $(e.currentTarget);
    if (elem[0].scrollHeight - elem.scrollTop() == elem.outerHeight()) {
    	$('.square-input').iCheck('enable');
    }
}

function show_terms_modal(){
	$('#viewerContainer').bind('scroll', chk_scroll);
	$('#terms_updated_modal').modal('toggle');
	$('#myModal').modal({
		show: true,
		backdrop: 'static', 
		keyboard: false
	});
}

function terms_url_exist(url){
    $.ajax({
    	cache: false,
        type : "get",
        url: url,
        success: function(response){
        	if(response.terms_url != null){
//        		Load pdf from term's url in modal pop up
        		var terms_url = response.terms_url;
        		$('.left-sidebar').addClass('acp-zero-zindex');
        		$('#terms_updated_modal').modal({
        			show: true,
        			backdrop: 'static', 
        			keyboard: false
        		});
        	}
        	else{

        	}
      }
    });
 }
