var timer = 0;
var minutes = 0;
var seconds = 0;

function start_timer_if_time_remaining(){
	var remaining_timer = localStorage.getItem("remaing_timer");
	if(remaining_timer != null){
		timer = parseInt(remaining_timer);
		var minutes = localStorage.getItem("minutes");
		var seconds = localStorage.getItem("seconds");
		var $speedtest_btn_ref = $('#run_speedtest_btn');
		var $container_speedtest_btn = $('#speedtest_btn_container');
		$container_speedtest_btn.prop('title', 'You can run speedtest in 5 minutes');
		$speedtest_btn_ref.addClass('disabled');
		var $display = $('#timer_container');
		$display.text(minutes + ":" + seconds);
		$display.show();
		var interval = setInterval(function(){
	    	calculate_timer(interval, $display, $container_speedtest_btn, $speedtest_btn_ref, minutes, seconds)
	    }
	    		, 1000 );
	}
}

function calculate_timer(interval, display, timer_name, $container_speedtest_btn, $speedtest_btn_ref, minutes, seconds) {
    minutes = parseInt(timer / 60, 10)
    seconds = parseInt(timer % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    
    if(timer_name == 'speedtest'){
    	display.text(minutes + ":" + seconds);
	    localStorage.setItem("sp_remaing_timer", timer);
	    localStorage.setItem("sp_minutes", minutes);
	    localStorage.setItem("sp_seconds", seconds);
    }
    
    else if(timer_name == 'logout'){
    	display.text(seconds);
	    localStorage.setItem("lo_remaing_timer", timer);
	    localStorage.setItem("lo_minutes", minutes);
	    localStorage.setItem("lo_seconds", seconds);
    }
    
    if (--timer < 0) {
        clearInterval(interval);
        if(timer_name == 'speedtest'){
	        localStorage.removeItem("sp_remaing_timer");
	        localStorage.removeItem("sp_minutes");
	        localStorage.removeItem("sp_seconds");
	        $container_speedtest_btn.prop('title', '');
	        $speedtest_btn_ref.removeClass('disabled');
	        display.hide();
	        display.text("05:00");
        }
        
        else if(timer_name == 'logout'){
        	localStorage.removeItem("lo_remaing_timer");
	        localStorage.removeItem("lo_minutes");
	        localStorage.removeItem("lo_seconds");
	        window.location = '/users/logout';
        }
        
    }
}

function startTimer(duration, display, $container_speedtest_btn, $speedtest_btn_ref, timer_name) {
    timer = duration, minutes, seconds;
    var interval = setInterval(function(){
    	calculate_timer(interval, display, timer_name, $container_speedtest_btn, $speedtest_btn_ref, minutes, seconds)
    }
    		, 1000 );
}